import React from "react";
import { View } from "react-native";
import ThankPage from "../containers/ThankPage";

export default function ThankScreen(props) {
  return (
    <View style={{ flex: 1 }}>
      <ThankPage navigation={props.navigation} route={props.route.params} />
    </View>
  );
}
