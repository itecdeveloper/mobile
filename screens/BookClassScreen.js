import React from "react";
import { ScrollView } from "react-native-gesture-handler";
import BookClassPage from "../containers/BookClassPage";
import { View } from "react-native";
import Constants from "expo-constants";

export default function RescheduleScreen(props) {
  return (
    <View style={{ flex: 1, backgroundColor: "#F8F7F7" }}>
      <View
        style={{
          height: Constants.statusBarHeight,
        }}
      />
      <BookClassPage navigation={props.navigation} route={props.route.params} />
    </View>
  );
}
