import * as WebBrowser from "expo-web-browser";
import React from "react";
import { ScrollView } from "react-native-gesture-handler";
import BankTransferPage from "../containers/BankTransferPage";
import { View } from "react-native";
import Constants from "expo-constants";
function BankTransferScreen(props) {
  return (
    <View style={{ flex: 1, backgroundColor: "#f8f7f7" }}>
      <View
        style={{
          backgroundColor: "#ffffff",
          height: Constants.statusBarHeight,
        }}
      />

      <BankTransferPage
        navigation={props.navigation}
        route={props.route.params}
      />
    </View>
  );
}

export default BankTransferScreen;
