import React from "react";
import { ScrollView } from "react-native-gesture-handler";
import RescheduleRequestPage from "../containers/RescheduleRequestPage";
import { View } from "react-native";
import Constants from "expo-constants";
function RescheduleRequestScreen(props) {
  return (
    <View style={{ flex: 1, backgroundColor: "#f8f7f7" }}>
      <View
        style={{
          backgroundColor: "#F8F7F7",
          height: Constants.statusBarHeight,
        }}
      />
      <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
        <RescheduleRequestPage
          navigation={props.navigation}
          route={props.route.params}
        />
      </ScrollView>
    </View>
  );
}

export default RescheduleRequestScreen;
