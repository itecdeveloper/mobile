import * as WebBrowser from "expo-web-browser";
import React from "react";
import { ScrollView } from "react-native-gesture-handler";
import ForgotPasswordPage from "../containers/ForgotPasswordPage";
import { View } from "react-native";
import Constants from "expo-constants";

export default function ForgotPasswordScreen(props) {
  return (
    <View style={{ flex: 1 }}>
      <ForgotPasswordPage navigation={props.navigation} />
    </View>
  );
}
