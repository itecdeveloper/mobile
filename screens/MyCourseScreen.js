import * as WebBrowser from "expo-web-browser";
import React from "react";
import { ScrollView } from "react-native-gesture-handler";
import MyCoursePage from "../containers/MyCoursePage";
import { View } from "react-native";
import { connect } from "react-redux";
import LoginPage from "../containers/LoginPage";
function MyCourseScreen(props) {
  const [refreshing, setRefreshing] = React.useState(false);
  return (
    <View style={{ flex: 1, backgroundColor: "#f8f7f7" }}>
      {props.user.token ? (
        <View style={{ flex: 1 }}>
          <MyCoursePage navigation={props.navigation} refreshing={refreshing} />
        </View>
      ) : (
        <LoginPage
          navigation={props.navigation}
          dataPage={{ page: "MyCourse" }}
        />
      )}
    </View>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(MyCourseScreen);
