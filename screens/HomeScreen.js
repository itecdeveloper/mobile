import React from "react";
import { View } from "react-native";

import HomePage from "../containers/HomePage";
import Constants from "expo-constants";
import { ScrollView } from "react-native-gesture-handler";

export default function HomeScreen(props) {
  return (
    <View style={{ flex: 1 }}>
      <HomePage navigation={props.navigation} />
    </View>
  );
}
