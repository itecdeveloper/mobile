import * as WebBrowser from "expo-web-browser";
import React from "react";
import { ScrollView } from "react-native-gesture-handler";
import EditProfilePage from "../containers/EditProfilePage";
import { View } from "react-native";
import Constants from "expo-constants";

export default function EditProfileScreen(props) {
  return (
    <View style={{ flex: 1 }}>
      <EditProfilePage navigation={props.navigation} />
    </View>
  );
}
