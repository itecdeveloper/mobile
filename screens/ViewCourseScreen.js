import React from "react";
import { ScrollView } from "react-native-gesture-handler";
import ViewCoursePage from "../containers/ViewCoursePage";
import { View } from "react-native";

export default function ViewCourseScreen(props) {
  return (
    <View style={{ flex: 1 }}>
      <ViewCoursePage
        navigation={props.navigation}
        route={props.route.params}
      />
    </View>
  );
}
