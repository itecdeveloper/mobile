import React, { useState } from "react";
import PaymentPage from "../containers/PaymentPage";
import { View, ActivityIndicator } from "react-native";
import { connect } from "react-redux";
import LoginPage from "../containers/LoginPage";
import { useFocusEffect } from "@react-navigation/native";

function PaymentScreen(props) {
  const [load, setLoad] = useState(true);
  useFocusEffect(
    React.useCallback(() => {
      setLoad(true);
      setTimeout(() => {
        setLoad(false);
      }, 2000);
    }, [])
  );
  return (
    <View style={{ flex: 1 }}>
      {!props.user ? (
        <View style={{ flex: 1 }}>
          <LoginPage
            navigation={props.navigation}
            dataPage={props.route.params.dataViewCourse}
            onLogin={() => {
              props.navigation.goBack();
            }}
          />
        </View>
      ) : (
        <PaymentPage navigation={props.navigation} route={props.route.params} />
      )}
    </View>
  );
}
const mapStateToProps = (state) => {
  return {
    user: state.user.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(PaymentScreen);
