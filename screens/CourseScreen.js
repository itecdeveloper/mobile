import React from "react";
import { ScrollView } from "react-native-gesture-handler";
import CoursePage from "../containers/CoursePage";
import { View } from "react-native";
import Constants from "expo-constants";
function CourseScreen(props) {
  return (
    <View style={{ flex: 1, backgroundColor: "#f8f7f7" }}>
      <View
        style={{
          backgroundColor: "#F8F7F7",
          height: Constants.statusBarHeight,
        }}
      />
      <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
        <CoursePage navigation={props.navigation} route={props.route.params} />
      </ScrollView>
    </View>
  );
}

export default CourseScreen;
