import * as WebBrowser from "expo-web-browser";
import React from "react";
import { ScrollView } from "react-native-gesture-handler";
import { View } from "react-native";
import LoginPage from "../containers/LoginPage";
import Constants from "expo-constants";

export default function LoginScreen(props) {
  return (
    <View style={{ flex: 1 }}>
      <LoginPage navigation={props.navigation} />
    </View>
  );
}
