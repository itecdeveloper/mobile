import * as WebBrowser from "expo-web-browser";
import React from "react";
import { ScrollView } from "react-native-gesture-handler";
import RegisPage from "../containers/RegisPage";
import { View } from "react-native";
import Constants from "expo-constants";

export default function RegisScreen(props) {
  return (
    <View style={{ flex: 1 }}>
      <RegisPage navigation={props.navigation} route={props.route.params} />
    </View>
  );
}
