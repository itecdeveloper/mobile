import React from "react";
import { ScrollView } from "react-native-gesture-handler";
import BookCoursePage from "../containers/BookCoursePage";
import Constants from "expo-constants";
import { View, Platform } from "react-native";
import { connect } from "react-redux";
import LoginPage from "../containers/LoginPage";
function BookCourseScreen(props) {
  return (
    <View style={{ flex: 1 }}>
      {!props.user ? (
        <View style={{ flex: 1 }}>
          <LoginPage navigation={props.navigation} />
        </View>
      ) : (
        <View style={{ flex: 1 }}>
          <View
            style={{
              backgroundColor: "#f4f4f4",
              height: Constants.statusBarHeight,
            }}
          />

          <ScrollView
            scrollEnabled={false}
            contentContainerStyle={{ flexGrow: 1 }}
          >
            <BookCoursePage
              navigation={props.navigation}
              route={props.route.params}
            />
          </ScrollView>
        </View>
      )}
    </View>
  );
}
const mapStateToProps = (state) => {
  return {
    user: state.user.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(BookCourseScreen);
