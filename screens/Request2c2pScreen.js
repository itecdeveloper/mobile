import React from "react";
import { View } from "react-native";
import Constants from "expo-constants";
import Request2c2pPage from "../containers/Request2c2pPage";

export default function Request2c2pScreen(props) {
  return (
    <View style={{ flex: 1 }}>
      <View
        style={{
          backgroundColor: "#ffffff",
          height: Constants.statusBarHeight,
        }}
      />

      <Request2c2pPage
        navigation={props.navigation}
        route={props.route.params}
      />
    </View>
  );
}
