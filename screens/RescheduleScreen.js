import React from "react";
import { ScrollView } from "react-native-gesture-handler";
import ReschedulePage from "../containers/ReschedulePage";
import { View } from "react-native";

export default function RescheduleScreen(props) {
  return (
    <View style={{ flex: 1, backgroundColor: "#F8F7F7" }}>
      <ReschedulePage
        navigation={props.navigation}
        route={props.route.params}
      />
    </View>
  );
}
