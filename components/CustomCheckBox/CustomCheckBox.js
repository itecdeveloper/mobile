import React, { Component } from "react";
import { View, TouchableOpacity } from "react-native";
import CheckBox from "react-native-check-box";
import { InterRegularText, InterBoldText } from "../StyledText";
import { Language } from "../../constants/language";

export default class CustomCheckBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isChecked: this.props.isChecked ? this.props.isChecked : false,
    };
  }
  onCheck = () => {
    let isChecked = !this.state.isChecked;
    this.setState({ isChecked: isChecked });
    return isChecked;
  };

  render() {
    let isChecked = this.state.isChecked;
    return (
      <View
        style={{
          flexDirection: "row",
        }}
      >
        {!isChecked ? (
          <TouchableOpacity
            style={{
              width: 20,
              height: 20,
              borderWidth: 1,
              borderRadius: 5,
              borderColor: "#9b9b9b",
              marginRight: 8,
            }}
            onPress={() => {
              this.props.onCheck(this.onCheck());
            }}
          />
        ) : (
          <TouchableOpacity
            style={{
              width: 20,
              height: 20,
              borderWidth: 1,
              borderRadius: 5,
              marginRight: 8,
              borderColor: "#ED1B24",
              justifyContent: "center",
              alignItems: "center",
            }}
            onPress={() => {
              this.props.onCheck(this.onCheck());
            }}
          >
            <CheckBox
              uncheckedCheckBoxColor="#9b9b9b"
              checkedCheckBoxColor="#ED1B24"
              isChecked={true}
              onClick={() => {
                this.props.onCheck(this.onCheck());
              }}
            />
          </TouchableOpacity>
        )}
        <InterRegularText
          text={this.props.text1}
          style={{ color: "#9b9b9b" }}
        />
        <TouchableOpacity
          onPress={() => {
            this.props.onPressText2();
          }}
        >
          <InterBoldText text={this.props.text2} style={{ color: "#ED1B24" }} />
        </TouchableOpacity>
      </View>
    );
  }
}
