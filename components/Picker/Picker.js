import React, { useState } from "react";
import { View, StyleSheet, Platform } from "react-native";
import RNPickerSelect from "react-native-picker-select";
import PickerIcon from "../PicketIcon";
import { InterRegularText } from "../StyledText";
export default function Picker(props) {
  const [value, setValue] = useState(null);
  const [active, setActive] = useState(false);
  const inputRefs = {
    refPicker: null,
  };

  return (
    <View
      style={{
        width: "87%",
        height: 64,
        backgroundColor: "#ffffff",
        borderRadius: 5,
        marginTop: 16,
        flexDirection: "row",
        borderLeftWidth: active ? 3 : 0,
        borderLeftColor: "#ED1B24",
      }}
    >
      <View style={{ flex: 1, justifyContent: "center" }}>
        {value === props.placeholder.value ? null : (
          <InterRegularText
            text={props.label}
            style={{
              fontSize: 12,
              marginLeft: 10,
              color: "#7a7a7a",
              marginTop: 8,
            }}
          />
        )}
        <RNPickerSelect
          placeholder={props.placeholder}
          items={props.items}
          onValueChange={(value) => {
            props.onValueChange(value);
            setValue(value);
          }}
          style={pickerSelectStyles}
          value={value}
          useNativeAndroidPickerStyle={false}
          ref={(el) => {
            inputRefs.refPicker = el;
          }}
          onOpen={() => {
            if (Platform.OS === "ios") {
              setActive(true);
            }
          }}
          onClose={() => {
            setActive(false);
          }}
          modalProps={{ backgroundColor: "red" }}
        />
      </View>
      <View
        style={{ height: "100%", justifyContent: "center", marginRight: 27 }}
      >
        <PickerIcon onOpen={active} />
      </View>
    </View>
  );
}

const pickerSelectStyles = {
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    color: "#232326",
    paddingRight: 30, // to ensure the text is never behind the icon
    fontFamily: "Inter-Regular",
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    color: "#232326",
    paddingRight: 30, // to ensure the text is never behind the icon
    fontFamily: "Inter-Regular",
  },
};
