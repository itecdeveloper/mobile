import * as React from "react";
import { Image } from "react-native";

export default function Back(props) {
  return (
    <Image
      style={{
        width: props.onOpen ? 10 : 6,
        height: props.onOpen ? 6 : 10,
      }}
      source={
        props.onOpen
          ? require("../assets/images/picker_active.png")
          : require("../assets/images/picker.png")
      }
    />
  );
}
