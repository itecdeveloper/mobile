import * as React from "react";
import { Image } from "react-native";

export default function QrCode(props) {
  return (
    <Image
      style={{
        width: props.width ? props.width : 220,
        height: props.height ? props.height : 220,
      }}
      source={require("../assets/images/qrcode.png")}
    />
  );
}
