import * as React from "react";
import { Image } from "react-native";

export default function Back(props) {
  return (
    <Image
      style={{
        width: props.width ? props.width : 32,
        height: props.height ? props.height : 32,
      }}
      source={require("../assets/images/arrow-left.png")}
    />
  );
}
