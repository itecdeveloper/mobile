import * as React from "react";
import { Image, View } from "react-native";
import { Language } from "../constants/language";
import { InterBoldText } from "./StyledText";

export default function ApprovedStaus(props) {
  return (
    <View
      style={{
        width: 100,
        height: 32,
        backgroundColor: "#D8F4D2",
        borderRadius: 5,
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row",
      }}
    >
      <Image
        style={{
          width: props.width ? props.width : 10,
          height: props.height ? props.height : 8.5,
        }}
        source={require("../assets/images/approvedicon.png")}
      />
      <InterBoldText
        text={Language.EN.reschedule.complete}
        style={{ fontSize: 13, marginLeft: 6, color: "#2D7720" }}
      />
    </View>
  );
}
