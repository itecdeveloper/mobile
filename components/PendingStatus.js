import * as React from "react";
import { Image, View } from "react-native";
import { Language } from "../constants/language";
import { InterBoldText } from "./StyledText";

export default function PendingStatus(props) {
  return (
    <View
      style={{
        width: 96,
        height: 32,
        backgroundColor: "#FFEBBA",
        borderRadius: 5,
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row",
      }}
    >
      <Image
        style={{
          width: props.width ? props.width : 12,
          height: props.height ? props.height : 18,
        }}
        source={require("../assets/images/pendingicon.png")}
      />
      <InterBoldText
        text={Language.EN.reschedule.pending}
        style={{ fontSize: 13, marginLeft: 6, color: "#83610D" }}
      />
    </View>
  );
}
