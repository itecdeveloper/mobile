import * as React from "react";
import { Image, View } from "react-native";

export default function Cancel(props) {
  return (
    <View
      style={{
        width: 28,
        height: 28,
        backgroundColor: "#ED1B24",
        borderRadius: 5,
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Image
        style={{
          width: props.width ? props.width : 24,
          height: props.height ? props.height : 24,
        }}
        source={require("../assets/images/cancelicon.png")}
      />
    </View>
  );
}
