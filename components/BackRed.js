import * as React from "react";
import { Image, TouchableOpacity } from "react-native";
import { InterBoldText } from "./StyledText";

export default function BackRed(props) {
  return (
    <TouchableOpacity
      style={{ flexDirection: "row", alignItems: "center" }}
      onPress={() => {
        props.onPress();
      }}
    >
      <Image
        style={[
          {
            width: props.width ? props.width : 14,
            height: props.height ? props.height : 14,
          },
          props.style,
        ]}
        source={require("../assets/images/backred.png")}
      />
      <InterBoldText
        text={props.text ? props.text : "Back"}
        style={{ color: "#ED1B24", marginLeft: 13, fontSize: 14 }}
      />
    </TouchableOpacity>
  );
}
