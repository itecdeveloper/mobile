import * as React from "react";
import { Image } from "react-native";

export default function NextBlack(props) {
  return (
    <Image
      style={[
        {
          width: props.width ? props.width : 14,
          height: props.height ? props.height : 14,
        },
        props.style,
      ]}
      source={require("../assets/images/next-red.png")}
    />
  );
}
