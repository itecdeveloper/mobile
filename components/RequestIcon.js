import * as React from "react";
import { Image } from "react-native";

export default function Back(props) {
  return (
    <Image
      style={[
        {
          width: props.width ? props.width : 50,
          height: props.height ? props.height : 50,
        },
        props.style,
      ]}
      source={require("../assets/images/request.png")}
    />
  );
}
