import * as React from "react";
import { Image, View } from "react-native";

export default function Logo(props) {
  return (
    <View
      style={{
        width: 72,
        height: 72,
        backgroundColor: "#00529C",
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 20,
      }}
    >
      <Image
        style={{
          width: props.width ? props.width : 56,
          height: props.height ? props.height : 56,
        }}
        source={require("../assets/images/eduworldicon.png")}
      />
    </View>
  );
}
