import * as React from "react";
import { Image, View } from "react-native";
import { Language } from "../constants/language";
import { InterBoldText } from "./StyledText";

export default function DeniedStatus(props) {
  return (
    <View
      style={{
        height: 32,
        backgroundColor: "#FFD8D8",
        borderRadius: 5,
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row",
        padding: 5,
      }}
    >
      <Image
        style={{
          width: props.width ? props.width : 20,
          height: props.height ? props.height : 20,
        }}
        source={require("../assets/images/deniedicon.png")}
      />
      <InterBoldText
        text={Language.EN.reschedule.denied}
        style={{ fontSize: 13, marginLeft: 3, color: "#B41B1B" }}
      />
    </View>
  );
}
