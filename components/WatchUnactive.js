import * as React from "react";
import { Image } from "react-native";

export default function WatchUnactive(props) {
  return (
    <Image
      style={{
        width: props.width ? props.width : 20,
        height: props.height ? props.height : 20,
      }}
      source={require("../assets/images/watch-unactive.png")}
    />
  );
}
