import * as React from "react";
import { Image } from "react-native";

export default function CalendarIcon(props) {
  return (
    <Image
      style={{
        width: props.width ? props.width : 24,
        height: props.height ? props.height : 24,
      }}
      source={require("../assets/images/calendar.png")}
    />
  );
}
