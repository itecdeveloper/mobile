import * as React from "react";
import { Text, View, TouchableOpacity, TextStyle } from "react-native";
import { TextField } from "react-native-material-textfield";
import AlertErr from "./AlertErr";
import Note from "./Note";
import CalendarIcon from "./CalendarIcon";
import Clock from "./Clock";

export function MonoText(props) {
  return (
    <Text {...props} style={[props.style, { fontFamily: "space-mono" }]} />
  );
}

export function InterBoldText(props) {
  return (
    <Text
      {...props}
      style={[{ fontFamily: "Inter-Bold", color: "#333333" }, props.style]}
    >
      {props.text}
    </Text>
  );
}

export function InterRegularText(props) {
  return (
    <Text
      {...props}
      style={[{ fontFamily: "Inter-Regular", color: "#333333" }, props.style]}
    >
      {props.text}
    </Text>
  );
}

export function InterLightText(props) {
  return (
    <Text
      {...props}
      style={[{ fontFamily: "Inter-Light", color: "#333333" }, props.style]}
    >
      {props.text}
    </Text>
  );
}

export const InterRegularTextInput = React.forwardRef((props, ref) => {
  const [focus, setFocus] = React.useState(false);
  const [showError, setShowError] = React.useState(false);
  const [value, setValue] = React.useState("");
  const [error, setError] = React.useState(false);

  React.useEffect(() => {
    if (props.showError) {
      setShowError(error);
    }
  }, [error]);
  React.useEffect(() => {
    if (props.fixError) {
      setShowError(true);
    } else {
      setShowError(false);
    }
  }, [props.fixError]);

  return (
    <View style={{ width: "87%" }}>
      <TouchableOpacity
        style={[
          {
            width: "100%",
            height: 64,
            backgroundColor: "#ffffff",
            justifyContent: "center",
            borderRadius: 5,
            marginTop: 16,
            flexDirection: "row",
          },
          props.touchableStyle,
        ]}
        onPress={() => {
          ref.current.focus();
          if (props.onPress) {
            props.onPress();
          }
        }}
      >
        {focus ? (
          <View
            style={{
              height: "100%",
              width: 3,
              backgroundColor: "red",
              borderTopLeftRadius: 5,
              borderBottomLeftRadius: 5,
            }}
          />
        ) : null}
        <View style={{ flex: 1, justifyContent: "center" }}>
          <TextField
            pointerEvents="none"
            ref={ref}
            baseColor="rgba(0, 0, 0, 0.5)"
            tintColor="rgba(0, 0, 0, 0.5)"
            textColor="rgba(0, 0, 0, 0.7)"
            lineWidth={0}
            activeLineWidth={0}
            selectionColor="rgba(0, 0, 0, 0.7)"
            labelTextStyle={{ fontFamily: "Inter-Regular" }}
            containerStyle={{
              marginLeft: 16,
              marginRight: 5,
              marginBottom: 10,
            }}
            style={{
              fontFamily: "Inter-Regular",
            }}
            {...props}
            onBlur={() => {
              setFocus(false);

              if (value) {
                setError(false);
              } else {
                setError(true);
              }
              if (props.onBlur) {
                props.onBlur();
              }
            }}
            onFocus={() => {
              setFocus(true);
              setValue(ref.current ? ref.current.value() : "");
              if (props.onFocus) {
                props.onFocus();
              }
            }}
            onChangeText={(value) => {
              if (props.keyboardType === "phone-pad") {
                let text = value;
                let check;
                var newText = "";
                var numbers = "0123456789";
                if (text.length < 1) {
                  check = "";
                }
                for (var i = 0; i < text.length; i++) {
                  if (numbers.indexOf(text[i]) > -1) {
                    newText = newText + text[i];
                  }
                  check = newText;
                }

                props.onChangeText(check);
                if (check) {
                  setError(false);
                } else {
                  setError(true);
                }
                setValue(check);
                ref.current.setValue(check);
              } else {
                props.onChangeText(value);
                if (value) {
                  setError(false);
                } else {
                  setError(true);
                }
                setValue(value);
              }
            }}
            value={props.value ? props.value : value}
            onSubmitEditing={() => {
              if (value) {
                setError(false);
              } else {
                setError(true);
              }
              if (props.onSubmitEditing) {
                props.onSubmitEditing();
              }
            }}
          />
        </View>
        {props.showIconDate ? (
          <View style={{ justifyContent: "center", marginRight: 23 }}>
            <CalendarIcon />
          </View>
        ) : null}
        {props.showIconTime ? (
          <View style={{ justifyContent: "center", marginRight: 23 }}>
            <Clock />
          </View>
        ) : null}
        {showError ? (
          <TouchableOpacity
            activeOpacity={1}
            style={{ justifyContent: "center", marginRight: 12 }}
          >
            <AlertErr />
          </TouchableOpacity>
        ) : null}
      </TouchableOpacity>
      {showError ? (
        <View
          style={{
            height: 36,
            width: "100%",
            position: "absolute",
            alignItems: "flex-end",
          }}
        >
          <Note
            text={props.textError ? props.textError : null}
            fontSize={props.fontSizeErr}
          />
        </View>
      ) : null}
    </View>
  );
});
