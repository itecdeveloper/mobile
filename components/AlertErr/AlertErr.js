import React, { useState, useEffect } from "react";
import { Image } from "react-native";

const AlertErr = (props) => {
  return (
    <Image
      style={{
        width: props.width ? props.width : 24,
        height: props.height ? props.height : 24,
      }}
      source={require("../../assets/images/alert.png")}
    />
  );
};

export default AlertErr;
