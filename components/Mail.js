import * as React from "react";
import { Image } from "react-native";

export default function Mail(props) {
  return (
    <Image
      style={{
        width: props.width ? props.width : 160,
        height: props.height ? props.height : 160,
      }}
      source={require("../assets/images/mail.png")}
    />
  );
}
