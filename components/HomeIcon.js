import * as React from "react";
import { Image } from "react-native";

export default function HomeIcon(props) {
  return (
    <Image
      style={{
        width: props.width ? props.width : 24,
        height: props.height ? props.height : 24,
      }}
      source={
        props.focused
          ? require("../assets/images/home_active.png")
          : require("../assets/images/home.png")
      }
    />
  );
}
