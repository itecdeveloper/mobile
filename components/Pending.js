import * as React from "react";
import { Image, View } from "react-native";

export default function Pending(props) {
  return (
    <View
      style={{
        width: 28,
        height: 28,
        backgroundColor: "#ED1B24",
        borderRadius: 5,
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Image
        style={{
          width: props.width ? props.width : 16,
          height: props.height ? props.height : 20,
        }}
        source={require("../assets/images/pending.png")}
      />
    </View>
  );
}
