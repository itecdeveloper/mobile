import * as React from "react";
import { Image, TouchableOpacity } from "react-native";

export default function CloseGrey(props) {
  return (
    <TouchableOpacity
      onPress={() => {
        props.onPress();
      }}
    >
      <Image
        style={{
          width: props.width ? props.width : 24,
          height: props.height ? props.height : 24,
        }}
        source={require("../assets/images/close_grey.png")}
      />
    </TouchableOpacity>
  );
}
