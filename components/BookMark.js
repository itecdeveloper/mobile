import * as React from "react";
import { Image } from "react-native";

export default function BookMark(props) {
  return (
    <Image
      style={{
        width: props.width ? props.width : 19,
        height: props.height ? props.height : 19,
      }}
      source={require("../assets/images/bookmark.png")}
    />
  );
}
