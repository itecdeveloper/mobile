import * as React from "react";
import { Image } from "react-native";

export default function Important(props) {
  return (
    <Image
      style={{
        width: props.width ? props.width : 28,
        height: props.height ? props.height : 28,
      }}
      source={require("../assets/images/important.png")}
    />
  );
}
