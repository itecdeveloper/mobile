import * as React from "react";
import { Image } from "react-native";

export default function BackBlack(props) {
  return (
    <Image
      style={{
        width: props.width ? props.width : 16,
        height: props.height ? props.height : 16,
      }}
      source={require("../assets/images/bell.png")}
    />
  );
}
