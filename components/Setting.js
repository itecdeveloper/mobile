import * as React from "react";
import { Image } from "react-native";
import { Ionicons } from "@expo/vector-icons";

export default function Setting(props) {
  return <Ionicons name={"ios-settings"} size={30} color="#4d4d4d" />;
}
