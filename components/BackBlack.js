import * as React from "react";
import { Image } from "react-native";

export default function BackBlack(props) {
  return (
    <Image
      style={{
        width: props.width ? props.width : 5,
        height: props.height ? props.height : 10,
      }}
      source={require("../assets/images/back-black.png")}
    />
  );
}
