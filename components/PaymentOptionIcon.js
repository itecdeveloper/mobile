import * as React from "react";
import { Image } from "react-native";

export default function PaymentOptionIcon(props) {
  let icon;
  switch (props.name) {
    case "mastercard":
      icon = require("../assets/images/mastercard.png");
      break;
    case "visa":
      icon = require("../assets/images/visa.png");
      break;
    case "american":
      icon = require("../assets/images/american.png");
      break;
    case "jcb":
      icon = require("../assets/images/jcb.png");
      break;
    case "unionpay":
      icon = require("../assets/images/unionpay.png");
      break;
    case "discovercard":
      icon = require("../assets/images/discovercard.png");
      break;
    case "dinersclub":
      icon = require("../assets/images/dinersclub.png");
      break;
    default:
      icon = require("../assets/images/promtpay.png");
  }
  return (
    <Image
      style={[
        {
          width: props.width ? props.width : 29,
          height: props.height ? props.height : 19,
        },
        props.style,
      ]}
      source={icon}
    />
  );
}
