import React, { Component } from "react";
import { View, TouchableOpacity } from "react-native";
import { InterRegularTextInput } from "../StyledText";

export default class LoginTextInput extends Component {
  constructor(props) {
    super(props);

    this.onSubmitEmail = this.onSubmitEmail.bind(this);
    this.onSubmitPassword = this.onSubmitPassword.bind(this);

    this.emailRef = this.updateRef.bind(this, "email");
    this.passwordRef = this.updateRef.bind(this, "password");
  }
  updateRef(name, ref) {
    this[name] = ref;
  }

  onSubmitEmail() {
    this.password.focus();
  }
  onSubmitPassword() {}

  componentDidMount() {}

  render() {
    return (
      <View>
        {InterRegularTextInput({
          label: "Email",
          ref: this.emailRef,
          keyboardType: "email-address",
          returnKeyType: "next",
          onSubmitEditing: this.onSubmitEmail,
          showError: false,
        })}
        {InterRegularTextInput({
          label: "Password",
          ref: this.passwordRef,
          secureTextEntry: true,
          showError: false,
        })}
      </View>
    );
  }
}
