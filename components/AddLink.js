import * as React from "react";
import { TouchableOpacity } from "react-native";
import { Ionicons } from "@expo/vector-icons";

export default function AddLink(props) {
  return <Ionicons name="ios-add-circle-outline" size={30} color="#999999" />;
}
