import * as React from "react";
import { Image, View } from "react-native";
import { InterBoldText } from "./StyledText";
import { Language } from "../constants/language";

export default function Note(props) {
  return (
    <View>
      <Image
        style={{
          width: props.width ? props.width : 158,
          height: props.height ? props.height : 36,
        }}
        source={require("../assets/images/note.png")}
      />
      <View
        style={{
          position: "absolute",
          width: props.width ? props.width : 158,
          height: props.height ? props.height : 30,
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <InterBoldText
          text={props.text ? props.text : Language.EN.regis.textError}
          style={{
            color: "#ffffff",
            fontSize: props.fontSize ? props.fontSize : 13,
            marginLeft: 4,
          }}
        />
      </View>
    </View>
  );
}
