import React, { Component } from "react";
import { TouchableOpacity, ActivityIndicator, View } from "react-native";
import { InterBoldText } from "../../components/StyledText";
import BookMark from "../BookMark";
import Watch from "../Watch";
import WatchUnactive from "../WatchUnactive";

export default function Button(props) {
  return (
    <TouchableOpacity
      style={[
        {
          width: "87%",
          marginTop: 16,
          height: 64,
          backgroundColor: "#cccccc",
          borderRadius: 5,
          justifyContent: "center",
          alignItems: "center",
        },
        props.style,
      ]}
      activeOpacity={props.activeOpacity ? props.activeOpacity : 0.2}
      onPress={props.onPress}
    >
      {props.isLoad ? (
        <View style={{ flex: 1, position: "absolute" }}>
          <ActivityIndicator
            size="large"
            color="#ED1B24"
            style={[{ marginRight: 120 }, props.styleActivityIndicator]}
          />
        </View>
      ) : null}
      {!props.book && !props.reschedule ? (
        <InterBoldText
          text={props.text}
          style={[{ fontSize: 18, color: "#ffffff" }, props.styleText]}
        />
      ) : props.book ? (
        <View
          style={{
            flex: 1,
            position: "absolute",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <BookMark />
          <InterBoldText
            text={props.text}
            style={{ fontSize: 18, color: "#ffffff", marginLeft: 8 }}
          />
        </View>
      ) : (
        <View
          style={{
            flex: 1,
            position: "absolute",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          {props.active ? <Watch /> : <WatchUnactive />}
          <InterBoldText
            text={props.text}
            style={[
              { fontSize: 14, color: "#ffffff", marginLeft: 8 },
              props.styleTextReschedule,
            ]}
          />
        </View>
      )}
    </TouchableOpacity>
  );
}
