import * as React from "react";
import { Image } from "react-native";

export default function NextBlack(props) {
  return (
    <Image
      style={{
        width: props.width ? props.width : 6,
        height: props.height ? props.height : 10,
      }}
      source={require("../assets/images/next-black.png")}
    />
  );
}
