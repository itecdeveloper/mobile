import moment from "moment";
export function checkTime(props) {
  const { type, time, limitTime, exp } = props;
  let result = false;
  switch (type) {
    case "exp":
      if (parseInt(time) > moment().format("x")) {
        result = true;
      }
      break;
    case "bookClass":
      if (
        parseInt(time) >=
        moment(moment().add(2, "day").format("YYYY-MM-DD")).format("x")
      ) {
        if (parseInt(time) <= parseInt(exp)) {
          result = true;
        }
      } else {
        if (
          parseInt(time) >=
          moment(moment().add(1, "day").format("YYYY-MM-DD")).format("x")
        ) {
          const date = moment().format("YYYY-MM-DD");
          const dateTime = `${date} ${limitTime}`;
          const lim = moment(dateTime).format("x");
          const now = moment().format("x");
          if (lim > now) {
            result = true;
          }
        }
      }
      break;
    case "re":
      if (parseInt(time) > moment().add(1, "day").format("x")) {
        result = true;
      }
      break;
    default:
      result = false;
  }

  return result;
}
