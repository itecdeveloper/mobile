import { AsyncStorage } from "react-native";

export const setItem = async (key, data) => {
  let result;
  try {
    await AsyncStorage.setItem(key, JSON.stringify(data));
    result = true;
  } catch (error) {
    // Error saving data
    console.log(error);
    result = false;
  }
  return result;
};

export const getItem = async (key) => {
  let result;
  try {
    const value = await AsyncStorage.getItem(key);
    if (value !== null) {
      // We have data!!
      //  console.log(JSON.parse(value));
      result = JSON.parse(value);
    } else {
      result = "";
    }
  } catch (error) {
    // Error retrieving data
    result = false;
  }
  return result;
};
