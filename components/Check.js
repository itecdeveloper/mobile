import * as React from "react";
import { Image } from "react-native";

export default function Check(props) {
  return (
    <Image
      style={[
        {
          width: props.width ? props.width : 24,
          height: props.height ? props.height : 24,
        },
        props.style,
      ]}
      source={require("../assets/images/check.png")}
    />
  );
}
