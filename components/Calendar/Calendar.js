import React, { useState, useEffect } from "react";

import moment from "moment";
import { View, TouchableOpacity, Text } from "react-native";
import { InterBoldText, InterRegularText } from "../StyledText";
import NextBlack from "../NetxBlack";
import BackBlack from "../BackBlack";
import { Language } from "../../constants/language";

export default function Carlendar(props) {
  const [weekData, setWeekData] = useState({});
  const [weekIndex, setWeekIndex] = useState(0);
  const [active, setActive] = useState(false);
  const [data, setData] = useState(props.data);

  useEffect(() => {
    getWeek();
  }, [weekIndex, active]);

  function getWeek() {
    const week = moment().startOf("isoWeek").week() + weekIndex;
    const startDate = moment().week(week).day(1).format("D");
    const endDate = moment().week(week).day(7).format("D MMMM YYYY");

    let days = [];
    for (let i = 1; i <= 7; i++) {
      let date = moment().week(week).day(i).format("D MMMM YYYY");
      let D = moment().week(week).day(i).format("D");
      let dd = Language.EN.myCourse.day[i - 1];
      let isActive = active
        ? date === active
        : date === moment().format("D MMMM YYYY");
      let dateNow = date === moment().format("D MMMM YYYY");

      let course = [];

      props.data.map((row) => {
        let startTime = moment(row.START_TIME).format("D MMMM YYYY");
        if (row.STATUS_NEW_START_TIME === "approve") {
          startTime = moment(row.NEW_START_TIME).format("D MMMM YYYY");
        }

        if (date === startTime) {
          course.push(row);
        }
      });
      let day = {
        D,
        dd,
        isActive: isActive,
        dateNow: dateNow,
        course,
        date,
      };
      if (isActive) {
        props.onActive(course.length > 0 ? course : false);
      }
      days.push(day);
    }
    const weekDays = {
      days,
      startDate,
      endDate,
    };
    setWeekData(weekDays);
  }

  function getDate() {
    return (
      <View
        style={{
          width: "95%",
          alignItems: "center",
          flexDirection: "row",
          marginTop: 20,
        }}
      >
        {weekData.days
          ? weekData.days.map((day, key) => {
              if (day.isActive) {
                return (
                  <TouchableOpacity
                    key={key}
                    style={{
                      flex: 1,
                      alignItems: "center",
                      backgroundColor: "#ffffff",
                      borderRadius: 10,
                      shadowColor: "#000",
                      shadowOffset: {
                        width: 0,
                        height: 1,
                      },
                      shadowOpacity: 0.2,
                      shadowRadius: 1.41,
                      elevation: 2,
                    }}
                  >
                    <InterBoldText
                      text={day.dd}
                      style={{
                        fontSize: 13,
                        color: day.dateNow ? "#333333" : "#4d4d4d",
                        marginTop: 8,
                      }}
                    />

                    <InterBoldText
                      text={day.D}
                      style={{
                        fontSize: 18,
                        color: day.dateNow ? "#ED1B24" : "#4d4d4d",
                        marginTop: 8,
                        marginBottom: 8,
                      }}
                    />
                    <View
                      style={{
                        backgroundColor: "#ED1B24",
                        height: 6,
                        width: "100%",
                        borderRadius: 1.5,
                      }}
                    />
                  </TouchableOpacity>
                );
              } else {
                return (
                  <TouchableOpacity
                    key={key}
                    style={{ flex: 1, alignItems: "center" }}
                    onPress={() => {
                      setActive(day.date);
                    }}
                  >
                    <InterBoldText
                      text={day.dd}
                      style={{
                        fontSize: 13,
                        color: day.dateNow ? "#333333" : "#808080",
                        marginTop: 8,
                      }}
                    />

                    <InterBoldText
                      text={day.D}
                      style={{
                        fontSize: 18,
                        color: day.dateNow ? "#ED1B24" : "#808080",
                        marginTop: 8,
                        marginBottom: 8,
                      }}
                    />
                    <View
                      style={{
                        height: 6,
                        width: "100%",
                        borderRadius: 1.5,
                        alignItems: "center",
                      }}
                    >
                      {day.course.length > 0 ? (
                        <View
                          style={{
                            borderRadius: 50,
                            backgroundColor: "#ED1B24",
                            padding: 2,
                          }}
                        />
                      ) : null}
                    </View>
                  </TouchableOpacity>
                );
              }
            })
          : null}
      </View>
    );
  }

  return (
    <View style={{ width: "100%", alignItems: "center" }}>
      <View
        style={{
          width: "95%",
          backgroundColor: "#f8f7f7",
          alignItems: "center",
          flexDirection: "row",
        }}
      >
        <InterBoldText
          text={weekData.startDate + " - " + weekData.endDate}
          style={{ fontSize: 20, flex: 2, marginLeft: 15 }}
        />
        <View
          style={{
            flex: 1,
            justifyContent: "flex-end",
            flexDirection: "row",
          }}
        >
          <TouchableOpacity
            style={{
              padding: 20,
            }}
            onPress={() => {
              setWeekIndex(weekIndex - 1);
            }}
          >
            <BackBlack />
          </TouchableOpacity>
          <TouchableOpacity
            style={{ padding: 20 }}
            onPress={() => {
              setWeekIndex(weekIndex + 1);
            }}
          >
            <NextBlack />
          </TouchableOpacity>
        </View>
      </View>
      {getDate()}
    </View>
  );
}
