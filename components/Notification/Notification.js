import React, { useState, useEffect } from "react";
import { Text, View, Button, Vibration, Platform } from "react-native";
import { Notifications } from "expo";
import * as Permissions from "expo-permissions";
import Constants from "expo-constants";
const Notification = (props) => {
  const [expoPushToken, setExpoPushToken] = useState("");
  const [notification, setNotification] = useState({});

  registerForPushNotificationsAsync = async () => {
    if (Constants.isDevice) {
      const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
      );
      let finalStatus = existingStatus;
      if (existingStatus !== "granted") {
        const { status } = await Permissions.askAsync(
          Permissions.NOTIFICATIONS
        );
        finalStatus = status;
      }
      if (finalStatus !== "granted") {
        alert("Failed to get push token for push notification!");
        return;
      }
      let token = await Notifications.getExpoPushTokenAsync();

      setExpoPushToken(token);
    } else {
      alert("Must use physical device for Push Notifications");
    }

    if (Platform.OS === "android") {
      Notifications.createChannelAndroidAsync("default", {
        name: "default",
        sound: true,
        priority: "max",
        vibrate: [0, 250, 250, 250],
      });
    }
  };
  useEffect(() => {
    registerForPushNotificationsAsync();
    Notifications.addListener(handleNotification);
  }, []);
  handleNotification = (notification) => {
    Vibration.vibrate();

    setNotification(notification);
  };
  sendPushNotification = async () => {
    const message = {
      to: expoPushToken,
      sound: "default",
      title: "ITEC",
      body: "HELLO WORL!",
      data: { data: "goes here" },
      _displayInForeground: true,
    };
    const response = await fetch("https://exp.host/--/api/v2/push/send", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Accept-encoding": "gzip, deflate",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(message),
    });
  };
  return (
    <View
      style={{
        flex: 1,
        alignItems: "center",
        justifyContent: "space-around",
      }}
    >
      <View style={{ alignItems: "center", justifyContent: "center" }}>
        <Text>Origin: {notification.origin}</Text>
        <Text>Data: {JSON.stringify(notification.data)}</Text>
        <Text>Token: {expoPushToken}</Text>
      </View>
      <Button
        title={"Press to Send Notification"}
        onPress={() => sendPushNotification()}
      />
    </View>
  );
};
export default Notification;
