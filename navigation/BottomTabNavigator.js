import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import * as React from "react";
import MyCourseIcon from "../components/MyCourseIcon";
import HomeIcon from "../components/HomeIcon";
import HomeScreen from "../screens/HomeScreen";
import MyCourseScreen from "../screens/MyCourseScreen";
import { View } from "react-native";

const BottomTab = createBottomTabNavigator();
const INITIAL_ROUTE_NAME = "Home";

export default function BottomTabNavigator({ navigation, route }) {
  // Set the header title on the parent stack navigator depending on the
  // currently active tab. Learn more in the documentation:
  // https://reactnavigation.org/docs/en/screen-options-resolution.html
  //navigation.setOptions({ headerTitle: getHeaderTitle(route) });

  return (
    <BottomTab.Navigator
      initialRouteName={INITIAL_ROUTE_NAME}
      tabBarOptions={{
        keyboardHidesTabBar: true,
        showLabel: false,
      }}
    >
      <BottomTab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          tabBarIcon: ({ focused }) => <HomeIcon focused={focused} />,
        }}
      />

      <BottomTab.Screen
        name="MyCourse"
        component={MyCourseScreen}
        options={{
          tabBarIcon: ({ focused }) => <MyCourseIcon focused={focused} />,
        }}
      />
    </BottomTab.Navigator>
  );
}

function getHeaderTitle(route) {
  const routeName =
    route.state?.routes[route.state.index]?.name ?? INITIAL_ROUTE_NAME;

  switch (routeName) {
    case "Home":
      return "How to get started";
    case "Links":
      return "Links to learn more";
  }
}
