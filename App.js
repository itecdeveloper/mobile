import * as React from "react";
import { Platform, StatusBar, StyleSheet, View } from "react-native";
import { SplashScreen } from "expo";
import * as Font from "expo-font";
import { Ionicons } from "@expo/vector-icons";
import { NavigationContainer } from "@react-navigation/native";
import Constants from "expo-constants";
import StackNavigator from "./navigation/StackNavigator";
import useLinking from "./navigation/useLinking";
import { Provider, connect } from "react-redux";
import store from "./store";
import Colors from "./constants/Colors";

export default function App(props) {
  const [isLoadingComplete, setLoadingComplete] = React.useState(false);
  const [initialNavigationState, setInitialNavigationState] = React.useState();
  const containerRef = React.useRef();
  const { getInitialState } = useLinking(containerRef);

  // Load any resources or data that we need prior to rendering the app
  React.useEffect(() => {
    async function loadResourcesAndDataAsync() {
      try {
        SplashScreen.preventAutoHide();

        // Load our initial navigation state
        setInitialNavigationState(await getInitialState());

        // Load fonts
        await Font.loadAsync({
          ...Ionicons.font,
          "space-mono": require("./assets/fonts/SpaceMono-Regular.ttf"),
          "Inter-Bold": require("./assets/fonts/Inter-Bold.ttf"),
          "Inter-Light": require("./assets/fonts/Inter-Light.ttf"),
          "Inter-Regular": require("./assets/fonts/Inter-Regular.ttf"),
          Inter: require("./assets/fonts/Inter.ttf"),
        });
      } catch (e) {
        // We might want to provide this error information to an error reporting service
        console.warn(e);
      } finally {
        setLoadingComplete(true);
        SplashScreen.hide();
      }
    }

    loadResourcesAndDataAsync();
  }, []);

  if (!isLoadingComplete && !props.skipLoadingScreen) {
    return null;
  } else {
    return (
      <View style={styles.container}>
        <View style={styles.statusBar} />
        {Platform.OS === "ios" && <StatusBar barStyle="dark-content" />}
        <Provider store={store}>
          <NavigationContainer
            ref={containerRef}
            initialState={initialNavigationState}
          >
            <StackNavigator />
          </NavigationContainer>
        </Provider>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  statusBar: {
    backgroundColor: Colors.statusBar,
    height: 0,
  },
});
