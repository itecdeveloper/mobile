export const set = (value) => {
  return {
    type: "SET_SESSION",
    data: value,
  };
};
export const clear = () => {
  return {
    type: "CLEAR_SESSION",
  };
};

export const setSession = (value) => async (dispatch) => {
  dispatch(set(value));
};
