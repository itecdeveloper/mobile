import { postAPIService } from "./CallApi/CallAPI";
import { Link } from "../constants/ApiSetup";
export const setPage = (value) => {
  return {
    type: "SET_PAGE",
    data: value,
  };
};
export const clearPage = () => {
  return {
    type: "CLEAR_PAGE",
  };
};

export const setBookCoursePage = (value) => async (dispatch) => {
  if (value) {
    dispatch(setPage(value));
  } else {
    dispatch(clearPage());
  }
};

export const bookCourse = async () => {
  let body = {
    COURSE_ID: "a71918bb-1c20-4f90-8aaa-a82dba683425",
    TYPE: "LEVEL#e7c11eac-d4f1-4acb-bdb9-478716c641be#DETAIL",
    DATE: "1590099929342",
    PAYMENT_DETAIL: "pop",
  };
  let token =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9Nczz1geyJzdWIiOnsiaWQiOiJlNWYyNWMwZi1lMzYxLTQ4NjUtOGY0Mi1mNzNkMzllZDc0Y2MiLCJyb2xlIjoiU1RVREVOVCJ9LCJpYXQiOjE1OTE1OTgzNTB9Nczz1gShgyEeUjlwleLHO82Gx8wKvA1LL6AL553rr_u1zim60";
  const url = `${Link}/order`;
  const header = {
    "content-type": "application/json",
    Authorization: token,
  };
  const res = await postAPIService(url, header, JSON.stringify(body));
};
