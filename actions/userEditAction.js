import { putAPIService, getAPIService } from "./CallApi/CallAPI";
import { Link } from "../constants/ApiSetup";

export const setUser = (data) => {
  return {
    type: "SET_USER",
    data,
  };
};
export const clearUser = () => {
  return {
    type: "CLEAR_USER",
  };
};

export const editPropfile = (body, user) => async (dispatch, getState) => {
  const url = `${Link}/account/${user.detail.ACCOUNT_ID}`;
  const header = {
    "content-type": "application/json",
    Authorization: user.token,
  };
  const res = await putAPIService(url, header, JSON.stringify(body));
  let result = false;
  if (res.message.statusCode == 0) {
    dispatch(setUser(res.message.data));
    result = res.message.data;
  }
  return result;
};

export const getUser = (user) => async (dispatch, getState) => {
  const url = `${Link}/me`;
  const header = {
    "content-type": "application/json",
    Authorization: user.token,
  };
  const res = await getAPIService(url, header);
  let result = false;
  if (res.message.data) {
    // dispatch(setAllCourse(res.message));

    result = res.message;
  }
  return result;
};
