import {
  getAPIService,
  postAPIService,
  putAPIService,
  delAPIService,
} from "./CallApi/CallAPI";
import { Link } from "../constants/ApiSetup";

export const setCourseStudent = (data) => {
  return {
    type: "SET_COURSE_STUDENT",
    data,
  };
};
export const clearCourseStudent = () => {
  return {
    type: "CLEAR_COURSE_STUDENT",
  };
};

export const myCourseStudent = (user) => async (dispatch) => {
  const url = `${Link}/order/student`;
  const header = {
    "content-type": "application/json",
    Authorization: user.token,
  };

  const res = await getAPIService(url, header);
  let result = false;

  if (res.message.statusCode == 0) {
    dispatch(setCourseStudent(res.message));
    result = res.message.data;
  }
  return result;
};

export const setCourseTeacher = (data) => {
  return {
    type: "SET_COURSE_TEACHER",
    data,
  };
};
export const clearCourseTeacher = () => {
  return {
    type: "CLEAR_COURSE_TEACHER",
  };
};

export const myCourseTeacher = (user) => async (dispatch) => {
  const url = `${Link}/order/teacher`;
  const header = {
    "content-type": "application/json",
    Authorization: user.token,
  };
  const res = await getAPIService(url, header);
  let result = false;
  if (res.message.data) {
    dispatch(setCourseTeacher(res.message));
    result = res.message.data;
  }
  return result;
};

export const getCourseLevel = async (user, levelID) => {
  const url = `${Link}/course/level/${levelID}`;
  const header = {
    "content-type": "application/json",
    Authorization: user.token,
  };

  const res = await getAPIService(url, header);
  let result = false;
  if (res.message.statusCode == 0) {
    result = res.message.data;
  }
  return result;
};

export const getCourse = async (user, courseID) => {
  const url = `${Link}/course/${courseID}`;
  const header = {
    "content-type": "application/json",
    Authorization: user.token,
  };

  const res = await getAPIService(url, header);
  let result = false;
  if (res.message.statusCode == 0) {
    result = res.message.data;
  }
  return result;
};

export const getAccount = async (user, accountID) => {
  const url = `${Link}/account/${accountID}`;
  const header = {
    "content-type": "application/json",
    Authorization: user.token,
  };

  const res = await getAPIService(url, header);
  let result = false;
  if (res.message.statusCode == 0) {
    result = res.message.data;
  }
  return result;
};

export const createSessions = async (user, orderID, body) => {
  const url = `${Link}/order/${orderID}/sessions`;
  const header = {
    "content-type": "application/json",
    Authorization: user.token,
  };
  const res = await postAPIService(url, header, JSON.stringify(body));
  let result = false;
  if (res.message.statusCode == 0) {
    result = res.message.data;
  }
  return result;
};

export const updateSession = async (user, orderID, sessionID, body) => {
  const url = `${Link}/order/${orderID}/sessions/${sessionID}`;
  const header = {
    "content-type": "application/json",
    Authorization: user.token,
  };
  const res = await putAPIService(url, header, JSON.stringify(body));
  let result = false;
  if (res.message.statusCode == 0) {
    result = res.message.data;
  }
  return result;
};

export const updateOrder = async (user, orderID, body) => {
  const url = `${Link}/order/${orderID}`;
  const header = {
    "content-type": "application/json",
    Authorization: user.token,
  };
  const res = await putAPIService(url, header, JSON.stringify(body));
  let result = false;
  if (res.message.statusCode == 0) {
    result = res.message.data;
  }
  return result;
};

export const getOrder = async (user, orderID) => {
  const url = `${Link}/order/${orderID}`;
  const header = {
    "content-type": "application/json",
    Authorization: user.token,
  };
  const res = await getAPIService(url, header);
  let result = false;
  if (res.message.statusCode == 0) {
    result = res.message.data;
  }
  return result;
};

export const delSession = async (user, orderID, body) => {
  const url = `${Link}/order/${orderID}/sessions`;
  const header = {
    "content-type": "application/json",
    Authorization: user.token,
  };
  const res = await delAPIService(url, header, JSON.stringify(body));
  let result = false;
  if (res.message.statusCode == 0) {
    result = res.message.data;
  }
  return result;
};
