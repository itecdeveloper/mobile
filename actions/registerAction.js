import { postAPIService } from "./CallApi/CallAPI";
import { Link } from "../constants/ApiSetup";

export const setUser = (data) => {
  return {
    type: "SET_USER",
    data,
  };
};
export const clearUser = () => {
  return {
    type: "CLEAR_USER",
  };
};

export const register = (body) => async (dispatch, getState) => {
  const url = `${Link}/account`;
  const header = {
    "content-type": "application/json",
  };
  const res = await postAPIService(url, header, JSON.stringify(body));
  let user = false;
  if (res.message.statusCode === "0") {
    // dispatch(setUser(res.message.data));
    user = true;
  } else {
    user = false;
  }

  return user;
};

export const checkEmail = async (body) => {
  const url = `${Link}/services/email_checker`;
  const header = {
    "content-type": "application/json",
  };
  const res = await postAPIService(url, header, JSON.stringify(body));

  return res.message;
};

export const forgotPassword = async (body) => {
  const url = `${Link}/account/forgot?type=EM_GET`;
  const header = {
    "content-type": "application/json",
  };
  const res = await postAPIService(url, header, JSON.stringify(body));

  return res.message;
};
