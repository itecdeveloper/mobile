export const set = (value) => {
  return {
    type: "SET_COURSE",
    data: value,
  };
};
export const clear = () => {
  return {
    type: "CLEAR_COURSE",
  };
};

export const setCourse = (value) => async (dispatch) => {
  dispatch(set(value));
};
