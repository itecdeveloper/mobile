import { postAPIService, getAPIService } from "./CallApi/CallAPI";
import { Link } from "../constants/ApiSetup";

export const payment = async (body, user) => {
  const url = `${Link}/order`;
  const header = {
    "content-type": "application/json",
    Authorization: user.token,
  };
  const res = await postAPIService(url, header, JSON.stringify(body));
  if (res.message.statusCode === "0") {
    return res.message;
  }
};

export const getHash = async (body, user) => {
  const url = `${Link}/payment/gethash`;
  const header = {
    "content-type": "application/json",
    Authorization: user.token,
  };
  const res = await postAPIService(url, header, JSON.stringify(body));

  return res;
};

export const getOrder = async (orderID, user) => {
  const url = `${Link}/order/${orderID}`;
  const header = {
    "content-type": "application/json",
    Authorization: user.token,
  };
  const res = await getAPIService(url, header);

  return res.message;
};
