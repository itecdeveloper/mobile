import { getAPIService } from "./CallApi/CallAPI";
import { Link } from "../constants/ApiSetup";

export const setAllCourse = (data) => {
  return {
    type: "SET_ALL_COURSE",
    data,
  };
};
export const clearAllCourse = () => {
  return {
    type: "CLEAR_ALL_COURSE",
  };
};

export const getAllCourse = () => async (dispatch, getState) => {
  const url = `${Link}/courses`;
  const header = {
    "content-type": "application/json",
  };
  const res = await getAPIService(url, header);
  let result = false;
  if (res.message.data) {
    dispatch(setAllCourse(res.message));
    result = true;
  }
  return result;
};

export const getInstitute = (name, slug) => async (dispatch, getState) => {
  const url = `${Link}/institute?filter=SLUG&qr=${slug}`;
  const header = {
    "content-type": "application/json",
  };
  const res = await getAPIService(url, header);
  let result = false;
  if (res.message.data) {
    result = res.message.data;
  }
  return result;
};

export const getAllCourseInstitute = (slug) => async (dispatch, getState) => {
  const url = `${Link}/courses?filter=SLUG&qr=${slug}`;
  const header = {
    "content-type": "application/json",
  };
  const res = await getAPIService(url, header);
  let result = false;
  if (res.message.data) {
    result = res.message.data;
  }
  return result;
};

export const getAllInstitute = () => async (dispatch, getState) => {
  const url = `${Link}/institute`;
  const header = {
    "content-type": "application/json",
  };
  const res = await getAPIService(url, header);
  let result = false;
  if (res.message.data) {
    result = res.message.data;
  }
  return result;
};
