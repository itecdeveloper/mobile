
export const getAPIService = async (url, header) => {
  const urlCall = `${url}`
  const headers = buildHeader(header)
  const res = await fetch(urlCall, {
    method: 'GET',
    headers,
  })
    .then(response => {
      return response.json()
    })
    .then(json => {
      if (json.code === 0) {
        return { status: true, message: json }
      } else {
        return { status: false, message: json }
      }
    })
    .catch(ex => {
      return { status: false, message: ex }
    })
  return res
}

export const postAPIService = async (url, header, body) => {
  const headers = buildHeader(header)
  const res = await fetch(url, {
    method: 'POST',
    headers,
    body: body,
  })
    .then(response => {
      return response.json()
    })
    .then(json => {
      if (json.code === 0) {
        return { status: true, message: json }
      } else {
        return { status: false, message: json }
      }
    })
    .catch(ex => {
      console.log('catch', ex)
      return { status: false, message: ex }
    })
  return res
}

export const putAPIService = async (url, header, body) => {
  const headers = buildHeader(header)
  const res = await fetch(url, {
    method: 'PUT',
    headers,
    body: body,
  })
    .then(response => {
      return response.json()
    })
    .then(json => {
      if (json.code === 0) {
        return { status: true, message: json }
      } else {
        return { status: false, message: json }
      }
    })
    .catch(ex => {
      return { status: false, message: ex }
    })
  return res
}

export const delAPIService = async (url, header, body) => {
  const headers = buildHeader(header)
  const res = await fetch(url, {
    method: 'DELETE',
    headers,
    body: body,
  })
    .then(response => {
      return response.json()
    })
    .then(json => {
      if (json.code === 0) {
        return { status: true, message: json }
      } else {
        return { status: false, message: json }
      }
    })
    .catch(ex => {
      return { status: false, message: ex }
    })
  return res
}

export const uploadFile = async (url, header, files) => {
  const headers = buildHeader(header)
  const res = await fetch(url, {
    method: 'POST',
    headers,
    body: files,
  })
    .then(response => {
      return response.json()
    })
    .then(json => {
      if (json.code === 0) {
        return { status: true, message: json }
      } else {
        return { status: false, message: json }
      }
    })
    .catch(ex => {
      return { status: false, message: ex }
    })
  return res
}

export const buildHeader = header => {
  const result = {
    ...header,
  }
  return result
}
export const buildHeaderForUploadFiles = header => {
  const result = {
    ...header,
    'Content-Type': 'multipart/form-data',
  }
  return result
}

export const setHeaderContentLanguage = language => {
  return {
    'Content-Language': language,
    'Accept-Language': language,
  }
}
