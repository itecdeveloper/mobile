import {
  postAPIService,
  delAPIService,
  getAPIService,
} from "./CallApi/CallAPI";
import { Link } from "../constants/ApiSetup";
import { getItem, setItem } from "../components/Offline";

export const setUser = (data) => {
  return {
    type: "SET_USER",
    data,
  };
};
export const clearUser = () => {
  return {
    type: "CLEAR_USER",
  };
};

export const userLogin = (body) => async (dispatch, getState) => {
  const url = `${Link}/auth`;
  const header = {
    "content-type": "application/json",
  };
  const res = await postAPIService(url, header, JSON.stringify(body));
  let user = false;
  if (res.message.statusCode === "0") {
    if (
      res.message.data.detail.TYPE === "STUDENT" ||
      res.message.data.detail.TYPE === "TEACHER"
    ) {
      dispatch(setUser(res.message.data));
      await setItem("USER", res.message.data);
      user = true;
    } else {
      user = false;
    }
  } else {
    user = false;
  }

  return user;
};

export const userLogOut = (user) => async (dispatch, getState) => {
  const url = `${Link}/auth`;
  const header = {
    "content-type": "application/json",
    Authorization: user.token,
  };
  const res = await delAPIService(url, header);
  await setItem("USER", false);
  dispatch(clearUser());
};

export const getUser = (user) => async (dispatch, getState) => {
  const url = `${Link}/me`;
  const header = {
    "content-type": "application/json",
    Authorization: user.token,
  };
  const res = await getAPIService(url, header);
  let result = false;

  if (res.message.data) {
    dispatch(setUser(res.message.data));
    // await setItem("USER", res.message.data);
    result = res.message;
  }
  return result;
};
