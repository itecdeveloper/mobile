const INITIAL_STATE = {
  data: false,
};
const page = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "SET_PAGE":
      return { ...state, data: action.data };
    case "CLEAR_PAGE":
      return INITIAL_STATE;
    default:
      return state;
  }
};
export default page;
