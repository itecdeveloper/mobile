const INITIAL_STATE = {
  data: false,
};
const allcourse = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "SET_ALL_COURSE":
      return { ...state, data: action.data };
    case "CLEAR_ALL_COURSE":
      return INITIAL_STATE;
    default:
      return state;
  }
};
export default allcourse;
