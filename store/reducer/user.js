const INITIAL_STATE = {
  data: false,
};
const user = (state = INITIAL_STATE, action) => {
  const data = action.data;
  switch (action.type) {
    case "SET_USER":
      return { ...state, data };
    case "CLEAR_USER":
      return INITIAL_STATE;
    default:
      return state;
  }
};
export default user;
