const INITIAL_STATE = {
  data: false,
};
const session = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "SET_SESSION":
      return { ...state, data: action.data };
    case "CLEAR_SESSION":
      return INITIAL_STATE;
    default:
      return state;
  }
};
export default session;
