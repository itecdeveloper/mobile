const INITIAL_STATE = {
  data: false,
};
const course = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "SET_COURSE":
      return { ...state, data: action.data };
    case "CLEAR_COURSE":
      return INITIAL_STATE;
    default:
      return state;
  }
};
export default course;
