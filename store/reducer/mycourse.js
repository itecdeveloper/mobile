const INITIAL_STATE = {
  data: false,
};
const mycourse = (state = INITIAL_STATE, action) => {
  const data = action.data;
  switch (action.type) {
    case "SET_COURSE_STUDENT":
      return { ...state, data };
    case "CLEAR_COURSE_STUDENT":
      return INITIAL_STATE;
    case "SET_COURSE_TEACHER":
      return { ...state, data };
    case "CLEAR_COURSE_TEACHER":
      return INITIAL_STATE;
    default:
      return state;
  }
};
export default mycourse;
