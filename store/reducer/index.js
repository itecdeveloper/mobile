import { combineReducers } from "redux";

import bookcousepage from "./bookcousepage";
import allcourse from "./allcourse";
import user from "./user";
import mycourse from "./mycourse";
import course from "./course";
import session from "./session";

const reducer = combineReducers({
  bookcousepage,
  allcourse,
  user,
  mycourse,
  course,
  session,
});
export default reducer;
