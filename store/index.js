import reducer from './reducer'
import { applyMiddleware, compose, createStore } from 'redux'
import thunk from 'redux-thunk'
const middleware = thunk

export default store = createStore(reducer,
    compose(
      applyMiddleware(middleware)
    )
);

