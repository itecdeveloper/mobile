import React, { useState, useEffect } from "react";
import { View, TouchableOpacity, StyleSheet } from "react-native";
import {
  InterRegularTextInput,
  InterRegularText,
  InterBoldText,
} from "../../components/StyledText";
import { Language } from "../../constants/language";
import CustomCheckBox from "../../components/CustomCheckBox";
import Button from "../../components/Button";
import Constants from "expo-constants";
import BackRed from "../../components/BackRed";
import { ScrollView } from "react-native-gesture-handler";
import UseKeyboard from "../../components/UseKeyboard";
import * as registerAction from "../../actions/registerAction";
import ForgotIcon from "../../components/ForgotIcon";
import RequestIcon from "../../components/RequestIcon";

export default function CardComPlete(props) {
  return (
    <View
      style={{
        width: "100%",
        height: "100%",
        marginTop: 70,
        alignItems: "center",
      }}
    >
      <View
        style={{
          height: 259,
          width: "87%",
          backgroundColor: "#ffffff",
          alignItems: "center",
          borderRadius: 10,
          marginBottom: 50,
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.23,
          shadowRadius: 2.62,
          elevation: 3,
        }}
      >
        <RequestIcon style={{ marginTop: 33 }} />
        <InterBoldText
          text={Language.EN.forgotPassword.request}
          style={{ marginTop: 16, color: "#ed1b24", fontSize: 16 }}
        />
        <InterRegularText
          text={Language.EN.forgotPassword.send}
          style={{
            width: "87%",
            textAlign: "center",
            marginTop: 16,
            color: "#4d4d4d",
          }}
        />
        <InterBoldText text={props.email} />
        <TouchableOpacity
          onPress={() => {
            props.onDone();
          }}
        >
          <InterBoldText
            text={Language.EN.forgotPassword.done}
            style={{ marginTop: 30, color: "#ed1b24", fontSize: 16 }}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}
