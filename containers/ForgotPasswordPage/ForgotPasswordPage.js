import React, { useState, useEffect } from "react";
import { View, TouchableOpacity, StyleSheet } from "react-native";
import {
  InterRegularTextInput,
  InterRegularText,
  InterBoldText,
} from "../../components/StyledText";
import { Language } from "../../constants/language";
import CustomCheckBox from "../../components/CustomCheckBox";
import Button from "../../components/Button";
import Constants from "expo-constants";
import BackRed from "../../components/BackRed";
import { ScrollView } from "react-native-gesture-handler";
import UseKeyboard from "../../components/UseKeyboard";
import * as registerAction from "../../actions/registerAction";
import ForgotIcon from "../../components/ForgotIcon";
import CardForgot from "./CardForgot";
import CardComplete from "./CardComplete";

export default function ForgotPasswordPage(props) {
  const [email, setEmail] = useState(false);

  return (
    <View style={{ flex: 1 }}>
      <View style={{ height: Constants.statusBarHeight }} />
      <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
        <View
          style={{
            flex: 1,
            backgroundColor: "#f4f4f4",
            alignItems: "center",
          }}
        >
          <View
            style={{
              marginTop: 17,
              width: "87%",
            }}
          >
            <BackRed
              onPress={() => {
                props.navigation.goBack();
              }}
            />
          </View>
          {!email ? (
            <CardForgot
              getEmail={(value) => {
                setEmail(value);
              }}
            />
          ) : (
            <CardComplete
              onDone={() => {
                props.navigation.goBack();
              }}
              email={email}
            />
          )}
        </View>
      </ScrollView>
    </View>
  );
}
