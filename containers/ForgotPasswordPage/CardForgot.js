import React, { useState, useEffect } from "react";
import { View, TouchableOpacity, StyleSheet } from "react-native";
import {
  InterRegularTextInput,
  InterRegularText,
  InterBoldText,
} from "../../components/StyledText";
import { Language } from "../../constants/language";
import CustomCheckBox from "../../components/CustomCheckBox";
import Button from "../../components/Button";
import Constants from "expo-constants";
import BackRed from "../../components/BackRed";
import { ScrollView } from "react-native-gesture-handler";
import UseKeyboard from "../../components/UseKeyboard";
import * as registerAction from "../../actions/registerAction";
import ForgotIcon from "../../components/ForgotIcon";
import _ from "lodash";

export default function CardForgot(props) {
  const [userName, setUserName] = useState("");
  const [emailError, setEmailError] = useState(false);
  const [messageEmailError, setMessageEmailError] = useState("");
  const [load, setLoad] = useState(false);
  const emailRef = React.createRef();

  async function checkEmail(value) {
    if (value) {
      let format = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if (format.test(value)) {
        let body = { EMAIL: value };
        const res = await registerAction.checkEmail(body);
        if (res.statusCode !== "0") {
          setEmailError(false);
        } else {
          setEmailError(true);
          setMessageEmailError(Language.EN.forgotPassword.error);
        }
      } else {
        setEmailError(true);
        setMessageEmailError(Language.EN.regis.invalid);
      }
    }
  }
  async function forgotPassword(value) {
    setLoad(true);
    let body = { EMAIL: value };
    const res = await registerAction.forgotPassword(body);
    props.getEmail(userName);
  }
  return (
    <View
      style={{
        width: "100%",
        height: "100%",
        marginTop: 70,
        alignItems: "center",
      }}
    >
      <View
        style={{
          height: 304,
          width: "87%",
          backgroundColor: "#ffffff",
          alignItems: "center",
          borderRadius: 10,
          marginBottom: 50,
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.23,
          shadowRadius: 2.62,
          elevation: 3,
        }}
      >
        <View
          style={{
            width: "87%",
            marginTop: 24,
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <ForgotIcon />
          <InterBoldText
            text={Language.EN.forgotPassword.header}
            style={{ marginLeft: 12, color: "#ED1B24" }}
          />
        </View>
        <InterBoldText
          text={Language.EN.forgotPassword.text}
          style={{
            width: "87%",
            textAlign: "center",
            marginTop: 28,
            color: "#4d4d4d",
          }}
        />
        <InterRegularTextInput
          label={Language.EN.forgotPassword.email}
          touchableStyle={{
            borderWidth: 1,
            borderColor: "#cccccc",
          }}
          ref={emailRef}
          keyboardType="email-address"
          showError={true}
          fixError={emailError}
          textError={emailError ? messageEmailError : null}
          onChangeText={(value) => {
            const email = _.toLower(value);
            setUserName(email);
            setEmailError(false);
          }}
          onBlur={() => {
            checkEmail(userName);
          }}
          fontSizeErr={emailError ? 12 : 13}
        />
        <Button
          text={Language.EN.forgotPassword.submit}
          activeOpacity={load ? 1 : userName && !emailError ? 0.2 : 1}
          isLoad={load}
          onPress={() => {
            if (!load && userName && !emailError) {
              forgotPassword(userName);
            }
          }}
          style={{
            backgroundColor: load
              ? "#cccccc"
              : userName && !emailError
              ? "#ED1B24"
              : "#cccccc",
          }}
        />
      </View>
    </View>
  );
}
