import React, { useState, useEffect } from "react";
import { View, Dimensions, TouchableOpacity, FlatList } from "react-native";
import { InterRegularText, InterBoldText } from "../../components/StyledText";
import Constants from "expo-constants";
import CardBox from "./CardBox";
import { Language } from "../../constants/language";

export default function Box(props) {
  let { width, height } = Dimensions.get("window");
  const imageHeight = height * 0.253;
  return (
    <View
      style={{
        width: "100%",
        alignItems: "center",
        marginBottom: 32,
      }}
      key={props.index}
    >
      <InterRegularText
        text={Language.EN.home.by}
        style={{
          width: "87%",
          fontSize: 13,
          color: "#999999",
        }}
      />
      <View style={{ width: "87%" }}>
        <TouchableOpacity
          onPress={() => {
            props.navigation.navigate("Institution", {
              name: props.item.institute,
              course: props.item.data,
              slug: props.item.slug,
            });
          }}
        >
          <InterBoldText
            text={props.item.institute}
            style={{
              color: "#4d4d4d",
              fontSize: 18,
              marginBottom: 12,
            }}
          />
        </TouchableOpacity>
      </View>
      <FlatList
        style={{ width: "100%" }}
        data={props.item.data}
        horizontal={true}
        renderItem={({ item, index }) => (
          <CardBox
            navigation={props.navigation}
            item={item}
            index={index}
            length={props.item.data.length}
            institute={{
              name: props.item.institute,
              course: props.item.data,
              slug: props.item.slug,
            }}
          />
        )}
        keyExtractor={(item, index) => index.toString()}
        disableVirtualization
        showsHorizontalScrollIndicator={false}
      />
    </View>
  );
}
