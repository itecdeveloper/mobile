import * as React from "react";
import {
  createStackNavigator,
  CardStyleInterpolators,
} from "@react-navigation/stack";
import HomePage from "./HomePage";
import InstitutionPage from "./InstitutionPage";
const Stack = createStackNavigator();
export default function Navigator(extraData) {
  return (
    <Stack.Navigator
      initialRouteName={"HomePage"}
      screenOptions={{
        headerShown: false,
        animationEnabled: true,
      }}
    >
      <Stack.Screen
        name="HomePage"
        options={{
          title: "HomePage",
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      >
        {(props) => <HomePage {...props} extraData={extraData} />}
      </Stack.Screen>
      <Stack.Screen
        name="Institution"
        options={{
          title: "Institution",
          // cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      >
        {(props) => <InstitutionPage {...props} extraData={extraData} />}
      </Stack.Screen>
    </Stack.Navigator>
  );
}
