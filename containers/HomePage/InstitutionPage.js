import React, { useState, useEffect } from "react";
import {
  View,
  FlatList,
  ActivityIndicator,
  RefreshControl,
} from "react-native";
import HeaderInstitute from "./HeaderInstitute";
import Card from "./Card";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as getAllCourseAction from "../../actions/getAllCourseAction";
import * as userLoginAction from "../../actions/userLoginAction";
import { getItem, setItem } from "../../components/Offline";
import registerForPushNotificationsAsync from "../../components/Notification/registerForPushNotificationsAsync";
import _ from "lodash";
import { InterBoldText } from "../../components/StyledText";
import { Language } from "../../constants/language";

const InstitutionPage = (props) => {
  const [load, setLoad] = useState(true);
  const [institute, setInstitute] = useState();
  const [refresh, setRefresh] = useState(false);

  useEffect(() => {
    getCourse();
  }, []);

  async function getCourse() {
    const res = await props.getAllCourseAction.getAllCourse();
    if (res) {
      //  setAllCourse(props.allCourse);
    }
    const res1 = await props.getAllCourseAction.getInstitute(
      props.route.params.name,
      props.route.params.slug
    );
    if (res1) {
      setLoad(false);
      setInstitute(res1);
      setRefresh(false);
    }
  }

  function sort(data) {
    let result = [];
    data.map((item) => {
      result.push({
        ...item,
        keySort: `${item.DETAIL.INSTITUTE}${item.DETAIL.COURSE_NAME}`,
      });
    });
    result = _.orderBy(result, ["keySort"], ["asc"]);
    return result;
  }

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: "#F8F7F7",
      }}
    >
      {institute ? (
        <HeaderInstitute data={institute} navigation={props.navigation} />
      ) : null}
      <View
        style={{
          flex: 1,
          alignItems: "center",
        }}
      >
        {load ? (
          <ActivityIndicator
            size="large"
            color="#ED1B24"
            style={{ marginTop: 100 }}
          />
        ) : (
          <FlatList
            refreshControl={
              <RefreshControl
                refreshing={refresh}
                onRefresh={() => {
                  setLoad(true);
                  getCourse();
                }}
              />
            }
            style={{ width: "100%" }}
            data={sort(props.route.params.course)}
            ListHeaderComponent={
              <View
                style={{
                  width: "100%",
                  marginTop: 27,
                  marginBottom: 8,
                  alignItems: "center",
                }}
              >
                <InterBoldText
                  text={`${Language.EN.home.under} ${props.route.params.name}`}
                  style={{ color: "#999999", width: "91.5%" }}
                />
              </View>
            }
            renderItem={({ item, index }) => (
              <Card
                navigation={props.navigation}
                item={item}
                index={index}
                institute={{
                  name: props.route.params.name,
                  course: props.route.params.course,
                  slug: props.route.params.slug,
                }}
                length={props.allCourse.length}
              />
            )}
            keyExtractor={(item, index) => index.toString()}
            disableVirtualization
            showsVerticalScrollIndicator={false}
          />
        )}
      </View>
    </View>
  );
};

const mapStateToProps = (state) => {
  return { allCourse: state.allcourse.data.data };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getAllCourseAction: bindActionCreators(getAllCourseAction, dispatch),
    userLoginAction: bindActionCreators(userLoginAction, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(InstitutionPage);
