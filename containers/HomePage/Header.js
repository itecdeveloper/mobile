import React, { useState, useEffect } from "react";
import { View } from "react-native";
import Logo from "../../components/Logo";
import { Language } from "../../constants/language";
import { InterRegularText } from "../../components/StyledText";
export default function Header(props) {
  return (
    <View style={{ alignItems: "center" }}>
      <View
        style={{
          width: "100%",
          backgroundColor: "#F8F7F7",
          justifyContent: "center",
          alignItems: "center",
          marginBottom: 17,
          marginTop: 8,
        }}
      >
        <Logo />
      </View>
    </View>
  );
}
