import React, { useState, useEffect } from "react";
import { View, Dimensions, TouchableOpacity, Image } from "react-native";
import { InterRegularText, InterBoldText } from "../../components/StyledText";
import { LinearGradient } from "expo-linear-gradient";
import Constants from "expo-constants";

export default function CardBox(props) {
  let { width, height } = Dimensions.get("window");
  const imageHeight = height * 0.17;
  const imageWidth = width * 0.656;
  const viewHeight = height * 0.195;
  const viewWidth = width * 0.72;
  const marginLeft = width * 0.064;
  const arr = props.item.DETAIL.COURSE_NAME.split(" / ");
  return (
    <View
      style={{
        width: viewWidth,
        height: viewHeight,
        //  marginRight: props.index == props.length - 1 ? marginLeft : 0,
        marginLeft: props.index == 0 ? marginLeft : 0,
      }}
      key={props.index}
    >
      <TouchableOpacity
        activeOpacity={0.8}
        style={{
          width: imageWidth,
          height: imageHeight,
          backgroundColor: "#ffffff",
          marginTop: 10,
          borderRadius: 10,
          marginBottom: 24,
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.2,
          shadowRadius: 1.41,

          elevation: 2,
        }}
        onPress={() => {
          props.navigation.navigate("ViewCourse", {
            data: props.item,
            institute: props.institute,
          });
        }}
      >
        <View
          style={{
            width: imageWidth,
            height: imageHeight,
            borderRadius: 10,
          }}
        >
          <Image
            style={{
              width: imageWidth,
              height: imageHeight,
              borderRadius: 10,
            }}
            source={{
              uri: props.item.DETAIL.COURSE_IMG,
            }}
          />
          <LinearGradient
            style={{
              borderRadius: 10,
              position: "absolute",
              width: imageWidth,
              height: imageHeight,
            }}
            colors={["rgba(0,0,0,0)", "rgba(0,0,0,0.75)"]}
          >
            <View
              style={{
                position: "absolute",
                bottom: 12,
                marginLeft: 16,
                marginRight: 16,
              }}
            >
              <InterBoldText
                text={arr[0]}
                style={{ fontSize: 16, color: "#ffffff" }}
                ellipsizeMode="tail"
              />
              {arr[1] ? (
                <InterRegularText
                  text={arr[1]}
                  style={{ fontSize: 13, color: "#ffffff" }}
                  ellipsizeMode="tail"
                />
              ) : null}
            </View>
          </LinearGradient>
        </View>
      </TouchableOpacity>
    </View>
  );
}
