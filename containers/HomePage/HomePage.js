import React, { useState, useEffect } from "react";
import {
  View,
  FlatList,
  ActivityIndicator,
  RefreshControl,
} from "react-native";
import Header from "./Header";
import Card from "./Card";
import Box from "./Box";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as getAllCourseAction from "../../actions/getAllCourseAction";
import * as userLoginAction from "../../actions/userLoginAction";
import { getItem, setItem } from "../../components/Offline";
import registerForPushNotificationsAsync from "../../components/Notification/registerForPushNotificationsAsync";
import _ from "lodash";
import Constants from "expo-constants";
import { useFocusEffect } from "@react-navigation/native";

const HomePage = (props) => {
  const [load, setLoad] = useState(true);
  const [institute, setInstitute] = useState([]);
  const [refresh, setRefresh] = useState(false);

  useEffect(() => {
    getUser();
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      setLoad(true);
      getCourse();
    }, [])
  );

  async function getUser() {
    const user = await getItem("USER");
    if (user) {
      const { EMAIL, PASSWORD } = user.detail;
      const token = await registerForPushNotificationsAsync();
      // console.log(token);
      const body = {
        EMAIL: EMAIL,
        PASSWORD: PASSWORD,
        NOTI_TOKEN: token,
      };
      await props.userLoginAction.userLogin(body);
    }
  }

  async function getCourse() {
    const res = await props.getAllCourseAction.getAllCourse();
    if (res) {
      const res1 = await props.getAllCourseAction.getAllInstitute();
      if (res1) {
        setInstitute(res1.Items);
        setLoad(false);
        setRefresh(false);
      }
    }
  }

  function sort(data) {
    let result = [];
    data.map((item) => {
      let index;
      let name;
      institute.map((data) => {
        if (data.SLUG === item.DETAIL.SLUG) {
          index = data.INDEX;
          name = data.NAME;
        }
      });
      if (!item.DETAIL.HIDE) {
        result.push({
          ...item,
          keySort: `${item.DETAIL.INSTITUTE}${item.DETAIL.COURSE_NAME}`,
          institute: name,
          slug: item.DETAIL.SLUG,
          index: index,
        });
      }
    });

    let groupSchool = _.groupBy(result, "institute");
    let school = [];
    Object.keys(groupSchool).map((row) => {
      school.push({
        institute: row,
        slug: groupSchool[row][0].slug,
        data: groupSchool[row],
        index: groupSchool[row][0].index,
      });
    });

    school = _.orderBy(school, ["index"], ["asc"]);
    return school;
  }

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: "#F8F7F7",
      }}
    >
      <View
        style={{
          backgroundColor: "#F8F7F7",
          height: Constants.statusBarHeight,
        }}
      />
      <Header />
      <View
        style={{
          flex: 1,
          alignItems: "center",
        }}
      >
        {load ? (
          <ActivityIndicator
            size="large"
            color="#ED1B24"
            style={{ marginTop: 30 }}
          />
        ) : (
          <FlatList
            refreshControl={
              <RefreshControl
                refreshing={refresh}
                onRefresh={() => {
                  setLoad(true);
                  getCourse();
                }}
              />
            }
            style={{ width: "100%" }}
            data={sort(props.allCourse)}
            renderItem={({ item, index }) => (
              <Box
                navigation={props.navigation}
                item={item}
                index={index}
                allCourse={props.allCourse.length}
              />
            )}
            keyExtractor={(item, index) => index.toString()}
            disableVirtualization
            showsVerticalScrollIndicator={false}
          />
        )}
      </View>
    </View>
  );
};

const mapStateToProps = (state) => {
  return { allCourse: state.allcourse.data.data };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getAllCourseAction: bindActionCreators(getAllCourseAction, dispatch),
    userLoginAction: bindActionCreators(userLoginAction, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
