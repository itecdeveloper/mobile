import React, { useState, useEffect } from "react";
import { View, Dimensions, TouchableOpacity, Image } from "react-native";
import { InterRegularText, InterBoldText } from "../../components/StyledText";
import Constants from "expo-constants";
import { LinearGradient } from "expo-linear-gradient";

export default function Card(props) {
  let { width, height } = Dimensions.get("window");
  const imageHeight = height * 0.253;

  const arr = props.item.DETAIL.COURSE_NAME.split(" / ");
  return (
    <View style={{ width: "100%", alignItems: "center" }} key={props.index}>
      <TouchableOpacity
        activeOpacity={0.8}
        style={{
          width: "91.5%",
          backgroundColor: "#ffffff",
          // borderWidth: 0.2,
          borderRadius: 10,
          marginBottom: 24,
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.2,
          shadowRadius: 1.41,

          elevation: 2,
        }}
        onPress={() => {
          props.navigation.navigate("ViewCourse", {
            data: props.item,
            institute: props.institute,
          });
        }}
      >
        <View
          style={{
            width: "100%",
            height: imageHeight,
            borderRadius: 10,
          }}
        >
          <Image
            style={{
              width: "100%",
              height: "100%",
              borderRadius: 10,
            }}
            source={{
              uri: props.item.DETAIL.COURSE_IMG,
            }}
          />
          <LinearGradient
            style={{
              borderRadius: 10,
              position: "absolute",
              width: "100%",
              height: "100%",
            }}
            colors={["rgba(0,0,0,0)", "rgba(0,0,0,0.75)"]}
          >
            <View
              style={{
                position: "absolute",
                bottom: 12,
                marginLeft: 16,
                marginRight: 16,
              }}
            >
              <InterBoldText
                text={arr[0]}
                style={{ fontSize: 16, color: "#ffffff" }}
                ellipsizeMode="tail"
              />
              {arr[1] ? (
                <InterRegularText
                  text={arr[1]}
                  style={{ fontSize: 13, color: "#ffffff" }}
                  ellipsizeMode="tail"
                />
              ) : null}
            </View>
          </LinearGradient>
        </View>
      </TouchableOpacity>
    </View>
  );
}
