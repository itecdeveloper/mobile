import React, { useState, useEffect } from "react";
import { View, Dimensions, Image } from "react-native";
import Logo from "../../components/Logo";
import Constants from "expo-constants";
import { Language } from "../../constants/language";
import { InterRegularText, InterBoldText } from "../../components/StyledText";
import BackRed from "../../components/BackRed";
export default function Header(props) {
  let { width, height } = Dimensions.get("window");
  let bannerX = height * 0.13;
  let bannerY = "91.5%"; //width * 0.872;
  return (
    <View
      style={{
        width: "100%",
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        alignItems: "center",
        backgroundColor: "white",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 1,
        },
        shadowOpacity: 0.18,
        shadowRadius: 1.0,
        elevation: 1,
      }}
    >
      <View
        style={{
          backgroundColor: "#F8F7F7",
          height: Constants.statusBarHeight,
        }}
      />
      <View style={{ width: "91.5%", marginTop: 11, marginBottom: 20 }}>
        <BackRed
          onPress={() => {
            props.navigation.goBack();
          }}
        />
      </View>
      <View
        style={{
          width: bannerY,
          height: bannerX,
          borderRadius: 10,
          borderWidth: 1,
          borderColor: "#F1EFEF",
          marginBottom: 16,
        }}
      >
        <Image
          style={{
            width: "100%",
            height: "100%",
            borderRadius: 10,
          }}
          source={{
            uri: props.data.Items[0].IMG_BANNER,
          }}
        />
      </View>
      <InterBoldText
        text={props.data.Items[0].NAME}
        style={{ fontSize: 24, color: "#333333", width: "91.5%" }}
      />
      <InterRegularText
        text={props.data.Items[0].DESC}
        style={{
          fontSize: 13,
          lineHeight: 20,
          color: "#999999",
          width: "91.5%",
          marginTop: 4,
          marginBottom: 16,
        }}
      />
    </View>
  );
}
