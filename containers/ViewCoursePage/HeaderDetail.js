import React, { useState, useEffect } from "react";
import { View, TouchableOpacity } from "react-native";

import { Language } from "../../constants/language";
import { InterBoldText, InterRegularText } from "../../components/StyledText";

const HeaderDetail = (props) => {
  return (
    <View style={[{ width: "87%", marginBottom: 12 }, props.containStyle]}>
      <InterBoldText
        text={props.label ? props.label : ""}
        style={{
          fontSize: 13,
          color: "#999999",
          lineHeight: 20,
        }}
      />

      {props.multi ? (
        <View>
          {props.text.map((data) => {
            return (
              <InterBoldText
                text={props.length ? data.text : data.label}
                style={{
                  fontSize: 14,
                  color: props.textColor ? props.textColor : "#333333",
                }}
              />
            );
          })}
        </View>
      ) : (
        <TouchableOpacity
          activeOpacity={props.onPress ? 0.2 : 1}
          onPress={() => {
            if (props.onPress) {
              props.onPress();
            }
          }}
        >
          <InterBoldText
            text={props.text}
            style={{
              fontSize: 14,
              color: props.textColor ? props.textColor : "#333333",
            }}
          />
        </TouchableOpacity>
      )}
    </View>
  );
};

export default HeaderDetail;
