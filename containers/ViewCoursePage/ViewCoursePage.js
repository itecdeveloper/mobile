import React, { useState, useEffect } from "react";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Platform,
  Dimensions,
  ActivityIndicator,
} from "react-native";
import Constants from "expo-constants";
import ReactNativeParallaxHeader from "../../components/react-native-parallax-header";
import Content from "./Content";
import { Language } from "../../constants/language";
import BackWhite from "../../components/BackWhite";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import ModalBox from "./ModalBox";
import { InterBoldText, InterRegularText } from "../../components/StyledText";
import { ScrollView } from "react-native-gesture-handler";
import Button from "../../components/Button";
import _, { result } from "lodash";
import AwesomeAlert from "react-native-awesome-alerts";
import * as getAllCourseAction from "../../actions/getAllCourseAction";
import { useFocusEffect } from "@react-navigation/native";

const SCREEN_HEIGHT = Dimensions.get("window").height;
const IS_IPHONE_X = SCREEN_HEIGHT === 812 || SCREEN_HEIGHT === 896;
const STATUS_BAR_HEIGHT = Platform.OS === "ios" ? (IS_IPHONE_X ? 44 : 20) : 0;
const HEADER_HEIGHT = Platform.OS === "ios" ? (IS_IPHONE_X ? 88 : 64) : 64;
const NAV_BAR_HEIGHT = HEADER_HEIGHT - STATUS_BAR_HEIGHT;
const HeaderHeight = Dimensions.get("window").height * 0.327;
const headerMinHeight = Constants.statusBarHeight;

function ViewCoursePage(props) {
  // const images = {
  //   background: {
  //     uri: props.route.data.DETAIL.COURSE_IMG,
  //   },
  // };

  const [load, setLoad] = useState(true);
  const [dataCourse, setDataCourse] = useState(props.route.data);
  const [images, setImages] = useState({
    background: {
      uri: props.route.data.DETAIL.COURSE_IMG,
    },
  });

  useFocusEffect(
    React.useCallback(() => {
      getCourse();
    }, [])
  );

  async function getCourse() {
    setLoad(true);
    const res1 = await props.getAllCourseAction.getAllCourseInstitute(
      props.route.data.slug
    );

    if (res1) {
      res1.map((item) => {
        if (item.DETAIL.COURSE_ID === props.route.data.DETAIL.COURSE_ID) {
          setLoad(false);
          setDataCourse(item);
          setImages({
            background: {
              uri: item.DETAIL.COURSE_IMG,
            },
          });
        }
      });
    }
  }

  const [visible, setVisible] = useState(false);
  const [level, setLevel] = useState(false);
  const [error, setError] = useState(false);

  const renderNavBar = () => {
    return (
      <View style={styles.navContainer}>
        <View style={styles.statusBar} />
        <View style={styles.navBar}>
          <TouchableOpacity
            onPress={() => {
              props.navigation.goBack();
            }}
            style={{ flexDirection: "row", alignItems: "center" }}
          >
            <BackWhite />
            <InterBoldText
              text={Language.EN.viewCourse.allCourse}
              style={{ marginLeft: 10, color: "#ffffff" }}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  // let groupAge = _.groupBy(props.route.data.LEVEL, "AGE");
  let dataAge = [];
  //  let age = [];
  let sessionQuantity = 0;
  let sessionTime = 0;
  let session = dataCourse.DETAIL.SESSION_QUANTITY;
  let mins = dataCourse.DETAIL.SESSION_TIME / 60;
  const arr = dataCourse.DETAIL.AGE.split("-");
  let label = "";
  if (arr[0] == 0 && arr[arr.length - 1] == 99) {
    label = Language.EN.viewCourse.allAge;
  } else if (arr[arr.length - 1] == 99) {
    label = `${arr[0]} ${Language.EN.viewCourse.above}`;
  } else {
    label = `${dataCourse.DETAIL.AGE} ${Language.EN.viewCourse.years}`;
  }
  let data = {
    label: label,
    text: `${session} sessions • ${mins} ${Language.EN.viewCourse.min}`,
  };
  dataAge.push(data);
  // arr.map((value) => {
  //   let num = value.replace(" ", "");
  //   age.push({ value: num });
  // });

  // Object.keys(groupAge).map((row) => {
  //   let notHide = [];
  //   // console.log(groupAge[row]);
  //   groupAge[row].map((item) => {
  //     if (!item.HIDE) {
  //       notHide.push(item);
  //     }
  //   });
  //   if (notHide.length > 0) {
  //     let session = notHide[0].SESSION_QUANTITY;
  //     let min = notHide[0].SESSION_TIME / 60;
  //     const arr = row.split("-");
  //     let label = "";
  //     if (arr[0] == 0 && arr[arr.length - 1] == 99) {
  //       label = Language.EN.viewCourse.allAge;
  //     } else if (arr[arr.length - 1] == 99) {
  //       label = `${arr[0]} ${Language.EN.viewCourse.above}`;
  //     } else {
  //       label = `${row} ${Language.EN.viewCourse.years}`;
  //     }
  //     let data = {
  //       label: label,
  //       text: `${session} sessions • ${min} ${Language.EN.viewCourse.min}`,
  //     };
  //     dataAge.push(data);

  //     if (props.user) {
  //       const birthDate = new Date(props.user.detail.DOB);
  //       const difference = Date.now() - birthDate.getTime();
  //       let ages = new Date(difference);
  //       ages = Math.abs(ages.getUTCFullYear() - 1970);
  //       if (arr[0] <= ages && ages <= arr[arr.length - 1]) {
  //         sessionQuantity = notHide[0].SESSION_QUANTITY;
  //         sessionTime = notHide[0].SESSION_TIME;
  //       }
  //     }

  //     arr.map((value) => {
  //       let num = value.replace(" ", "");
  //       age.push({ value: num });
  //     });
  //   }
  // });
  //  age = _.orderBy(age, ["value"], ["asc"]);
  let min = arr[0];
  let max = arr[1];

  function bookCourse() {
    if (props.user) {
      const birthDate = new Date(props.user.detail.DOB);
      const difference = Date.now() - birthDate.getTime();
      let age = new Date(difference);
      age = Math.abs(age.getUTCFullYear() - 1970);
      if (min <= age && age <= max) {
        props.navigation.navigate("Payment", {
          courseID: dataCourse.DETAIL.COURSE_ID,
          min: min,
          max: max,
          sessionQuantity: session,
          sessionTime: dataCourse.DETAIL.SESSION_TIME,
          dataViewCourse: {
            data: dataCourse,
            institute: props.route.institute,
            page: "ViewCourse",
          },
        });
      } else {
        setError(true);
      }
    } else {
      props.navigation.navigate("Payment", {
        courseID: dataCourse.DETAIL.COURSE_ID,
        min: min,
        max: max,
        sessionQuantity: session,
        sessionTime: dataCourse.DETAIL.SESSION_TIME,
        dataViewCourse: {
          data: dataCourse,
          institute: props.route.institute,
          page: "ViewCourse",
        },
      });
    }
  }

  function checkUser() {
    let result = false;
    if (props.user) {
      if (props.user.detail.TYPE === "STUDENT") {
        result = true;
      }
    } else {
      result = true;
    }
    return result;
  }

  return (
    <View style={styles.container}>
      <ModalBox
        onClosed={() => {
          setVisible(false);
        }}
        isOpen={visible}
        level={level}
        user={props.user}
        route={dataCourse}
        onChange={() => {
          // setVisible(false);
        }}
        navigation={props.navigation}
      />
      {load ? (
        <ActivityIndicator
          size="large"
          color="#ED1B24"
          style={{ marginTop: 100 }}
        />
      ) : (
        <ReactNativeParallaxHeader
          headerMinHeight={headerMinHeight}
          headerMaxHeight={HeaderHeight}
          extraScrollHeight={20}
          navbarColor="white"
          backgroundImage={images.background}
          backgroundImageScale={1.2}
          renderNavBar={renderNavBar}
          alwaysShowTitle={false}
          containerStyle={{ backgroundColor: "#f8f7f7" }}
          renderContent={() => (
            <Content
              data={dataCourse}
              allCourse={props.allCourse}
              navigation={props.navigation}
              dataAge={dataAge}
              checkUser={checkUser()}
              onPress={(value) => {
                // setLevel(value);
                // setVisible(true);
              }}
              onPressName={() => {
                props.navigation.navigate("Institution", {
                  name: props.route.institute.name,
                  course: props.route.institute.course,
                  slug: props.route.institute.slug,
                });
              }}
              onPressBuy={checkUser() ? bookCourse : null}
            />
          )}
        />
      )}

      <View
        style={{
          width: "100%",
          height: 0,
          position: "absolute",
          bottom: 0,
          alignItems: "center",
          backgroundColor: "#ffffff",
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 6,
          },
          shadowOpacity: 0.37,
          shadowRadius: 7.49,

          elevation: 12,
        }}
      >
        <InterRegularText
          text={Language.EN.viewCourse.text}
          style={{
            width: "87%",
            textAlign: "center",
            fontSize: 13,
            color: "#999999",
            marginTop: 16,
          }}
        />
        <Button
          text={Language.EN.viewCourse.book}
          style={{ backgroundColor: checkUser() ? "#ED1B24" : "#cccccc" }}
          book={true}
          activeOpacity={checkUser() ? 0.2 : 1}
          onPress={checkUser() ? bookCourse : null}
        />
      </View>
      <AwesomeAlert
        show={error}
        showProgress={false}
        title={Language.EN.viewCourse.title}
        message={Language.EN.viewCourse.message}
        closeOnTouchOutside={true}
        closeOnHardwareBackPress={false}
        showCancelButton={false}
        showConfirmButton={true}
        confirmText={Language.EN.login.confirm}
        confirmButtonColor="#ED1B24"
        titleStyle={{
          fontSize: 18,
          color: "#333333",
          fontFamily: "Inter-Bold",
        }}
        confirmButtonTextStyle={{ fontFamily: "Inter-Bold" }}
        messageStyle={{
          textAlign: "center",
          fontSize: 14,
          color: "#333333",
          fontFamily: "Inter-Regular",
        }}
        onConfirmPressed={() => {
          setError(false);
        }}
        onDismiss={() => {
          setError(false);
        }}
      />
    </View>
  );
}
const mapStateToProps = (state) => {
  return { allCourse: state.allcourse.data.data, user: state.user.data };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getAllCourseAction: bindActionCreators(getAllCourseAction, dispatch),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ViewCoursePage);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
  },
  navContainer: {
    height: HEADER_HEIGHT,
    marginHorizontal: 10,
  },
  statusBar: {
    height: headerMinHeight,
    backgroundColor: "transparent",
  },
  navBar: {
    height: NAV_BAR_HEIGHT,
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "row",
    backgroundColor: "transparent",
  },
});
