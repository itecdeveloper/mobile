import React, { useState, useEffect } from "react";
import { View, Dimensions, TouchableOpacity, Image } from "react-native";
import { InterBoldText, InterRegularText } from "../../components/StyledText";
import Constants from "expo-constants";

export default function Card(props) {
  return (
    <View style={{ width: "100%", alignItems: "center" }} key={props.index}>
      <TouchableOpacity
        style={{
          width: "87%",
          marginTop: 12,
          marginBottom: 12,
          backgroundColor: "#ffffff",
          borderRadius: 10,
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.2,
          shadowRadius: 1.41,
          elevation: 2,
        }}
        onPress={() => {
          props.onPress(props.item);
        }}
        activeOpacity={1}
      >
        <InterBoldText
          text={props.item.LEVEL}
          style={{
            fontSize: 16,
            color: "#4d4d4d",
            marginLeft: 16,
            marginTop: 12,
          }}
        />
        <InterRegularText
          text={props.item.DETAIL}
          style={{
            fontSize: 14,
            color: "#999999",
            marginLeft: 16,
            marginRight: 16,
            marginTop: 4,
            marginBottom: 12,
          }}
        />
      </TouchableOpacity>
    </View>
  );
}
