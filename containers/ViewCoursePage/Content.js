import React, { useState, useEffect } from "react";
import { View, Text, FlatList } from "react-native";
import { InterRegularText, InterBoldText } from "../../components/StyledText";
import OtheCourse from "./OtherCourse/OtherCourse";
import { Language } from "../../constants/language";
import Card from "./Card";
import HeaderDetail from "./HeaderDetail";
import numeral from "numeral";
import _ from "lodash";
import Button from "../../components/Button";

export default function Content(props) {
  const groupLevel = _.groupBy(props.data.LEVEL, "LEVEL");
  let level = [];
  Object.keys(groupLevel).map((row) => {
    if (!groupLevel[row][0].HIDE || groupLevel[row][0].HIDE === "NO") {
      level.push(groupLevel[row][0]);
    }
  });

  const arr = props.data.DETAIL.COURSE_NAME.split(" / ");

  return (
    <View style={{ width: "100%", alignItems: "center" }}>
      <View
        style={{
          width: "100%",
          borderBottomLeftRadius: 20,
          borderBottomRightRadius: 20,
          alignItems: "center",
          backgroundColor: "white",
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.18,
          shadowRadius: 1.0,
          elevation: 1,
          marginBottom: 5,
        }}
      >
        <InterBoldText
          text={arr[0]}
          style={{
            fontSize: 24,
            width: "87%",
            marginTop: 20,
            marginBottom: arr[1] ? 0 : 8,
          }}
        />
        {arr[1] ? (
          <InterBoldText
            text={arr[1]}
            style={{
              fontSize: 24,
              width: "87%",
              marginBottom: 8,
            }}
          />
        ) : null}

        <View style={{ width: "87%", flexDirection: "row" }}>
          <View style={{ flex: 1 }}>
            <HeaderDetail
              multi={true}
              label={Language.EN.viewCourse.age}
              text={props.dataAge}
              containStyle={{ width: "100%" }}
            />
          </View>
          <View>
            <HeaderDetail
              onPress={() => {
                props.onPressName();
              }}
              label={Language.EN.viewCourse.by}
              text={props.data.DETAIL.INSTITUTE}
              containStyle={{ width: "100%", marginRight: "10%" }}
              textColor={"#ED1B24"}
            />
          </View>
        </View>

        <HeaderDetail
          multi={true}
          length={true}
          label={Language.EN.viewCourse.length}
          text={props.dataAge}
        />
        <HeaderDetail
          label={Language.EN.viewCourse.price}
          text={numeral(props.data.DETAIL.PRICE).format(0, 0) + " THB"}
        />

        <Button
          text={Language.EN.viewCourse.buy}
          style={{
            backgroundColor: props.checkUser ? "#ED1B24" : "#cccccc",
            marginTop: 4,
          }}
          styleText={{ fontSize: 16 }}
          activeOpacity={props.checkUser ? 0.2 : 1}
          onPress={() => {
            if (props.checkUser) {
              props.onPressBuy();
            }
          }}
        />
        <InterRegularText
          text={Language.EN.viewCourse.note}
          style={{
            width: "87%",
            textAlign: "center",
            fontSize: 13,
            color: "#808080",
            marginTop: 16,
            marginBottom: 24,
          }}
        />
      </View>

      <InterBoldText
        text={Language.EN.viewCourse.about}
        style={{
          width: "87%",
          fontSize: 18,
          color: "#4D4D4D",
          marginTop: 32,
        }}
      />

      <InterRegularText
        text={props.data.DETAIL.DETAIL}
        style={{
          width: "87%",
          fontSize: 13,
          lineHeight: 20,
          color: "#808080",
          marginTop: 8,
          marginBottom: 8,
        }}
      />
      <InterRegularText
        text={Language.EN.viewCourse.noteAbout}
        style={{
          width: "87%",
          fontSize: 13,
          lineHeight: 20,
          color: "#808080",
          marginTop: 8,
          marginBottom: 24,
        }}
      />
    </View>
  );
}
