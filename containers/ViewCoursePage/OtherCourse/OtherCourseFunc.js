export const OtherCourseFunc = (allCourse, thisCourse) => {
  let result = [];
  allCourse.map((data) => {
    if (data.DETAIL.COURSE_ID !== thisCourse.DETAIL.COURSE_ID) {
      result.push(data);
    }
  });
  return result;
};
