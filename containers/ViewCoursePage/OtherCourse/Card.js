import React, { useState, useEffect } from "react";
import { View, Dimensions, TouchableOpacity, Image } from "react-native";
import { InterBoldText } from "../../../components/StyledText";
import Constants from "expo-constants";

export default function Card(props) {
  let { width, height } = Dimensions.get("window");
  const imageHeight = height * 0.133;
  const imageWidth = width * 0.603;

  return (
    <View
      style={{
        width: imageWidth,
        alignItems: "center",
        marginLeft: 20,
        marginRight: props.index == props.length - 2 ? 20 : 0,
      }}
      key={props.index}
    >
      <TouchableOpacity
        activeOpacity={0.8}
        style={{
          width: imageWidth,
          backgroundColor: "#ffffff",
          borderRadius: 10,
          marginBottom: Constants.statusBarHeight,

          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.2,
          shadowRadius: 1.41,

          elevation: 2,
        }}
        onPress={() => {
          props.navigation.pop();
          props.navigation.navigate("ViewCourse", { data: props.item });
        }}
      >
        <View
          style={{
            width: imageWidth,
            height: imageHeight,
            borderRadius: 10,
          }}
        >
          <Image
            style={{
              width: "100%",
              height: "100%",
              borderRadius: 10,
            }}
            source={{
              uri: props.item.DETAIL.COURSE_IMG,
            }}
          />
          <InterBoldText
            text={props.item.DETAIL.COURSE_NAME}
            style={{
              fontSize: 18,
              marginLeft: 16,
              marginRight: 16,
              position: "absolute",
              bottom: 8,
              color: "#ffffff",
            }}
            ellipsizeMode="tail"
            numberOfLines={1}
          />
        </View>
      </TouchableOpacity>
    </View>
  );
}
