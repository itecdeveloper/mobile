import React, { useState, useEffect } from "react";
import { View, FlatList } from "react-native";
import { InterBoldText } from "../../../components/StyledText";
import { Language } from "../../../constants/language";
import Card from "./Card";
import { OtherCourseFunc } from "./OtherCourseFunc";

export default function OtherCourse(props) {
  const [otherCourse, setOtherCourse] = useState([]);
  useEffect(() => {
    setOtherCourse(OtherCourseFunc(props.allCourse, props.thisCoure));
  }, []);

  return (
    <View
      style={{
        width: "100%",
        alignItems: "center",
        backgroundColor: "#F8F7F7",
        marginBottom: 161,
      }}
    >
      <InterBoldText
        text={Language.EN.viewCourse.other}
        style={{
          marginTop: 21,
          width: "87%",
          marginBottom: 20,
          fontSize: 16,
          color: "#4D4D4D",
        }}
      />

      <FlatList
        style={{ width: "100%" }}
        data={otherCourse}
        horizontal={true}
        renderItem={({ item, index }) => (
          <Card
            navigation={props.navigation}
            item={item}
            index={index}
            length={props.allCourse.length}
            navigation={props.navigation}
          />
        )}
        keyExtractor={(item, index) => index.toString()}
        showsHorizontalScrollIndicator={false}
      />
    </View>
  );
}
