import React, { useState, useEffect } from "react";
import { View, TouchableOpacity, Platform, Dimensions } from "react-native";
import Modal from "react-native-modalbox";
import { InterBoldText, InterRegularText } from "../../components/StyledText";
import { ScrollView } from "react-native-gesture-handler";
import { Language } from "../../constants/language";
import Button from "../../components/Button";

const modalHeight = Dimensions.get("window").height * 0.742;

export default function ModalBox(props) {
  return (
    <Modal
      onClosed={() => {
        props.onClosed();
      }}
      isOpen={props.isOpen}
      position="bottom"
      style={{
        backgroundColor: "white",
        height: modalHeight,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
      }}
      swipeArea={100}
    >
      <View
        style={{
          width: "100%",
          backgroundColor: "#ffffff",
          alignItems: "center",
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
          height: modalHeight,
        }}
      >
        <InterBoldText
          text={props.level.LEVEL}
          style={{
            width: "83%",
            fontSize: 18,
            marginTop: 24,
            color: "#4d4d4d",
          }}
        />
        <InterRegularText
          text={props.level.SESSION_QUANTITY + " Sessions • 13,000 THB"}
          style={{ width: "83%", fontSize: 14, color: "#999999" }}
        />
        <ScrollView
          style={{
            width: "100%",
            marginTop: 24,
            marginBottom: 161,
          }}
          contentContainerStyle={{ alignItems: "center" }}
        >
          <InterRegularText
            text={props.level.DETAIL}
            style={{
              width: "83%",
              fontSize: 14,
              marginBottom: 24,
              color: "#4d4d4d",
            }}
          />
        </ScrollView>
        <View
          style={{
            width: "100%",
            height: 161,
            position: "absolute",
            bottom: 0,
            alignItems: "center",
          }}
        >
          <InterRegularText
            text={Language.EN.viewCourse.text}
            style={{
              width: "87%",
              textAlign: "center",
              fontSize: 13,
              color: "#999999",
              marginTop: 16,
            }}
          />
          <Button
            text={Language.EN.viewCourse.book}
            style={{ backgroundColor: "#ED1B24" }}
            book={true}
            onPress={() => {
              props.onChange();
              props.navigation.navigate("Payment", { course: props.route });
            }}
          />
        </View>
      </View>
    </Modal>
  );
}
