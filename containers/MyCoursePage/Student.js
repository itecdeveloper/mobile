import React, { useState, useEffect } from "react";
import { View, Dimensions, TouchableOpacity, FlatList } from "react-native";
import { Language } from "../../constants/language";
import { InterBoldText, InterRegularText } from "../../components/StyledText";
import PhotoGrid from "../../components/react-native-image-grid";
import Card from "./Card";
import CardPending from "./CardPending";
import _ from "lodash";

const Student = (props) => {
  const { course, user } = props;
  const [session, setSession] = useState(props.session);
  const paid = [];
  const pending = [];
  useEffect(() => {
    setSession(props.session);
  }, [props]);
  course.map((item) => {
    if (item.STATUS === "PAID") {
      paid.push(item);
    } else {
      if (item.STATUS !== "INACTIVE") {
        pending.push(item);
      }
    }
  });

  const renderPending = () => {
    return (
      <View style={{ width: "100%", alignItems: "center" }}>
        <InterBoldText
          text={Language.EN.myCourse.pending}
          style={{
            fontSize: 14,
            color: "#808080",
            width: "87%",
            marginTop: 25,
          }}
        />

        <FlatList
          style={{ width: "100%" }}
          data={pending}
          renderItem={({ item, index }) => (
            <CardPending
              item={item}
              index={index}
              user={user}
              session={session}
              onPress={(item) => {
                props.onPressPending(item);
              }}
            />
          )}
          keyExtractor={(item, index) => index.toString()}
          disableVirtualization
          showsHorizontalScrollIndicator={false}
        />
      </View>
    );
  };

  const renderPaid = () => {
    return (
      <View style={{ width: "100%", alignItems: "center" }}>
        <View
          style={{
            marginTop: 25,
            marginBottom: 12,
            flexDirection: "row",
            width: "87%",
            height: 20,
          }}
        >
          <View style={{ flex: 1 }}>
            <InterBoldText
              text={Language.EN.myCourse.myCourse}
              style={{ fontSize: 14, color: "#808080" }}
            />
          </View>
          <View style={{ flex: 1, alignItems: "flex-end" }}>
            <TouchableOpacity
              onPress={() => {
                props.seeAll(paid);
              }}
            >
              <InterBoldText
                text={Language.EN.myCourse.seeAll}
                style={{ fontSize: 13, color: "#ED1B24" }}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ width: "90%", flex: 1 }}>
          {session ? (
            <PhotoGrid
              data={paid}
              itemsPerRow={2}
              itemMargin={1}
              itemPaddingHorizontal={8}
              renderItem={(item, index) => {
                return (
                  <Card
                    item={item}
                    index={index}
                    user={user}
                    session={session}
                    courseDetail={(data) => {
                      props.courseDetail(data);
                    }}
                    backToPay={(data) => {
                      props.backToPay(data);
                    }}
                    onCancel={(data) => {
                      props.onCancel(data);
                    }}
                  />
                );
              }}
            />
          ) : null}
        </View>
      </View>
    );
  };

  return (
    <View style={{ width: "100%", alignItems: "center" }}>
      {pending.length > 0 ? renderPending() : null}
      {paid.length > 0 ? renderPaid() : null}
    </View>
  );
};

export default Student;
