import React, { useState, useEffect } from "react";
import { View, Dimensions, TouchableOpacity } from "react-native";
import { InterBoldText, InterRegularText } from "../../components/StyledText";
import * as myCourseAction from "../../actions/myCourseAction";

function CardTeacher(props) {
  const [name, setName] = useState("");
  const [level, setLevel] = useState("");
  useEffect(() => {
    async function getCourse() {
      const res = await myCourseAction.getCourse(
        props.user,
        props.item.COURSE_ID
      );
      if (res) {
        setName(res.DETAIL.COURSE_NAME);
      }
    }
    async function getCourseLevel() {
      const res = await myCourseAction.getCourseLevel(
        props.user,
        props.item.LEVEL_ID
      );
      if (res) {
        setLevel(res.LEVEL);
      }
    }
    if (props.item.LEVEL_ID !== "-") {
      getCourseLevel();
    } else {
      setLevel("-");
    }
    getCourse();
  }, []);

  return (
    <View style={{ width: "100%", alignItems: "center" }} key={props.index}>
      <TouchableOpacity
        style={{
          width: "87%",
          height: 51,
          backgroundColor: "#ffffff",
          marginBottom: 16,
          borderRadius: 10,
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.2,
          shadowRadius: 1.41,
          elevation: 2,
          flexDirection: "row",
          alignItems: "center",
        }}
        onPress={() => {
          props.courseDetail({
            user: props.user,
            item: {
              ...props.item,
              courseName: name,
              levelName: level,
            },
          });
        }}
      >
        <InterBoldText
          text={name}
          style={{ marginLeft: 16, color: "#4d4d4d" }}
        />
      </TouchableOpacity>
    </View>
  );
}

export default CardTeacher;
