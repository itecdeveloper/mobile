import React, { useState, useEffect } from "react";
import { View, Dimensions, TouchableOpacity } from "react-native";
import Calendar from "../../components/Calendar";
import Constants from "expo-constants";
import { Language } from "../../constants/language";
import { InterBoldText, InterRegularText } from "../../components/StyledText";
import _ from "lodash";
import Progress from "./Progress";
import * as myCourseAction from "../../actions/myCourseAction";
import moment from "moment";

function CardPending(props) {
  const [name, setName] = useState("");
  const [dataCourse, setDataCourse] = useState("");
  const time = props.item.CREATED_AT + props.item.ORDER_EXPIRATION_TIME * 1000;
  const expiration = moment(time).format("MMMM D • HH:mm");

  useEffect(() => {
    async function getCourse() {
      const res = await myCourseAction.getCourse(
        props.user,
        props.item.COURSE_ID
      );
      if (res) {
        const arr = res.DETAIL.COURSE_NAME.split(" / ");
        let name = arr[0];
        let detail = false;
        if (arr[1]) {
          detail = arr[1];
        }

        let dataAge = [];
        let session = res.DETAIL.SESSION_QUANTITY;
        let mins = res.DETAIL.SESSION_TIME / 60;
        const age = res.DETAIL.AGE.split("-");
        let label = "";
        if (age[0] == 0 && age[age.length - 1] == 99) {
          label = Language.EN.viewCourse.allAge;
        } else if (age[age.length - 1] == 99) {
          label = `${age[0]} ${Language.EN.viewCourse.above}`;
        } else {
          label = `${res.DETAIL.AGE} ${Language.EN.viewCourse.years}`;
        }
        let data = {
          label: label,
          text: `${session} sessions • ${mins} ${Language.EN.viewCourse.min}`,
        };
        dataAge.push(data);
        let item = {
          ...res.DETAIL,
          name,
          detail,
          dataAge,
          within: `${Language.EN.myCourse.within} ${expiration}`,
        };
        setName(name);
        setDataCourse(item);
      }
    }
    getCourse();
  }, []);

  return (
    <View style={{ width: "100%", alignItems: "center" }}>
      <TouchableOpacity
        style={{
          width: "87%",
          backgroundColor: "#ffffff",
          borderRadius: 10,
          marginTop: 8,
          marginBottom: 8,
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.2,
          shadowRadius: 1.41,
          elevation: 2,
        }}
        onPress={() => {
          props.onPress({ item: props.item, dataCourse: dataCourse });
        }}
      >
        <InterBoldText
          text={name}
          style={{
            marginTop: 12,
            marginLeft: 12,
            marginRight: 12,
            color: "#4d4d4d",
            fontSize: 14,
          }}
          ellipsizeMode="tail"
          numberOfLines={1}
        />
        <InterRegularText
          text={`${Language.EN.myCourse.within} ${expiration}`}
          style={{
            marginLeft: 12,
            fontSize: 12,
            color: "#999999",
            marginBottom: 12,
          }}
        />
      </TouchableOpacity>
    </View>
  );
}

export default CardPending;
