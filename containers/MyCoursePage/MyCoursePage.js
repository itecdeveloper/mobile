import React, { useState, useEffect } from "react";
import { View, ActivityIndicator, TouchableOpacity } from "react-native";
import Calendar from "../../components/Calendar";
import Constants from "expo-constants";
import { Language } from "../../constants/language";
import { InterBoldText, InterRegularText } from "../../components/StyledText";
import PhotoGrid from "../../components/react-native-image-grid";
import Card from "./Card";
import Bell from "../../components/Bell";
import NextRed from "../../components/NextRed";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as myCourseAction from "../../actions/myCourseAction";
import * as courseAction from "../../actions/courseAction";
import _ from "lodash";
import moment from "moment";
import Active from "./Active";
import { useFocusEffect } from "@react-navigation/native";
import Student from "./Student";
import Teacher from "./Teacher";
import Setting from "../../components/Setting";
import * as userLoginAction from "../../actions/userLoginAction";
import * as sessionAction from "../../actions/sessionAction";
import ModalBox from "./ModalBox";
import { ScrollView } from "react-native-gesture-handler";

const MyCoursePage = (props) => {
  const [dataDate, setDataDate] = useState(false);
  const [course, setCourse] = useState([]);
  const [groupCourse, setGroupCourse] = useState(false);
  const [typeUser, setTypeUser] = useState(props.user.detail.TYPE);
  const [loadDatadata, setLoadDataDate] = useState(true);
  const [isSetting, setIsSetting] = useState(false);
  const [session, setSession] = useState(false);
  const [dataReq, setDataReq] = useState([]);
  const [dataPen, setDataPen] = useState([]);
  const [dataDen, setDataDen] = useState([]);
  const [teacher, setTeacher] = useState("-");
  const [student, setStudent] = useState("-");
  const [isOpen, setIsOpen] = useState(false);
  const [backToPay, setBackToPay] = useState(false);

  if (props.refreshing) {
    getMyCourse();
  }

  useFocusEffect(
    React.useCallback(() => {
      getMyCourse();
    }, [])
  );

  function getReq(session) {
    let data = [];
    const req = _.reduce(
      session,
      function (sum, n) {
        let re = 0;
        if (n.CREATED_BY !== props.user.detail.ACCOUNT_ID) {
          if (n.STATUS_NEW_START_TIME === "req") {
            re = 1;
            data.push(n);
          }
        }
        return sum + re;
      },
      0
    );

    let datapen = [];
    let dataDen = [];
    const pen = _.reduce(
      session,
      function (sum, n) {
        let pe = 0;
        if (n.CREATED_BY === props.user.detail.ACCOUNT_ID) {
          if (n.STATUS_NEW_START_TIME === "req") {
            pe = 1;
            datapen.push(n);
          }
          if (n.STATUS_NEW_START_TIME === "reject") {
            dataDen.push(n);
          }
        }
        return sum + pe;
      },
      0
    );
    setDataDen(dataDen);
    setDataPen(datapen);
    setDataReq(data);
  }

  async function cancel(orderID) {
    const body = {
      STATUS: "CANCEL",
    };
    const res = await myCourseAction.updateOrder(props.user, orderID, body);
    getMyCourse();
  }

  async function getMyCourse() {
    setGroupCourse(false);
    setCourse(false);
    if (typeUser === "STUDENT") {
      const res = await props.myCourseAction.myCourseStudent(props.user);
      if (res) {
        if (res === "ORDERS NOT FOUND") {
          setGroupCourse([]);
          setCourse([]);
        } else {
          let data = _.orderBy(res.DETAIL, ["CREATED_AT"], ["desc"]);
          let dataCourse = [];
          data.map((item) => {
            if (item.STATUS !== "CANCEL") {
              dataCourse.push(item);
            }
          });
          setGroupCourse(dataCourse);
          let session = [];
          res.SESSIONS.map((item) => {
            if (item.S_STATUS !== "INACTIVE") {
              session.push(item);
            }
          });
          setCourse(session);
          getReq(session);
        }
      } else {
        setGroupCourse([]);
        setCourse([]);
      }
    } else {
      const res = await props.myCourseAction.myCourseTeacher(props.user);
      if (res) {
        if (res === "ORDERS NOT FOUND") {
          setGroupCourse([]);
          setCourse([]);
        } else {
          let data = _.orderBy(res.DETAIL, ["CREATED_AT"], ["desc"]);
          let dataCourse = [];
          data.map((item) => {
            if (item.STATUS !== "CANCEL") {
              dataCourse.push(item);
            }
          });
          let orderDetail = [];
          let session = [];
          for (let i = 0; i < dataCourse.length; i++) {
            const dataOrder = await myCourseAction.getOrder(
              props.user,
              dataCourse[i].ORDER_ID
            );

            let sessionByUser = 0;
            let waitting = 0;

            dataOrder.SESSIONS.map((item) => {
              if (item.TEACHER_ID === props.user.detail.ACCOUNT_ID) {
                sessionByUser = sessionByUser + 1;
                if (item.S_STATUS === "WAITING_APPROVE") {
                  waitting = waitting + 1;
                }
              }
            });
            //  if (sessionByUser > waitting) {
            orderDetail.push(dataOrder.DETAIL);
            session.push(...dataOrder.SESSIONS);
            //}
          }
          setGroupCourse(orderDetail);
          let active = [];
          session.map((item) => {
            if (
              item.S_STATUS !== "INACTIVE" &&
              item.S_STATUS !== "WAITING_APPROVE"
            ) {
              active.push(item);
            }
          });
          setCourse(active);
          getReq(active);
        }
      } else {
        setGroupCourse([]);
        setCourse([]);
      }
    }
  }

  function renderLoad(loadDate) {
    return (
      <ActivityIndicator
        size="large"
        color="#ED1B24"
        style={{ marginTop: loadDate ? 30 : 100, marginBottom: 20 }}
      />
    );
  }

  async function getAccount(accountID, type) {
    const res = await myCourseAction.getAccount(props.user, accountID);
    if (res) {
      if (type === "student") {
        setStudent(`${res.Items[0].FISRT_NAME} ${res.Items[0].LAST_NAME}`);
      } else {
        setTeacher(`${res.Items[0].FISRT_NAME} ${res.Items[0].LAST_NAME}`);
      }
      return `${res.Items[0].FISRT_NAME} ${res.Items[0].LAST_NAME}`;
    }
  }

  function courseUser() {
    let page;
    switch (typeUser) {
      case "STUDENT1":
        page = (
          <Student
            course={groupCourse}
            user={props.user}
            seeAll={() => {
              props.navigation.navigate("Course", {
                groupCourse: groupCourse,
                course: course,
                user: props.user,
              });
            }}
            backToPay={(item) => {
              props.navigation.navigate("Payment", {
                courseID: item.COURSE_ID,
                backToPay: item,
              });
            }}
            courseDetail={(item) => {
              const mySession = _.groupBy(course, "ORDER_ID");

              const data = {
                ...item,
                session: mySession[item.item.ORDER_ID]
                  ? mySession[item.item.ORDER_ID]
                  : [],
              };
              props.courseAction.setCourse(data);
              props.navigation.navigate("CourseDetail", { data: data });
            }}
          />
        );
        break;
      case "TEACHER1":
        page = (
          <Teacher
            course={groupCourse}
            user={props.user}
            courseDetail={(item) => {
              const mySession = _.groupBy(course, "ORDER_ID");

              const data = {
                ...item,
                session: mySession[item.item.ORDER_ID]
                  ? mySession[item.item.ORDER_ID]
                  : [],
              };
              props.courseAction.setCourse(data);
              props.navigation.navigate("CourseDetail", { data: data });
            }}
          />
        );
        break;
      default:
        page = page = (
          <Student
            course={groupCourse}
            user={props.user}
            session={course}
            seeAll={(item) => {
              props.navigation.navigate("Course", {
                groupCourse: item,
                course: course,
                user: props.user,
              });
            }}
            backToPay={(item) => {
              props.navigation.navigate("Payment", {
                courseID: item.COURSE_ID,
                backToPay: item,
              });
            }}
            onCancel={(item) => {
              cancel(item.ORDER_ID);
            }}
            onPressPending={(item) => {
              setBackToPay(item);
              setIsOpen(true);
            }}
            courseDetail={(item) => {
              const mySession = _.groupBy(course, "ORDER_ID");

              const data = {
                ...item,
                session: mySession[item.item.ORDER_ID]
                  ? mySession[item.item.ORDER_ID]
                  : [],
              };
              props.courseAction.setCourse(data);
              props.navigation.navigate("CourseDetail", { data: data });
            }}
          />
        );
    }
    return page;
  }

  const renderHeader = () => {
    return (
      <View style={{ backgroundColor: "#F8F7F7" }}>
        <View
          style={{
            alignItems: "center",
            borderBottomLeftRadius: 20,
            borderBottomRightRadius: 20,
            backgroundColor: "#f8f7f7",
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 1,
            },
            shadowOpacity: 0.2,
            shadowRadius: 1.41,

            elevation: 2,
          }}
        >
          <View style={{ flexDirection: "row", width: "87%", marginTop: 45 }}>
            <View style={{ flexDirection: "row", flex: 1 }}>
              <InterRegularText
                text={Language.EN.myCourse.hello}
                style={{ fontSize: 24 }}
              />
              <InterBoldText
                text={
                  props.user.detail.FISRT_NAME +
                  " " +
                  props.user.detail.LAST_NAME
                }
                style={{ fontSize: 24, marginRight: 30 }}
              />
            </View>
            <View style={{ width: 100 }} />
          </View>
          <InterRegularText
            text={props.user.detail.EMAIL}
            style={{
              fontSize: 13,
              width: "87%",
              color: "rgba(122, 122, 122, 0.7)",
            }}
          />
          <View
            style={{
              justifyContent: "flex-end",
              width: "87%",
              marginTop: 45,
              height: 60,
              position: "absolute",
              flexDirection: "row",
            }}
          >
            {isSetting ? (
              <View
                style={{
                  width: 100,
                  height: 60,

                  borderColor: "#cccccc",
                  borderRadius: 5,
                  backgroundColor: "#ffffff",
                  marginRight: 5,
                  shadowColor: "#000",
                  shadowOffset: {
                    width: 0,
                    height: 1,
                  },
                  shadowOpacity: 0.2,
                  shadowRadius: 1.41,

                  elevation: 2,
                }}
              >
                <TouchableOpacity
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                  onPress={() => {
                    setIsSetting(false);
                    props.navigation.navigate("EditProfile");
                  }}
                >
                  <InterRegularText text={Language.EN.myCourse.edit} />
                </TouchableOpacity>
                <View
                  style={{
                    width: "100%",
                    height: 0.5,
                    backgroundColor: "#cccccc",
                  }}
                />
                <TouchableOpacity
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                  onPress={() => {
                    setIsSetting(false);
                    props.userLoginAction.userLogOut(props.user);
                  }}
                >
                  <InterRegularText text={Language.EN.myCourse.signOut} />
                </TouchableOpacity>
              </View>
            ) : null}
            <TouchableOpacity
              onPress={() => {
                setIsSetting(!isSetting);
              }}
            >
              <Setting />
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={{
              width: "87%",
              backgroundColor: "#ffffff",
              borderRadius: 5,
              alignItems: "center",
              marginTop: 12,
              marginBottom: 22,
              flexDirection: "row",
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 1,
              },
              shadowOpacity: 0.2,
              shadowRadius: 1.41,

              elevation: 2,
            }}
            onPress={() => {
              props.navigation.navigate("RescheduleRequest", {
                data: dataReq,
                dataPen: dataPen,
                dataDen: dataDen,
                user: props.user,
                groupCourse: groupCourse,
              });
            }}
          >
            <View
              style={{
                width: 28,
                height: 28,
                borderRadius: 5,
                backgroundColor: "#ED1B24",
                marginTop: 7,
                marginBottom: 7,
                marginLeft: 8,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Bell />
            </View>
            <InterBoldText
              text={dataReq.length + " " + Language.EN.myCourse.rescchedule}
              style={{
                fontSize: 14,
                color: "#ED1B24",
                marginLeft: 10,
                flex: 1,
              }}
            />
            <NextRed style={{ marginRight: 8 }} />
          </TouchableOpacity>

          {course ? (
            <Calendar
              data={course}
              onActive={async (course) => {
                setLoadDataDate(true);
                let dataCourse = [];
                if (course) {
                  for (let i = 0; i < course.length; i++) {
                    let res;
                    if (course[i].LEVEL_ID !== "-") {
                      res = await myCourseAction.getCourseLevel(
                        props.user,
                        course[i].LEVEL_ID
                      );
                      res.isLevel = true;
                    } else {
                      res = await myCourseAction.getCourse(
                        props.user,
                        course[i].COURSE_ID
                      );
                      res = res.DETAIL;
                      res.isLevel = false;
                    }
                    if (i == course.length - 1) {
                      setLoadDataDate(false);
                    }
                    dataCourse.push({
                      ...course[i],
                      ...res,
                    });
                  }
                } else {
                  dataCourse = false;
                  setLoadDataDate(false);
                }

                setDataDate(dataCourse);
              }}
            />
          ) : (
            renderLoad()
          )}
          {course ? (
            loadDatadata ? (
              renderLoad(true)
            ) : dataDate ? (
              <Active
                dataDate={dataDate}
                onPress={async (item) => {
                  if (typeUser === "STUDENT") {
                    let t = await getAccount(item.TEACHER_ID, "teacher");
                    setStudent(
                      `${props.user.detail.FISRT_NAME} ${props.user.detail.LAST_NAME}`
                    );
                    let s = `${props.user.detail.FISRT_NAME} ${props.user.detail.LAST_NAME}`;
                    let groupSession = _.groupBy(course, "ORDER_ID");
                    let exp;
                    Object.keys(groupSession).map((row) => {
                      if (row === item.ORDER_ID) {
                        let sort = _.orderBy(
                          groupSession[row],
                          ["CREATED_AT"],
                          ["asc"]
                        );
                        exp = moment(parseInt(sort[0].CREATED_AT))
                          .add(1, "year")
                          .format("MMMM DD, YYYY");
                      }
                    });

                    const arr = item.COURSE_NAME.split(" / ");
                    let name = arr[0];
                    let detail;
                    if (arr[1]) {
                      detail = arr[1];
                    }

                    const data = {
                      ...item,
                      teacher: t,
                      student: s,
                      levelName: item.isLevel ? item.LEVEL : "-",
                      courseName: name,
                      detail: detail,
                      exp,
                      item: { time: item.CREATE_TICKET_TIME_LIMIT },
                    };
                    // console.log(item.CREATE_TICKET_TIME_LIMIT);
                    props.sessionAction.setSession(data);
                    props.navigation.navigate("Reschedule", { data, data });
                  } else {
                    let s = await getAccount(item.STUDENT_ID, "student");

                    setTeacher(
                      `${props.user.detail.FISRT_NAME} ${props.user.detail.LAST_NAME}`
                    );
                    // let t = `${props.user.detail.FISRT_NAME} ${props.user.detail.LAST_NAME}`;
                    let t = await getAccount(item.TEACHER_ID, "teacher");
                    let groupSession = _.groupBy(course, "ORDER_ID");
                    let exp;
                    Object.keys(groupSession).map((row) => {
                      if (row === item.ORDER_ID) {
                        let sort = _.orderBy(
                          groupSession[row],
                          ["CREATED_AT"],
                          ["asc"]
                        );
                        exp = moment(parseInt(sort[0].CREATED_AT))
                          .add(1, "year")
                          .format("MMMM DD, YYYY");
                      }
                    });
                    const data = {
                      ...item,
                      teacher: t,
                      student: s,
                      levelName: item.LEVEL,
                      courseName: item.COURSE_NAME,
                      exp,
                      item: { time: item.CREATE_TICKET_TIME_LIMIT },
                    };
                    // console.log(item.CREATE_TICKET_TIME_LIMIT);
                    props.sessionAction.setSession(data);
                    props.navigation.navigate("Reschedule", { data, data });
                  }
                }}
              />
            ) : (
              <InterRegularText
                text={Language.EN.myCourse.noClass}
                style={{
                  marginTop: 42,
                  marginBottom: 40,
                  width: "100%",
                  textAlign: "center",
                }}
              />
            )
          ) : null}
        </View>
      </View>
    );
  };

  return (
    <View style={{ flex: 1 }}>
      <View
        style={{
          backgroundColor: "#F8F7F7",
          height: Constants.statusBarHeight,
        }}
      />
      <ScrollView
        contentContainerStyle={{ flexGrow: 1 }}
        showsVerticalScrollIndicator={false}
      >
        <View style={{ flex: 1, backgroundColor: "#F8F7F7" }}>
          {renderHeader()}
          {groupCourse
            ? groupCourse.length > 0
              ? courseUser()
              : null
            : renderLoad()}
        </View>
      </ScrollView>
      <ModalBox
        isOpen={isOpen}
        item={backToPay}
        onClosed={() => {
          setBackToPay(false);
          setIsOpen(false);
        }}
        backToPay={(item) => {
          setIsOpen(false);
          props.navigation.navigate("Payment", {
            courseID: item.COURSE_ID,
            backToPay: item,
          });
        }}
        onCancel={(item) => {
          setIsOpen(false);
          cancel(item.ORDER_ID);
        }}
      />
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.user.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    myCourseAction: bindActionCreators(myCourseAction, dispatch),
    courseAction: bindActionCreators(courseAction, dispatch),
    userLoginAction: bindActionCreators(userLoginAction, dispatch),
    sessionAction: bindActionCreators(sessionAction, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MyCoursePage);
