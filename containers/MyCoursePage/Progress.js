import React from "react";
import { View } from "react-native";
import { InterBoldText } from "../../components/StyledText";
function Progress(props) {
  const success = (props.success * 100) / props.max;
  if (props.success > props.max) {
    success = props.max * 100;
  }
  return (
    <View style={{ marginBottom: 12 }}>
      <View style={{ flexDirection: "row" }}>
        <InterBoldText
          text={props.text}
          style={{ fontSize: 13, color: "#999999", flex: 1 }}
          ellipsizeMode="tail"
          numberOfLines={1}
        />
        <View style={{ flex: 1, alignItems: "flex-end" }}>
          <InterBoldText
            text={`${props.success} / ${props.max}`}
            style={{
              fontSize: 13,
              color: "#999999",
            }}
          />
        </View>
      </View>
      <View
        style={{
          width: "100%",
          height: 6,
          borderRadius: 1.5,
          backgroundColor: "#FDE8E9",
          marginTop: 6,
        }}
      >
        {success != 0 ? (
          <View
            style={{
              height: "100%",
              width: `${success}%`,
              backgroundColor: "#ED1B24",
              borderRadius: 1.5,
            }}
          />
        ) : null}
      </View>
    </View>
  );
}

export default Progress;
