import React, { useState, useEffect } from "react";
import { View, TouchableOpacity, Platform, Dimensions } from "react-native";
import Modal from "react-native-modalbox";
import CheckGrey from "../../components/CheckGrey";
import EditGrey from "../../components/EditGrey";
import {
  InterBoldText,
  InterRegularText,
  InterRegularTextInput,
} from "../../components/StyledText";
import { ScrollView } from "react-native-gesture-handler";
import { Language } from "../../constants/language";
import Button from "../../components/Button";
import Pending from "../../components/Pending";
import HeaderDetail from "./HeaderDetail";
import * as myCourseAction from "../../actions/myCourseAction";
import numeral from "numeral";
import Constants from "expo-constants";

export default function ModalBox(props) {
  let addHeight = 0;
  if (props.item) {
    addHeight = (props.item.dataCourse.COURSE_NAME.length / 40) * 20;
  }
  const modalHeight = 410 + addHeight;
  return (
    <Modal
      onClosed={() => {
        props.onClosed();
      }}
      isOpen={props.isOpen}
      position="bottom"
      coverScreen={true}
      style={{
        backgroundColor: "white",
        height: modalHeight,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
      }}
    >
      <View
        style={{
          flex: 1,
          backgroundColor: "#f8f7f7",
          alignItems: "center",
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
        }}
      >
        <View
          style={{
            width: "87%",
            marginTop: 24,
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Pending />
          <InterBoldText
            text={Language.EN.myCourse.pending}
            style={{ fontSize: 14, color: "#ED1B24", marginLeft: 12 }}
          />
        </View>
        <HeaderDetail
          label={Language.EN.myCourse.course}
          text={props.item ? props.item.dataCourse.COURSE_NAME : null}
          containStyle={{ marginTop: 16 }}
        />
        <HeaderDetail
          label={Language.EN.myCourse.by}
          text={props.item ? props.item.dataCourse.INSTITUTE : null}
          textColor={"#ED1B24"}
        />
        <HeaderDetail
          multi={true}
          label={Language.EN.myCourse.age}
          text={props.item ? props.item.dataCourse.dataAge : null}
        />
        <HeaderDetail
          label={Language.EN.myCourse.length}
          multi={true}
          length={true}
          text={props.item ? props.item.dataCourse.dataAge : null}
        />
        <HeaderDetail
          label={Language.EN.myCourse.price}
          text={
            numeral(props.item ? props.item.dataCourse.PRICE : 0).format(0, 0) +
            " THB"
          }
        />
        <InterRegularText
          text={props.item ? props.item.dataCourse.within : null}
          style={{
            width: "87%",
            marginTop: 12,
            fontSize: 14,
            color: "#999999",
          }}
        />
        <View
          style={{
            height: 46,
            width: "100%",
            marginTop: 14,
            flexDirection: "row",
            position: "absolute",
            bottom:
              Platform.OS === "ios" && Constants.statusBarHeight > 24 ? 24 : 0,
          }}
        >
          <TouchableOpacity
            style={{
              flex: 1,
              borderTopWidth: 1,
              borderColor: "#e6e6e6",
              justifyContent: "center",
              alignItems: "center",
            }}
            onPress={() => {
              props.onCancel(props.item.item);
            }}
          >
            <InterBoldText
              text={Language.EN.myCourse.cancel}
              style={{ color: "#ED1B24" }}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              flex: 1,
              backgroundColor: "#ED1B24",
              justifyContent: "center",
              alignItems: "center",
            }}
            onPress={() => {
              props.backToPay(props.item.item);
            }}
          >
            <InterBoldText
              text={Language.EN.myCourse.buy}
              style={{ color: "#ffffff" }}
            />
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
}
