import React, { useState, useEffect } from "react";
import { View, Dimensions, TouchableOpacity } from "react-native";
import { InterBoldText, InterRegularText } from "../../components/StyledText";
import moment from "moment";
import { Language } from "../../constants/language";
import _ from "lodash";

export default function Active(props) {
  const data = _.orderBy(
    props.dataDate,
    ["STATUS_NEW_START_TIME"] === "approve"
      ? ["STATUS_NEW_START_TIME"]
      : ["START_TIME"],
    ["asc"]
  );
  return (
    <View
      style={{
        width: "100%",
        backgroundColor: "#ffffff",
        borderRadius: 20,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,

        elevation: 2,
      }}
    >
      {data.map((row, index) => {
        let start = moment(row.START_TIME).format("ddd, MMMM D • HH:mm");
        let end = moment(row.END_TIME).format("HH:mm");

        if (row.STATUS_NEW_START_TIME === "approve") {
          start = moment(row.NEW_START_TIME).format("ddd, MMMM D • HH:mm");
          end = moment(row.NEW_START_TIME + row.SESSION_TIME * 1000).format(
            "HH:mm"
          );
        }
        const arr = row.COURSE_NAME.split(" / ");

        return (
          <View style={{ width: "83%" }} key={index}>
            <TouchableOpacity
              onPress={() => {
                props.onPress(row);
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  marginTop: 14,
                }}
              >
                {row.STATUS_NEW_START_TIME === "approve" ? (
                  <View
                    style={{
                      backgroundColor: "#ffffff",
                      borderRadius: 5,
                      marginRight: 8,
                      borderWidth: 1,
                      borderColor: "#f1efef",
                    }}
                  >
                    <InterBoldText
                      text={Language.EN.myCourse.r}
                      style={{
                        color: "#808080",
                        fontSize: 12,
                        marginLeft: 6,
                        marginRight: 6,
                        marginTop: 3,
                        marginBottom: 3,
                      }}
                    />
                  </View>
                ) : null}
                {row.S_STATUS === "WAITING_APPROVE" ||
                row.STATUS_NEW_START_TIME === "req" ? (
                  <View
                    style={{
                      backgroundColor: "#FFEBBA",
                      borderRadius: 5,
                      marginRight: 8,
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <InterBoldText
                      text={Language.EN.myCourse.p}
                      style={{
                        color: "#83610D",
                        fontSize: 12,
                        marginLeft: 6,
                        marginRight: 6,
                        marginTop: 3,
                        marginBottom: 3,
                      }}
                    />
                  </View>
                ) : null}
                {row.STATUS_NEW_START_TIME === "reject" ? (
                  <View
                    style={{
                      backgroundColor: "#FFD8D8",
                      borderRadius: 5,
                      marginRight: 8,
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <InterBoldText
                      text={Language.EN.myCourse.d}
                      style={{
                        color: "#B41B1B",
                        fontSize: 12,
                        marginLeft: 6,
                        marginRight: 6,
                        marginTop: 3,
                        marginBottom: 3,
                      }}
                    />
                  </View>
                ) : null}
                <InterBoldText
                  text={start + " - " + end}
                  style={{
                    color: "#999999",
                    fontSize: 13,
                  }}
                />
              </View>

              <InterBoldText
                text={arr[0]}
                style={{
                  color: "#4d4d4d",
                  marginBottom: 15,
                }}
              />
            </TouchableOpacity>
            {props.dataDate.length > 1 && props.dataDate.length != index + 1 ? (
              <View
                style={{ width: "100%", height: 1, backgroundColor: "#e6e6e6" }}
              />
            ) : null}
          </View>
        );
      })}
    </View>
  );
}
