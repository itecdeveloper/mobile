import React, { useState, useEffect } from "react";
import { View, FlatList, TouchableOpacity } from "react-native";
import { Language } from "../../constants/language";
import { InterBoldText, InterRegularText } from "../../components/StyledText";
import CardTeacher from "./CardTeacher";

const Teacher = (props) => {
  const { course, user } = props;
  return (
    <View style={{ width: "100%", alignItems: "center", marginTop: 47 }}>
      <FlatList
        style={{ width: "100%" }}
        ListHeaderComponent={
          <View style={{ width: "100%", alignItems: "center" }}>
            <InterBoldText
              text={Language.EN.myCourse.going}
              style={{ fontSize: 14, marginBottom: 12, width: "87%" }}
            />
          </View>
        }
        data={course}
        renderItem={({ item, index }) => (
          <CardTeacher
            item={item}
            index={index}
            user={user}
            courseDetail={(data) => {
              props.courseDetail(data);
            }}
          />
        )}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  );
};

export default Teacher;
