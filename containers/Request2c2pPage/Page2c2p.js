import React, { useState, useEffect } from "react";
import { WebView } from "react-native-webview";
import { ActivityIndicator } from "react-native";
const Page2c2p = React.forwardRef((props, ref) => {
  const {
    payment_url,
    version,
    merchant_id,
    currency,
    result_url_1,
    hash_value,
    user_defined_1,
    payment_description,
    order_id,
    amount,
  } = props.data;
  const [link, setLink] = useState("");
  return (
    <WebView
      ref={ref}
      source={{
        html: `<html> 
          <body>
          <form id="myform" method="post" action=${payment_url}>
              <input type="hidden" name="version" value=${version} />
              <input type="hidden" name="merchant_id" value=${merchant_id} />
              <input type="hidden" name="currency" value=${currency} />
              <input type="hidden" name="result_url_1" value=${result_url_1} />
              <input type="hidden" name="hash_value" value=${hash_value} />
              <input type="hidden" name="user_defined_1" value=${user_defined_1} />
          <input type="hidden" name="payment_description" value= '${payment_description}'  readonly/><br/>
              <input type="hidden" name="order_id" value=${order_id}  readonly/><br/>
              <input type="hidden" name="amount" value=${amount} readonly/><br/>
          </form>  
          <script type="text/javascript">
              document.forms.myform.submit();
          </script>
          </body>
          </html>`,
      }}
      style={{ flex: 1, marginTop: 20 }}
      onNavigationStateChange={(navEvent) => {
        if (link !== navEvent.url) {
          let value;
          switch (navEvent.url) {
            case "https://www.eduworld.ac.th/":
              value = { end: true, canGoBack: false };
              break;
            case "https://demo2.2c2p.com/2C2PFrontEnd/RedirectV3/payment/Accept":
              value = { end: false, canGoBack: false };
              break;
            case "https://www.2c2p.com/":
              value = { end: false, canGoBack: false, back: true };
              break;
            default:
              value = value = { end: false, canGoBack: true };
          }
          setLink(navEvent.url);
          props.onRequest(value);
        }
      }}
      onLoadStart={() => {
        props.onLoad(true);
      }}
      onLoadEnd={() => {
        props.onLoad(false);
      }}
    />
  );
});

export default Page2c2p;
