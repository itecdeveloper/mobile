import React, { useState, useEffect } from "react";
import { WebView } from "react-native-webview";
import numeral from "numeral";
import * as paymentAction from "../../actions/paymentAction";
import { View, TouchableOpacity, ActivityIndicator } from "react-native";
import Constants from "expo-constants";
import BackRed from "../../components/BackRed";
import { ScrollView } from "react-native-gesture-handler";
import Page2c2p from "./Page2c2p";

function Request2c2pPage(props) {
  const [data2c2p, setData2c2p] = useState("");
  const [canGoBack, setCanGoBack] = useState(false);
  const [isLoad, setIsLoad] = useState(true);
  const page2c2pRef = React.createRef();
  const bookData = props.route.bookData;
  const dataOrder = props.route.bookData.dataOrder;
  const data = props.route.bookData.data;
  const merchant_id = "764764000003261";
  const payment_description = data.COURSE_NAME;
  let order_id = dataOrder.ORDER_ID.toString().substring(0, 6) + random();
  const currency = "764";
  let num = numeral(dataOrder.PRICE).format("0000000000.00");
  num = num.replace(".", "");
  const amount = num;
  const version = "8.5";
  const payment_url = "https://t.2c2p.com/RedirectV3/payment";
  const result_url_1 = "https://www.eduworld.ac.th";
  const user_defined_1 = dataOrder.ORDER_ID;

  const params =
    version +
    merchant_id +
    payment_description +
    order_id +
    currency +
    amount +
    user_defined_1 +
    result_url_1;

  useEffect(() => {
    getHash();
  }, []);

  function random() {
    const length = 4;
    var result = "";
    var characters = dataOrder.ORDER_ID.split("-").join("");
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  async function getHash() {
    const body = {
      ORDER_ID: user_defined_1,
      PAYLOAD: params,
    };
    let hash = await paymentAction.getHash(body, props.route.bookData.dataUser);

    setData2c2p({
      payment_url,
      version,
      merchant_id,
      currency,
      result_url_1,
      hash_value: hash.message.data,
      user_defined_1,
      payment_description,
      order_id,
      amount,
    });
  }

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: "#ffffff",
      }}
    >
      <View style={{ alignItems: "center" }}>
        <View
          style={{
            marginTop: 17,
            width: "87%",
          }}
        >
          <BackRed
            onPress={() => {
              props.navigation.goBack();
            }}
          />
        </View>
      </View>
      {data2c2p ? (
        <Page2c2p
          ref={page2c2pRef}
          data={data2c2p}
          onRequest={(value) => {
            setCanGoBack(value.canGoBack);
            if (value.end) {
              props.navigation.navigate("Thank", { bookData });
            }
            if (value.back) {
              props.navigation.goBack();
            }
          }}
          onLoad={(value) => {
            setIsLoad(value);
          }}
        />
      ) : null}

      {isLoad ? (
        <View
          style={{
            width: "100%",
            height: "100%",
            position: "absolute",
            alignItems: "center",
            marginTop: 50,
            backgroundColor: "#ffffff",
          }}
        >
          <ActivityIndicator
            size="large"
            color="#ED1B24"
            style={{ marginTop: 100 }}
          />
        </View>
      ) : null}
    </View>
  );
}

export default Request2c2pPage;
