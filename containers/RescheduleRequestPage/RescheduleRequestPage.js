import React, { useState, useEffect } from "react";
import { View, Dimensions, TouchableOpacity } from "react-native";
import { Notifications } from "expo";
import BackRed from "../../components/BackRed";
import { Language } from "../../constants/language";
import { InterBoldText, InterRegularText } from "../../components/StyledText";
import CardWaiting from "./CardWaiting";
import CardPending from "./CardPending";
import * as myCourseAction from "../../actions/myCourseAction";

const RescheduleRequestPage = (props) => {
  const [waiting, setWaiting] = useState(props.route.data);
  const [pending, setPending] = useState(props.route.dataPen);
  const [denied, setDenied] = useState(props.route.dataDen);
  const [course, setCourse] = useState(props.route.groupCourse);

  useEffect(() => {
    getSessionWaiting();
    getSessionPending();
    getSessionDenied();
  }, []);

  const update = async (body) => {
    const res = await myCourseAction.updateSession(
      body.user,
      body.orderID,
      body.sessionID,
      body.body
    );
  };

  async function getSessionWaiting() {
    let data = [];
    for (let i = 0; i < waiting.length; i++) {
      for (let x = 0; x < course.length; x++) {
        if (waiting[i].ORDER_ID === course[x].ORDER_ID) {
          let name;
          let level;
          const couseName = await myCourseAction.getCourse(
            props.route.user,
            course[x].COURSE_ID
          );
          if (couseName) {
            name = couseName.DETAIL.COURSE_NAME;
          }
          const levelName = await myCourseAction.getCourseLevel(
            props.route.user,
            course[x].LEVEL_ID
          );

          if (levelName) {
            level = levelName.LEVEL;
          }

          data.push({
            ...waiting[i],
            name,
            level,
          });
        }
      }
    }
    setWaiting(data);
  }
  async function getSessionPending() {
    let data = [];
    for (let i = 0; i < pending.length; i++) {
      for (let x = 0; x < course.length; x++) {
        if (pending[i].ORDER_ID === course[x].ORDER_ID) {
          let name;
          let level;
          const couseName = await myCourseAction.getCourse(
            props.route.user,
            course[x].COURSE_ID
          );
          if (couseName) {
            name = couseName.DETAIL.COURSE_NAME;
          }
          const levelName = await myCourseAction.getCourseLevel(
            props.route.user,
            course[x].LEVEL_ID
          );

          if (levelName) {
            level = levelName.LEVEL;
          }

          data.push({
            ...pending[i],
            name,
            level,
          });
        }
      }
    }
    setPending(data);
  }

  async function getSessionDenied() {
    console.log(denied);
    let data = [];
    for (let i = 0; i < denied.length; i++) {
      for (let x = 0; x < course.length; x++) {
        if (denied[i].ORDER_ID === course[x].ORDER_ID) {
          let name;
          let level;
          const couseName = await myCourseAction.getCourse(
            props.route.user,
            course[x].COURSE_ID
          );
          if (couseName) {
            name = couseName.DETAIL.COURSE_NAME;
          }
          const levelName = await myCourseAction.getCourseLevel(
            props.route.user,
            course[x].LEVEL_ID
          );

          if (levelName) {
            level = levelName.LEVEL;
          }

          data.push({
            ...denied[i],
            name,
            level,
          });
        }
      }
    }
    setDenied(data);
  }

  return (
    <View style={{ flex: 1, backgroundColor: "#F8F7F7" }}>
      <View
        style={{
          alignItems: "center",
          borderBottomLeftRadius: 20,
          borderBottomRightRadius: 20,
          backgroundColor: "#f8f7f7",
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.2,
          shadowRadius: 1.41,

          elevation: 2,
        }}
      >
        <View
          style={{
            marginTop: 24,
            width: "87%",
          }}
        >
          <BackRed
            text={Language.EN.reschedule.myClass}
            onPress={() => {
              props.navigation.goBack();
            }}
          />
        </View>
        <InterBoldText
          text={Language.EN.reschedule.header}
          style={{
            fontSize: 24,
            color: "#333333",
            marginTop: 24,
            width: "87%",
            marginBottom: 19,
          }}
        />
        <CardWaiting
          data={waiting}
          onDeny={(item, index) => {
            let data = [];
            const body = {
              body: { STATUS_NEW_START_TIME: "reject" },
              user: props.route.user,
              orderID: item.ORDER_ID,
              sessionID: item.SESSION_ID,
            };
            update(body);
            for (var i = 0; i < waiting.length; i++) {
              if (i != index) {
                data.push(waiting[i]);
              }
            }
            setWaiting(data);
          }}
          onApprove={(item, index) => {
            let data = [];
            const body = {
              body: { STATUS_NEW_START_TIME: "approve" },
              user: props.route.user,
              orderID: item.ORDER_ID,
              sessionID: item.SESSION_ID,
            };
            update(body);
            for (var i = 0; i < waiting.length; i++) {
              if (i != index) {
                data.push(waiting[i]);
              }
            }
            setWaiting(data);
            // let datapen = pending;
            // datapen.push(item);
            // setPending(datapen);
          }}
        />
      </View>
      {denied.length > 0 ? (
        <View style={{ width: "100%", alignItems: "center", marginBottom: 20 }}>
          <InterBoldText
            text={Language.EN.reschedule.deniedText}
            style={{
              color: "#808080",
              marginTop: 32,
              width: "87%",
            }}
          />
          <View
            style={{
              width: "87%",
              borderRadius: 10,
              backgroundColor: "#ffffff",
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 1,
              },
              shadowOpacity: 0.2,
              shadowRadius: 1.41,
              elevation: 2,
              marginTop: 9,
            }}
          >
            <CardPending data={denied} />
          </View>
        </View>
      ) : null}

      {pending.length > 0 ? (
        <View style={{ width: "100%", alignItems: "center", marginBottom: 20 }}>
          <InterBoldText
            text={Language.EN.reschedule.pendingText}
            style={{
              color: "#808080",
              marginTop: 32,
              width: "87%",
            }}
          />
          <View
            style={{
              width: "87%",
              borderRadius: 10,
              backgroundColor: "#ffffff",
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 1,
              },
              shadowOpacity: 0.2,
              shadowRadius: 1.41,
              elevation: 2,
              marginTop: 9,
            }}
          >
            <CardPending data={pending} />
          </View>
        </View>
      ) : null}
    </View>
  );
};

export default RescheduleRequestPage;
