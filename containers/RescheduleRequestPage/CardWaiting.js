import React, { useState, useEffect } from "react";
import { View, Dimensions, TouchableOpacity } from "react-native";
import { Language } from "../../constants/language";
import { InterBoldText, InterRegularText } from "../../components/StyledText";
import moment from "moment";
export default function CardWaiting(props) {
  return props.data.map((row, index) => {
    let name = row.name ? row.name : "-";
    const arr = name.split(" / ");
    name = arr[0];
    let level = row.level ? "Lv. " + row.level : "";
    return (
      <View
        key={index}
        style={{
          width: "87%",
          backgroundColor: "#ffffff",
          borderRadius: 10,

          marginBottom: 24,
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.2,
          shadowRadius: 1.41,
          elevation: 2,
        }}
      >
        <InterBoldText
          text={name + " " + level}
          style={{
            marginTop: 16,
            marginLeft: 16,
            fontSize: 18,
            marginRight: 16,
            color: "#333333",
          }}
        />
        <View style={{ width: "100%", flexDirection: "row", marginTop: 12 }}>
          <InterRegularText
            text="Original Date"
            style={{
              flex: 1,
              marginLeft: 16,
              color: "#999999",
              fontSize: 13,
            }}
          />
          <InterRegularText
            text={moment(row.START_TIME).format("ddd, MMMM D • HH:mm")}
            style={{
              flex: 2,
              marginRight: 16,
              color: "#333333",
              fontSize: 13,
            }}
          />
        </View>
        <View style={{ width: "100%", flexDirection: "row", marginTop: 4 }}>
          <InterRegularText
            text="New Date"
            style={{
              flex: 1,
              marginLeft: 16,
              color: "#999999",
              fontSize: 13,
            }}
          />
          <InterBoldText
            text={moment(row.NEW_START_TIME).format("ddd, MMMM D • HH:mm")}
            style={{
              flex: 2,
              marginRight: 16,
              color: "#333333",
              fontSize: 13,
            }}
          />
        </View>
        <View
          style={{
            width: "100%",
            height: 48,
            marginTop: 20,
            borderBottomLeftRadius: 10,
            borderBottomRightRadius: 10,
            flexDirection: "row",
          }}
        >
          <TouchableOpacity
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              borderBottomLeftRadius: 10,
              borderTopWidth: 1,
              borderColor: "#e6e6e6",
            }}
            onPress={() => {
              props.onDeny(row, index);
            }}
          >
            <InterBoldText
              text={Language.EN.reschedule.deny}
              style={{
                color: "#4d4d4d",
                fontSize: 14,
              }}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "#ED1B24",
              borderBottomRightRadius: 10,
            }}
            onPress={() => {
              props.onApprove(row, index);
            }}
          >
            <InterBoldText
              text={Language.EN.reschedule.approve}
              style={{
                color: "#ffffff",
                fontSize: 14,
              }}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  });
}
