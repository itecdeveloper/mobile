import React, { useState, useEffect } from "react";
import { View, Dimensions, TouchableOpacity } from "react-native";
import { Language } from "../../constants/language";
import { InterBoldText, InterRegularText } from "../../components/StyledText";
import moment from "moment";
export default function CardPending(props) {
  return props.data.map((row, index) => {
    let name = row.name ? row.name : "-";
    const arr = name.split(" / ");
    name = arr[0];
    let level = row.level ? "Lv. " + row.level : "";
    return (
      <View
        key={index}
        style={{ borderColor: "#e6e6e6", borderTopWidth: index == 0 ? 0 : 1 }}
      >
        <InterBoldText
          text={name + " " + level}
          style={{
            marginTop: 16,
            marginLeft: 16,
            fontSize: 18,
            marginRight: 16,
            color: "#333333",
          }}
        />
        <View style={{ width: "100%", flexDirection: "row", marginTop: 12 }}>
          <InterRegularText
            text={Language.EN.reschedule.original}
            style={{
              flex: 1,
              marginLeft: 16,
              color: "#999999",
              fontSize: 13,
            }}
          />
          <InterRegularText
            text={moment(row.START_TIME).format("ddd, MMMM D • HH:mm")}
            style={{
              flex: 2,
              marginRight: 16,
              color: "#333333",
              fontSize: 13,
            }}
          />
        </View>
        <View
          style={{
            width: "100%",
            flexDirection: "row",
            marginTop: 4,
            marginBottom: 15,
          }}
        >
          <InterRegularText
            text={Language.EN.reschedule.new}
            style={{
              flex: 1,
              marginLeft: 16,
              color: "#999999",
              fontSize: 13,
            }}
          />
          <InterBoldText
            text={moment(row.NEW_START_TIME).format("ddd, MMMM D • HH:mm")}
            style={{
              flex: 2,
              marginRight: 16,
              color: "#333333",
              fontSize: 13,
            }}
          />
        </View>
      </View>
    );
  });
}
