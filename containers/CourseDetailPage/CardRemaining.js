import React, { useState, useEffect } from "react";
import { View, Dimensions, TouchableOpacity } from "react-native";
import { Language } from "../../constants/language";
import { InterBoldText, InterRegularText } from "../../components/StyledText";
import moment from "moment";
import Button from "../../components/Button";
export default function CardRemaining(props) {
  return (
    <View style={{ width: "100%", alignItems: "center", marginBottom: 33 }}>
      <View
        style={{
          width: "87%",
          borderRadius: 10,
          backgroundColor: "#f8f7f7",
        }}
      >
        <View style={{ flexDirection: "row", alignItems: "flex-end" }}>
          <InterBoldText
            text={Language.EN.courseDetail.remaining}
            style={{
              color: "#4d4d4d",
              fontSize: 16,
              flex: 1,
              marginTop: 16,
            }}
          />
          {props.teacher ? null : (
            <View style={{ flex: 1.5 }}>
              <Button
                style={{
                  marginTop: 0,
                  height: 35,
                  width: 120,
                  backgroundColor: props.isActive ? "#ED1B24" : "#cccccc",
                }}
                activeOpacity={props.isActive ? 0.2 : 1}
                styleText={{ fontSize: 16 }}
                text={Language.EN.courseDetail.book}
                onPress={() => {
                  props.isActive ? props.onBookClass() : null;
                }}
              />
            </View>
          )}
        </View>

        <View
          style={{
            borderRadius: 10,
            backgroundColor: "#ffffff",
            marginTop: 12,
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.23,
            shadowRadius: 2.62,
            elevation: 3,
          }}
        >
          {props.data.map((row, index) => {
            let start = moment(row.START_TIME).format("ddd, MMM D • HH:mm");
            let end = moment(row.END_TIME).format("HH:mm");
            if (row.STATUS_NEW_START_TIME === "approve") {
              start = moment(row.NEW_START_TIME).format("ddd, MMM D • HH:mm");
              let endTime = row.NEW_START_TIME + row.SESSION_TIME * 1000;
              end = moment(endTime).format("HH:mm");
            }
            return (
              <View
                style={{
                  width: "100%",
                  alignItems: "center",
                  marginBottom: 12,
                  borderColor: "#e6e6e6",
                  borderTopWidth: index == 0 ? 0 : 1,
                }}
                key={index}
              >
                <TouchableOpacity
                  style={{
                    width: "100%",

                    flexDirection: "row",
                  }}
                  onPress={() => {
                    props.onPress(row);
                  }}
                >
                  <InterBoldText
                    text={index + props.complete + 1}
                    style={{
                      marginTop: 15,
                      marginLeft: 16,
                      fontSize: 13,
                      color: "#999999",
                    }}
                  />
                  <InterBoldText
                    text={start + " - " + end}
                    style={{
                      marginTop: 15,
                      marginLeft: 16,
                      fontSize: 13,
                      color: "#4d4d4d",
                    }}
                  />
                  <View style={{ flex: 1, alignItems: "flex-end" }}>
                    {row.STATUS_NEW_START_TIME === "approve" ? (
                      <View
                        style={{
                          backgroundColor: "#ffffff",
                          borderRadius: 5,
                          marginTop: 14,
                          marginRight: 8,
                          justifyContent: "center",
                          alignItems: "center",
                          borderWidth: 1,
                          borderColor: "#f1efef",
                        }}
                      >
                        <InterBoldText
                          text={Language.EN.myCourse.r}
                          style={{
                            color: "#808080",
                            fontSize: 12,
                            marginLeft: 6,
                            marginRight: 6,
                            marginTop: 3,
                            marginBottom: 3,
                          }}
                        />
                      </View>
                    ) : row.S_STATUS === "WAITING_APPROVE" ||
                      row.STATUS_NEW_START_TIME === "req" ? (
                      <View
                        style={{
                          backgroundColor: "#FFEBBA",
                          borderRadius: 5,
                          marginTop: 14,
                          marginRight: 8,
                          justifyContent: "center",
                          alignItems: "center",
                        }}
                      >
                        <InterBoldText
                          text={Language.EN.myCourse.p}
                          style={{
                            color: "#83610D",
                            fontSize: 12,
                            marginLeft: 6,
                            marginRight: 6,
                            marginTop: 3,
                            marginBottom: 3,
                          }}
                        />
                      </View>
                    ) : row.STATUS_NEW_START_TIME === "reject" ? (
                      <View
                        style={{
                          backgroundColor: "#FFD8D8",
                          borderRadius: 5,
                          marginTop: 14,
                          marginRight: 8,
                          justifyContent: "center",
                          alignItems: "center",
                        }}
                      >
                        <InterBoldText
                          text={Language.EN.myCourse.d}
                          style={{
                            color: "#B41B1B",
                            fontSize: 12,
                            marginLeft: 6,
                            marginRight: 6,
                            marginTop: 3,
                            marginBottom: 3,
                          }}
                        />
                      </View>
                    ) : (
                      <View
                        style={{
                          backgroundColor: "#D8F4D2",
                          borderRadius: 5,
                          marginTop: 14,
                          marginRight: 8,
                          justifyContent: "center",
                          alignItems: "center",
                        }}
                      >
                        <InterBoldText
                          text={Language.EN.myCourse.a}
                          style={{
                            color: "#2D7720",
                            fontSize: 12,
                            marginLeft: 6,
                            marginRight: 6,
                            marginTop: 3,
                            marginBottom: 3,
                          }}
                        />
                      </View>
                    )}
                  </View>
                </TouchableOpacity>
              </View>
            );
          })}
        </View>
      </View>
    </View>
  );
}
