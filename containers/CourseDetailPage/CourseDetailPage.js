import React, { useState, useEffect } from "react";
import { View, Dimensions, ActivityIndicator } from "react-native";
import BackRed from "../../components/BackRed";
import { Language } from "../../constants/language";
import { InterBoldText, InterRegularText } from "../../components/StyledText";
import CardRemaining from "./CardRemaining";
import CardCompleated from "./CardCompleted";
import HeaderDetail from "./HeaderDetail";
import * as myCourseAction from "../../actions/myCourseAction";
import * as sessionAction from "../../actions/sessionAction";
import Button from "../../components/Button";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import _ from "lodash";
import moment from "moment";
import { useFocusEffect } from "@react-navigation/native";
import { checkTime } from "../../components/Func";

const CourseDetail = (props) => {
  const [remaining, setRemaining] = useState([]);
  const [complete, setComplete] = useState([]);
  const item = props.route.data.item;
  const user = props.route.data.user.detail;
  const [teacher, setTeacher] = useState("-");
  const [student, setStudent] = useState("-");
  const [EXP, setEXP] = useState("");
  const [isLoad, setIsLoad] = useState(true);
  const [session, setSession] = useState("");
  const [bookClass, setBookClass] = useState(false);

  function groupSession(session) {
    const sessionComplete = [];
    const sessionInComplete = [];
    session.map((row) => {
      if (row.S_STATUS === "COMPLETE") {
        sessionComplete.push(row);
      } else {
        sessionInComplete.push(row);
      }
    });
    const re = _.orderBy(addTime(sessionInComplete), ["start"], ["asc"]);
    let newRe = [];
    re.map((item) => {
      if (item.S_STATUS !== "INACTIVE") {
        if (user.TYPE === "STUDENT") {
          newRe.push(item);
        } else {
          if (item.S_STATUS !== "WAITING_APPROVE") {
            newRe.push(item);
          }
        }
      }
    });
    setRemaining(newRe);
    setComplete(sessionComplete);
    let exp = moment(parseInt(item.CREATED_AT))
      .add(1, "year")
      .format("MMMM DD, YYYY");
    if (re.length > 0 || sessionComplete > 0) {
      if (sessionComplete.length > 0) {
        exp = moment(parseInt(sessionComplete[0].CREATED_AT))
          .add(1, "year")
          .format("MMMM DD, YYYY");
      } else {
        exp = moment(parseInt(re[0].CREATED_AT))
          .add(1, "year")
          .format("MMMM DD, YYYY");
      }
    }

    setEXP(exp);
    setBookClass(checkTime({ time: moment(exp).format("x"), type: "exp" }));
  }

  function addTime(data) {
    const item = [];
    data.map((row) => {
      let start = row.START_TIME;
      let end = row.END_TIME;
      if (row.STATUS_NEW_START_TIME === "approve") {
        start = row.NEW_START_TIME;
        let endTime = row.NEW_START_TIME + row.SESSION_TIME * 1000;
        end = endTime;
      }
      item.push({
        ...row,
        start,
        end,
      });
    });
    return item;
  }

  async function getAccount(accountID) {
    const res = await myCourseAction.getAccount(
      props.route.data.user,
      accountID
    );
    if (res) {
      return `${res.Items[0].FISRT_NAME} ${res.Items[0].LAST_NAME}`;
    }
  }

  useFocusEffect(
    React.useCallback(() => {
      setComplete([]), setRemaining([]);
      setIsLoad(true);
      async function getAccount(accountID, type) {
        const res = await myCourseAction.getAccount(
          props.route.data.user,
          accountID
        );
        if (res) {
          if (type === "student") {
            setStudent(`${res.Items[0].FISRT_NAME} ${res.Items[0].LAST_NAME}`);
          } else {
            setTeacher(`${res.Items[0].FISRT_NAME} ${res.Items[0].LAST_NAME}`);
          }
        }
      }

      if (user.TYPE === "STUDENT") {
        getAccount(item.TEACHER_ID, "teacher");
        setStudent(`${user.FISRT_NAME} ${user.LAST_NAME}`);
      } else {
        getAccount(item.STUDENT_ID, "student");
        setTeacher(`${user.FISRT_NAME} ${user.LAST_NAME}`);
      }

      async function getOrder() {
        const res = await myCourseAction.getOrder(
          props.route.data.user,
          item.ORDER_ID
        );
        setIsLoad(false);
        if (res) {
          groupSession(res.SESSIONS);
          setSession(res.SESSIONS);
        }
      }

      getOrder();
    }, [])
  );
  function renderLoad(loadDate) {
    if (isLoad) {
      return (
        <ActivityIndicator
          size="large"
          color="#ED1B24"
          style={{ marginTop: loadDate ? 30 : 100, marginBottom: 20 }}
        />
      );
    }
  }
  return (
    <View style={{ flex: 1, backgroundColor: "#F8F7F7" }}>
      <View
        style={{
          alignItems: "center",
          borderBottomLeftRadius: 20,
          borderBottomRightRadius: 20,
          backgroundColor: "#f8f7f7",
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.2,
          shadowRadius: 1.41,
          elevation: 4,
          marginBottom: 16,
        }}
      >
        <View
          style={{
            marginTop: 24,
            width: "87%",
          }}
        >
          <BackRed
            text={Language.EN.courseDetail.course}
            onPress={() => {
              props.navigation.goBack();
            }}
          />
        </View>
        <InterBoldText
          text={item.courseName}
          style={{
            fontSize: 24,
            color: "#333333",
            marginTop: 17,
            width: "87%",
            marginBottom: 8,
          }}
        />
        <HeaderDetail
          label={Language.EN.courseDetail.level}
          text={item.levelName}
        />
        <HeaderDetail label={Language.EN.courseDetail.teacher} text={teacher} />
        <HeaderDetail label={Language.EN.courseDetail.student} text={student} />
        <HeaderDetail
          label={Language.EN.courseDetail.session}
          text={`${remaining.length + complete.length}/${
            item.SESSION_QUANTITY
          }`}
        />
        <HeaderDetail
          label={Language.EN.courseDetail.exp}
          text={EXP}
          containStyle={{ marginBottom: 12 }}
        />

        <InterBoldText
          text={Language.EN.courseDetail.un}
          style={{
            fontSize: 13,
            color: "#ED1B24",
            marginBottom: 16,
            width: "87%",
          }}
        />
      </View>
      {remaining.length > 0 ? (
        <CardRemaining
          complete={complete.length}
          isActive={
            item.SESSION_QUANTITY > remaining.length + complete.length &&
            bookClass
          }
          data={remaining}
          teacher={user.TYPE !== "STUDENT"}
          onBookClass={() => {
            const body = {
              item: { ...item, exp: EXP },
              session: session,
              user: props.route.data.user,
              book: remaining.length + complete.length,
            };
            props.navigation.navigate("BookClass", { data: body });
          }}
          onPress={async (value) => {
            const teach = await getAccount(value.TEACHER_ID);
            const data = {
              ...value,
              teacher: teach,
              student: student,
              levelName: item.levelName,
              courseName: item.courseName,
              detail: item.detail,
              exp: EXP,
              item: item,
            };

            props.sessionAction.setSession(data);
            props.navigation.navigate("Reschedule", { data, data });
          }}
        />
      ) : user.TYPE !== "STUDENT" ? null : !isLoad ? (
        <View style={{ width: "100%", alignItems: "center" }}>
          <Button
            style={{
              marginTop: 0,
              height: 35,
              width: 120,
              marginBottom: 16,
              backgroundColor:
                remaining.length + complete.length >= item.SESSION_QUANTITY ||
                !bookClass
                  ? "#cccccc"
                  : "#ED1B24",
            }}
            activeOpacity={
              remaining.length + complete.length >= item.SESSION_QUANTITY ||
              !bookClass
                ? 1
                : 0.2
            }
            styleText={{ fontSize: 16 }}
            text={Language.EN.courseDetail.book}
            onPress={() => {
              if (
                remaining.length + complete.length < item.SESSION_QUANTITY &&
                bookClass
              ) {
                const body = {
                  item: { ...item, exp: EXP },
                  session: session,
                  user: props.route.data.user,
                  book: remaining.length + complete.length,
                };
                props.navigation.navigate("BookClass", {
                  data: body,
                });
              }
            }}
          />
        </View>
      ) : null}
      {complete.length > 0 ? (
        <CardCompleated
          teacher={user.TYPE !== "STUDENT"}
          data={complete}
          onPress={async (value) => {
            const teach = await getAccount(value.TEACHER_ID);
            const data = {
              ...value,
              teacher: teach,
              student: student,
              levelName: item.levelName,
              courseName: item.courseName,
              detail: item.detail,
              exp: EXP,
              item: item,
            };

            props.sessionAction.setSession(data);
            props.navigation.navigate("Reschedule", { data, data });
          }}
        />
      ) : null}
      {renderLoad()}
    </View>
  );
};
const mapStateToProps = (state) => {
  return {
    course: state.course.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    sessionAction: bindActionCreators(sessionAction, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CourseDetail);
