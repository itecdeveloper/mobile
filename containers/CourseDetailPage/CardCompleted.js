import React, { useState, useEffect } from "react";
import { View, Dimensions, TouchableOpacity } from "react-native";
import { Language } from "../../constants/language";
import { InterBoldText, InterRegularText } from "../../components/StyledText";
import moment from "moment";
export default function CardCompleted(props) {
  return (
    <View style={{ width: "100%", alignItems: "center", marginBottom: 20 }}>
      <View
        style={{
          width: "87%",
          borderRadius: 10,
          backgroundColor: "#f8f7f7",
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.23,
          shadowRadius: 2.62,
          elevation: 3,
        }}
      >
        <View style={{ flexDirection: "row" }}>
          <InterBoldText
            text={Language.EN.courseDetail.complete}
            style={{
              marginTop: 15,
              marginLeft: 16,

              color: "#808080",
            }}
          />
        </View>
        <View
          style={{
            borderRadius: 10,
            backgroundColor: "#ffffff",
            marginTop: 9,
          }}
        >
          {props.data.map((row, index) => {
            let start = moment(row.START_TIME).format("ddd, MMM D • HH:mm");
            let end = moment(row.END_TIME).format("HH:mm");
            if (row.STATUS_NEW_START_TIME === "approve") {
              start = moment(row.NEW_START_TIME).format("ddd, MMM D • HH:mm");
              let endTime = row.NEW_START_TIME + row.SESSION_TIME * 1000;
              end = moment(endTime).format("HH:mm");
            }
            return (
              <View
                style={{
                  width: "100%",
                  alignItems: "center",
                  marginBottom: 12,
                }}
                key={index}
              >
                <TouchableOpacity
                  style={{
                    width: "100%",
                    borderColor: "#e6e6e6",
                    borderTopWidth: index == 0 ? 0 : 1,
                    flexDirection: "row",
                  }}
                  onPress={() => {
                    props.onPress(row);
                  }}
                >
                  <InterBoldText
                    text={index + 1}
                    style={{
                      marginTop: 15,
                      marginLeft: 16,
                      fontSize: 13,
                      color: "#999999",
                    }}
                  />
                  <InterBoldText
                    text={start + " - " + end}
                    style={{
                      marginTop: 15,
                      marginLeft: 16,
                      fontSize: 13,
                      color: "#4d4d4d",
                    }}
                  />
                </TouchableOpacity>
              </View>
            );
          })}
        </View>
      </View>
    </View>
  );
}
