import React, { useState, useEffect } from "react";
import { View } from "react-native";

import { Language } from "../../constants/language";
import { InterBoldText, InterRegularText } from "../../components/StyledText";

const HeaderDetail = (props) => {
  return (
    <View
      style={[
        { width: "87%", flexDirection: "row", marginBottom: 4 },
        props.containStyle,
      ]}
    >
      <View style={{ flex: 1 }}>
        <InterBoldText
          text={props.label}
          style={{
            fontSize: 13,
            color: "#999999",
          }}
        />
      </View>
      <View style={{ flex: 3 }}>
        <InterBoldText
          text={props.text}
          style={{
            fontSize: 13,
            color: "#4d4d4d",
          }}
        />
      </View>
    </View>
  );
};

export default HeaderDetail;
