import React, { useState, useEffect } from "react";
import { View, Dimensions, TouchableOpacity } from "react-native";
import { Language } from "../../constants/language";
import { InterBoldText, InterRegularText } from "../../components/StyledText";
import CloseGrey from "../../components/CloseGrey";
import Button from "../../components/Button";
import { ScrollView } from "react-native-gesture-handler";
import moment from "moment";
import * as WebBrowser from "expo-web-browser";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { checkTime } from "../../components/Func";
import * as myCourseAction from "../../actions/myCourseAction";
import PendingStatus from "../../components/PendingStatus";
import ApprovedStatus from "../../components/ApprovedStatus";
import DeniedStatus from "../../components/DeniedStatus";
import ComplatetStatus from "../../components/CompleteStatus";

const Default = (props) => {
  const { typeUser, user } = props.extraData;
  const data = props.session;
  const link = data.LINKS;
  let startTime = data.START_TIME;
  if (data.STATUS_NEW_START_TIME === "approve") {
    startTime = data.NEW_START_TIME;
  }
  let endTime = startTime + data.SESSION_TIME * 1000;

  const status = data.STATUS_NEW_START_TIME;
  let startNewTime;
  let endNewTime;
  if (
    data.STATUS_NEW_START_TIME === "req" ||
    data.STATUS_NEW_START_TIME === "reject"
  ) {
    startNewTime = moment(data.NEW_START_TIME).format("ddd, MMM D • HH:mm");
    endNewTime = moment(data.NEW_START_TIME + data.SESSION_TIME * 1000).format(
      "HH:mm"
    );
  }

  const start = moment(startTime).format("ddd, MMM D • HH:mm");
  const end = moment(endTime).format("HH:mm");
  const [isLoad, setIsLoad] = useState(false);

  async function handlePressButtonAsync(link) {
    let result = await WebBrowser.openBrowserAsync(link);
  }

  function onActive() {
    let result = false;
    if (typeUser === "STUDENT" && data.S_STATUS !== "COMPLETE") {
      result = checkTime({ type: "re", time: startTime });
    }
    return result;
  }

  const update = async (body) => {
    const res = await myCourseAction.updateSession(
      user,
      data.ORDER_ID,
      data.SESSION_ID,
      body
    );
    setIsLoad(false);
    props.extraData.onClose();
    // console.log(res);
  };

  function renderLink() {
    if (link.length > 0) {
      return link.map((row) => {
        return (
          <TouchableOpacity
            style={{ width: "87%" }}
            onPress={async () => {
              handlePressButtonAsync(row);
            }}
          >
            <InterBoldText
              text={row}
              style={{
                color: "#ED1B24",
                fontSize: 14,
              }}
            />
          </TouchableOpacity>
        );
      });
    } else {
      return (
        <InterBoldText
          text="-"
          style={{
            color: "#4d4d4d",
            width: "87%",
            marginTop: 4,
            fontSize: 14,
          }}
        />
      );
    }
  }
  // console.log(status);
  // console.log(data.S_STATUS);
  // console.log("---------");

  const renderBottomTeach = () => {
    return (
      <View
        style={{
          position: "absolute",
          bottom: 24,
          width: typeUser === "STUDENT" ? "100%" : "87%",
          justifyContent: "center",
        }}
      >
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            justifyContent: typeUser === "STUDENT" ? "center" : null,
          }}
        >
          {data.S_STATUS === "WAITING_APPROVE" ? (
            <Button
              style={{
                backgroundColor: isLoad ? "#cccccc" : "#ED1B24",
              }}
              isLoad={isLoad}
              text={Language.EN.courseDetail.cancel}
              activeOpacity={isLoad ? 1 : 0.2}
              onPress={() => {
                if (!isLoad) {
                  setIsLoad(true);
                  props.extraData.onCancelSession();
                }
              }}
            />
          ) : status === "req" ? (
            <View
              style={{
                width: "87%",
                flexDirection: "row",
              }}
            >
              <Button
                style={{ flex: 1, backgroundColor: "#f8f7f7" }}
                styleText={{ color: "#ED1B24", fontSize: 14 }}
                text={Language.EN.reschedule.cancel}
                onPress={() => {
                  props.extraData.onCancelReq();
                }}
              />
              <Button
                reschedule={true}
                style={{
                  flex: 2.5,
                  backgroundColor: "#ffffff",
                  borderWidth: 1,
                  borderColor: onActive() ? "#999999" : "#cccccc",
                }}
                text={Language.EN.courseDetail.reschedule}
                styleTextReschedule={{
                  color: onActive() ? "#4d4d4d" : "#cccccc",
                }}
                active={onActive()}
                activeOpacity={onActive() ? 0.2 : 1}
                onPress={() => {
                  if (onActive()) {
                    props.extraData.modalBox();
                  }
                }}
              />
            </View>
          ) : (
            <Button
              reschedule={true}
              style={{
                backgroundColor: "#ffffff",
                borderWidth: 1,
                borderColor: onActive() ? "#999999" : "#cccccc",
              }}
              text={Language.EN.courseDetail.reschedule}
              styleTextReschedule={{
                color: onActive() ? "#4d4d4d" : "#cccccc",
              }}
              active={onActive()}
              activeOpacity={onActive() ? 0.2 : 1}
              onPress={() => {
                if (onActive()) {
                  props.extraData.modalBox();
                }
              }}
            />
          )}

          {typeUser === "STUDENT" ? null : (
            <TouchableOpacity
              style={{
                marginTop: 16,
                justifyContent: "center",
                marginLeft: 19,
                width: 20,
                alignItems: "center",
              }}
              activeOpacity={data.S_STATUS !== "WAITING_APPROVE" ? 0.2 : 1}
              onPress={() => {
                if (data.S_STATUS !== "WAITING_APPROVE") {
                  props.extraData.modalEdit();
                }
              }}
            >
              <View
                style={{
                  width: 4,
                  height: 4,
                  backgroundColor:
                    data.S_STATUS !== "WAITING_APPROVE" ? "#4d4d4d" : "#cccccc",
                  borderRadius: 50,
                  marginBottom: 5,
                }}
              />
              <View
                style={{
                  width: 4,
                  height: 4,
                  backgroundColor:
                    data.S_STATUS !== "WAITING_APPROVE" ? "#4d4d4d" : "#cccccc",
                  borderRadius: 50,
                  marginBottom: 5,
                }}
              />
              <View
                style={{
                  width: 4,
                  height: 4,
                  backgroundColor:
                    data.S_STATUS !== "WAITING_APPROVE" ? "#4d4d4d" : "#cccccc",
                  borderRadius: 50,
                }}
              />
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  };

  const renderBotomStu = () => {
    return (
      <View
        style={{
          position: "absolute",
          width: "100%",
          bottom: 0,
          borderTopLeftRadius: 10,
          borderTopRightRadius: 10,
          alignItems: "center",
          backgroundColor: "#ffffff",
        }}
      >
        {status && status !== "cancel" && status !== "approve"
          ? haveStatus()
          : notHaveStatus()}
        <View style={{ marginBottom: 24 }} />
      </View>
    );
  };

  const haveStatus = () => {
    return (
      <View style={{ width: "100%", alignItems: "center" }}>
        {status === "reject" ? (
          <InterBoldText
            text={start + " - " + end}
            style={{ color: "#4D4D4D", width: "87%", marginTop: 16 }}
          />
        ) : (
          <InterRegularText
            text={start + " - " + end}
            style={{ color: "#999999", width: "87%", marginTop: 16 }}
          />
        )}
        <View
          style={{
            width: "87%",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <View style={{ flex: 1 }}>
            {status === "reject" ? (
              <InterRegularText
                text={"↳ " + startNewTime + " - " + endNewTime}
                style={{ color: "#999999", textDecorationLine: "line-through" }}
              />
            ) : (
              <InterBoldText
                text={"↳ " + startNewTime + " - " + endNewTime}
                style={{ color: "#4d4d4d" }}
              />
            )}
          </View>
          <View>
            {status === "req" ? (
              <PendingStatus />
            ) : status === "reject" ? (
              <DeniedStatus />
            ) : (
              <ApprovedStatus />
            )}
          </View>
        </View>
        {status === "req" ? (
          <Button
            style={{
              backgroundColor: "#ffffff",
              borderWidth: 1,
              borderColor: "#e6e6e6",
            }}
            text={Language.EN.courseDetail.cancelReq}
            styleText={{ color: "#ED1B24", fontSize: 14 }}
            onPress={() => {
              props.extraData.modalCancelReq();
            }}
          />
        ) : (
          <Button
            style={{
              backgroundColor: "#ffffff",
              borderWidth: 1,
              borderColor: "#e6e6e6",
            }}
            text={Language.EN.courseDetail.reschedule}
            activeOpacity={onActive() ? 0.2 : 1}
            styleText={{
              color: onActive() ? "#4d4d4d" : "#e6e6e6",
              fontSize: 14,
            }}
            onPress={() => {
              if (onActive()) {
                props.extraData.modalBox();
              }
            }}
          />
        )}
      </View>
    );
  };

  const notHaveStatus = () => {
    return (
      <View style={{ width: "100%", alignItems: "center" }}>
        <View
          style={{
            width: "87%",
            flexDirection: "row",
            alignItems: "center",
            marginTop: 12,
          }}
        >
          <View style={{ flex: 1 }}>
            <InterBoldText text={start + " - " + end} />
          </View>
          <View>
            {data.S_STATUS === "WAITING_APPROVE" ? (
              <PendingStatus />
            ) : data.S_STATUS === "reject" ? (
              <DeniedStatus />
            ) : data.S_STATUS === "COMPLETE" ? (
              <ComplatetStatus />
            ) : (
              <ApprovedStatus />
            )}
          </View>
        </View>
        {data.S_STATUS === "WAITING_APPROVE" ? (
          <Button
            style={{
              backgroundColor: "#ffffff",
              borderWidth: 1,
              borderColor: "#e6e6e6",
            }}
            text={Language.EN.courseDetail.cancelBook}
            styleText={{ color: "#ED1B24", fontSize: 14 }}
            onPress={() => {
              props.extraData.modalCancelBook();
            }}
          />
        ) : (
          <Button
            style={{
              backgroundColor: "#ffffff",
              borderWidth: 1,
              borderColor: "#e6e6e6",
            }}
            text={Language.EN.courseDetail.reschedule}
            activeOpacity={onActive() ? 0.2 : 1}
            styleText={{
              color: onActive() ? "#4d4d4d" : "#e6e6e6",
              fontSize: 14,
            }}
            onPress={() => {
              if (onActive()) {
                props.extraData.modalBox();
              }
            }}
          />
        )}
      </View>
    );
  };

  return (
    <ScrollView
      style={{ flex: 1, backgroundColor: "#F8F7F7" }}
      contentContainerStyle={{ flexGrow: 1, alignItems: "center" }}
    >
      <View style={{ marginTop: 27, width: "87%", flexDirection: "row" }}>
        <InterBoldText
          text={data.courseName ? `${data.courseName} ` : "-"}
          style={{ color: "#333333", fontSize: 24, flex: 1 }}
        />
        <CloseGrey
          onPress={() => {
            props.extraData.onClose();
          }}
        />
      </View>
      {data.detail ? (
        <InterRegularText
          text={data.detail}
          style={{
            width: "87%",
            fontSize: 13,
            color: "#999999",
          }}
        />
      ) : null}

      <InterBoldText
        text={Language.EN.courseDetail.teacher}
        style={{
          color: "#999999",
          width: "87%",
          marginTop: 24,
          fontSize: 13,
        }}
      />
      <InterBoldText
        text={data.teacher ? data.teacher : "-"}
        style={{
          color: "#4d4d4d",
          width: "87%",
          marginTop: 4,
          fontSize: 14,
        }}
      />
      <InterBoldText
        text={Language.EN.courseDetail.student}
        style={{
          color: "#999999",
          width: "87%",
          marginTop: 20,
          fontSize: 13,
        }}
      />
      <InterBoldText
        text={data.student ? data.student : "-"}
        style={{
          color: "#4d4d4d",
          width: "87%",
          marginTop: 4,
          fontSize: 14,
        }}
      />
      <InterBoldText
        text={Language.EN.courseDetail.note}
        style={{
          color: "#999999",
          width: "87%",
          marginTop: 20,
          fontSize: 13,
        }}
      />
      <InterBoldText
        text={data.NOTE}
        style={{
          color: "#4d4d4d",
          width: "87%",
          marginTop: 4,
          fontSize: 14,
        }}
      />
      <InterBoldText
        text={Language.EN.courseDetail.link}
        style={{
          color: "#999999",
          width: "87%",
          marginTop: 24,
          marginBottom: 4,
          fontSize: 13,
        }}
      />
      {renderLink()}
      {typeUser === "STUDENT" ? renderBotomStu() : renderBottomTeach()}

      <View style={{ marginBottom: 120 }} />
    </ScrollView>
  );
};

const mapStateToProps = (state) => {
  return {
    session: state.session.data,
    user: state.user.data,
  };
};

export default connect(mapStateToProps)(Default);
