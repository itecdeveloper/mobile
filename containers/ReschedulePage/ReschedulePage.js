import React, { useState, useEffect } from "react";
import { View, Dimensions, TouchableOpacity } from "react-native";
import { Language } from "../../constants/language";
import { InterBoldText, InterRegularText } from "../../components/StyledText";
import CloseGrey from "../../components/CloseGrey";
import Button from "../../components/Button";
import ModalBox from "./ModalBox";
import ModalEdit from "./ModalEdit";
import Constants from "expo-constants";
import { ScrollView } from "react-native-gesture-handler";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Default from "./Default";
import Edit from "./Edit";
import Container from "./Container";
import * as myCourseAction from "../../actions/myCourseAction";
import * as sessionAction from "../../actions/sessionAction";
import AwesomeAlert from "react-native-awesome-alerts";
import ModalCancelBook from "./ModalCancelBook";
import ModalCancelReq from "./ModalCancelReq";

const Reschedule = (props) => {
  const [visible, setVisible] = useState(false);
  const [visibleEdit, setVisibleEdit] = useState(false);
  const [typeUser, setTypeUser] = useState(props.user.detail.TYPE);
  const [data, setData] = useState(props.route.data);
  const [error, setError] = useState(false);
  const dataSession = props.session;
  const [visibleCancelBook, setVisibleCancelBook] = useState(false);
  const [visibleCancelReq, setVisibleCancelReq] = useState(false);

  // props.sessionAction.setSession(props.route.data);
  useEffect(() => {
    const body = {
      S_STATUS: "ACTIVE",
    };
    //update(body);
  }, []);

  const update = async (body) => {
    const res = await myCourseAction.updateSession(
      props.user,
      props.route.data.ORDER_ID,
      props.route.data.SESSION_ID,
      body
    );
    if (res) {
      const body = {
        ...res,
        teacher: dataSession.teacher,
        student: dataSession.student,
        levelName: dataSession.levelName,
        courseName: dataSession.courseName,
        detail: dataSession.detail,
      };
      props.sessionAction.setSession(body);
    }
    // console.log(res);
  };

  const delSession = async (body) => {
    const res = await myCourseAction.delSession(
      props.user,
      props.route.data.ORDER_ID,
      body
    );
    props.navigation.goBack();
  };

  return (
    <View style={{ flex: 1 }}>
      <View style={{ height: Constants.statusBarHeight }} />
      <ModalBox
        isOpen={visible}
        onClosed={() => {
          setVisible(false);
        }}
        data={props}
        onReSchedule={(item) => {
          // setData(item);
        }}
        onError={() => {
          setVisible(false);
          setError(true);
        }}
      />
      <ModalEdit
        isComplete={props.route.data.S_STATUS === "COMPLETE"}
        isOpen={visibleEdit}
        onClosed={() => {
          setVisibleEdit(false);
        }}
        item={props.route.data}
        onEdit={() => {
          props.navigation.navigate("Edit");
          setVisibleEdit(false);
        }}
        onMark={() => {
          const body = {
            S_STATUS: "COMPLETE",
          };
          update(body);
          setVisibleEdit(false);
          props.navigation.goBack();
        }}
        onMarkIn={() => {
          const body = {
            S_STATUS: "ACTIVE",
          };
          update(body);
          setVisibleEdit(false);
          props.navigation.goBack();
        }}
      />
      <ModalCancelBook
        isOpen={visibleCancelBook}
        data={data}
        onClosed={() => {
          setVisibleCancelBook(false);
        }}
        onCancelSession={() => {
          const body = {
            SESSIONS: [
              {
                ORDER_ID: props.route.data.ORDER_ID,
                SSID: props.route.data.SESSION_ID,
              },
            ],
          };
          delSession(body);
        }}
      />
      <ModalCancelReq
        isOpen={visibleCancelReq}
        data={data}
        onClosed={() => {
          setVisibleCancelReq(false);
        }}
        onCancelReq={() => {
          const body = {
            STATUS_NEW_START_TIME: "cancel",
            NEW_START_TIME: dataSession.NEW_START_TIME,
          };
          update(body);
        }}
      />
      <Container
        user={props.user}
        data={data}
        onClose={() => {
          props.navigation.goBack();
        }}
        modalEdit={() => {
          setVisibleEdit(true);
        }}
        modalBox={() => {
          setVisible(true);
        }}
        typeUser={typeUser}
        onCancelEdit={() => {
          setEdit(false);
        }}
        modalCancelBook={() => {
          setVisibleCancelBook(true);
        }}
        modalCancelReq={() => {
          setVisibleCancelReq(true);
        }}
        onCancelReq={() => {
          const body = {
            STATUS_NEW_START_TIME: "cancel",
            NEW_START_TIME: dataSession.NEW_START_TIME,
          };
          update(body);
        }}
        onCancelSession={() => {
          const body = {
            SESSIONS: [
              {
                ORDER_ID: props.route.data.ORDER_ID,
                SSID: props.route.data.SESSION_ID,
              },
            ],
          };
          delSession(body);
        }}
      />
      <AwesomeAlert
        show={error}
        showProgress={false}
        title={Language.EN.reschedule.title}
        message={Language.EN.reschedule.message}
        closeOnTouchOutside={true}
        closeOnHardwareBackPress={false}
        showCancelButton={false}
        showConfirmButton={true}
        confirmText={Language.EN.login.confirm}
        confirmButtonColor="#ED1B24"
        titleStyle={{
          fontSize: 18,
          color: "#333333",
          fontFamily: "Inter-Bold",
        }}
        confirmButtonTextStyle={{ fontFamily: "Inter-Bold" }}
        messageStyle={{
          textAlign: "center",
          fontSize: 14,
          color: "#333333",
          fontFamily: "Inter-Regular",
        }}
        onConfirmPressed={() => {
          setError(false);
        }}
        onDismiss={() => {
          setError(false);
        }}
      />
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.user.data,
    session: state.session.data,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    sessionAction: bindActionCreators(sessionAction, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Reschedule);
