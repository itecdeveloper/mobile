import * as React from "react";
import {
  createStackNavigator,
  CardStyleInterpolators,
} from "@react-navigation/stack";
import Default from "./Default";
import Edit from "./Edit";
const Stack = createStackNavigator();
export default function Container(extraData) {
  return (
    <Stack.Navigator
      initialRouteName={"Default"}
      screenOptions={{
        headerShown: false,
        animationEnabled: true,
      }}
    >
      <Stack.Screen
        name="Default"
        options={{
          title: "Default",
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      >
        {(props) => <Default {...props} extraData={extraData} />}
      </Stack.Screen>
      <Stack.Screen
        name="Edit"
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      >
        {(props) => <Edit {...props} extraData={extraData} />}
      </Stack.Screen>
    </Stack.Navigator>
  );
}
