import React, { useState, useEffect } from "react";
import { View, TouchableOpacity, Platform, Dimensions } from "react-native";
import Modal from "react-native-modalbox";
import CheckGrey from "../../components/CheckGrey";
import EditGrey from "../../components/EditGrey";
import {
  InterBoldText,
  InterRegularText,
  InterRegularTextInput,
} from "../../components/StyledText";
import { ScrollView } from "react-native-gesture-handler";
import { Language } from "../../constants/language";
import Button from "../../components/Button";
import moment from "moment";

export default function ModalEdit(props) {
  let modalHeight = 144;
  let startTime = props.item.START_TIME;
  if (props.item.STATUS_NEW_START_TIME === "approve") {
    startTime = props.item.NEW_START_TIME;
  }

  let completeAt = moment(props.item.COMPLETED_AT).format("DD-MM-YYYY");
  let dateNow = moment().format("DD-MM-YYYY");
  let endTime = startTime + props.item.SESSION_TIME * 1000;
  let disMark = false;
  let disMarkIn = false;
  if (moment().format("x") < endTime) {
    disMark = true;
  }

  if (completeAt !== dateNow) {
    disMarkIn = true;
  }

  return (
    <Modal
      onClosed={() => {
        props.onClosed();
      }}
      isOpen={props.isOpen}
      position="bottom"
      style={{
        backgroundColor: "white",
        height: modalHeight,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
      }}
    >
      <View
        style={{
          flex: 1,
          backgroundColor: "#f8f7f7",
          alignItems: "center",
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
        }}
      >
        {props.isComplete ? (
          <TouchableOpacity
            style={{
              height: 72,
              width: "87%",
              flexDirection: "row",
              alignItems: "center",
            }}
            activeOpacity={disMarkIn ? 1 : 0.2}
            onPress={() => {
              if (!disMarkIn) {
                props.onMarkIn();
              }
            }}
          >
            <CheckGrey />
            <InterBoldText
              text={Language.EN.courseDetail.markin}
              style={{
                marginLeft: 16,
                fontSize: 16,
                color: disMarkIn ? "#999999" : "#4d4d4d",
              }}
            />
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            style={{
              height: 72,
              width: "87%",
              flexDirection: "row",
              alignItems: "center",
            }}
            activeOpacity={disMark ? 1 : 0.2}
            onPress={() => {
              if (!disMark) {
                props.onMark();
              }
            }}
          >
            <CheckGrey />
            <InterBoldText
              text={Language.EN.courseDetail.mark}
              style={{
                marginLeft: 16,
                fontSize: 16,
                color: disMark ? "#999999" : "#4d4d4d",
              }}
            />
          </TouchableOpacity>
        )}

        <View style={{ width: "93%", height: 1, backgroundColor: "#e6e6e6" }} />
        <TouchableOpacity
          style={{
            height: 72,
            width: "87%",
            flexDirection: "row",
            alignItems: "center",
          }}
          onPress={() => {
            props.onEdit();
          }}
        >
          <EditGrey />
          <InterBoldText
            text={Language.EN.courseDetail.edit}
            style={{ marginLeft: 16, fontSize: 16, color: "#4d4d4d" }}
          />
        </TouchableOpacity>
      </View>
    </Modal>
  );
}
