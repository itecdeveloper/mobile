import React, { useState, useEffect } from "react";
import { View, Dimensions, TouchableOpacity } from "react-native";
import { Language } from "../../constants/language";
import { InterBoldText, InterRegularText } from "../../components/StyledText";
import CloseGrey from "../../components/CloseGrey";
import Button from "../../components/Button";
import { ScrollView } from "react-native-gesture-handler";
import Textarea from "react-native-textarea";
import UseKeyboard from "../../components/UseKeyboard";
import moment from "moment";
import * as myCourseAction from "../../actions/myCourseAction";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as sessionAction from "../../actions/sessionAction";
import AddLink from "../../components/AddLink";

const Edit = (props) => {
  const [keyboardHeight, setKeyboardHeight] = useState(0);

  const { user } = props.extraData;
  const data = props.session;
  let startTime = data.START_TIME;
  if (data.STATUS_NEW_START_TIME === "approve") {
    startTime = data.NEW_START_TIME;
  }
  let endTime = startTime + data.SESSION_TIME * 1000;
  const start = moment(startTime).format("ddd, MMMM D • HH:mm");
  const end = moment(endTime).format("HH:mm");
  const noteHeight = Dimensions.get("window").height * 0.264;
  const linkHeight = Dimensions.get("window").height * 0.1;
  const [note, setNote] = useState(data.NOTE);
  const [valueLink, setValueLink] = useState("-");
  const [isLoad, setIsLoad] = useState(false);
  const [textLink, setTextLink] = useState([]);
  function renderLink() {
    let result = "";
    if (data.LINKS.length > 0) {
      data.LINKS.map((row) => {
        result = result + " " + row;
      });
    } else {
      result = "-";
    }
    return result;
  }
  useEffect(() => {
    let link = [];
    data.LINKS.map((item) => {
      link.push({
        link: item,
      });
    });
    setTextLink(link);
  }, []);
  const keyboard = UseKeyboard();
  useEffect(() => {
    setKeyboardHeight(keyboard);
  }, [keyboard]);

  function save() {
    setIsLoad(true);
    let link = [];
    textLink.map((item) => {
      if (item.link) {
        link.push(item.link);
      }
    });
    let body = {
      NOTE: note,
      LINKS: link,
    };

    update(body);
  }

  const update = async (body) => {
    const res = await myCourseAction.updateSession(
      user,
      data.ORDER_ID,
      data.SESSION_ID,
      body
    );
    setIsLoad(false);
    if (res) {
      const body = {
        ...res,
        teacher: data.teacher,
        student: data.student,
        levelName: data.levelName,
        courseName: data.courseName,
      };
      props.sessionAction.setSession(body);
      props.navigation.goBack();
    } else {
    }
  };

  function renderTextLink() {
    return textLink.map((item, i) => {
      return (
        <Textarea
          containerStyle={{
            height: linkHeight,
            width: "87%",
            backgroundColor: "#ffffff",
            borderRadius: 4,
            marginTop: 4,
            textAlignVertical: "top",
            marginBottom: 4,
          }}
          style={{
            fontSize: 16,
            marginLeft: 16,
            marginRight: 16,
            marginTop: 8,
            color: "#4d4d4d",
            fontFamily: "Inter-Bold",
          }}
          onChangeText={(value) => {
            textLink[i].link = value;
            setTextLink(textLink);
          }}
          defaultValue={item.link}
          underlineColorAndroid={"transparent"}
        />
      );
    });
  }

  return (
    <ScrollView
      style={{ flex: 1, backgroundColor: "#F8F7F7" }}
      contentContainerStyle={{ flexGrow: 1, alignItems: "center" }}
    >
      <View style={{ marginTop: 27, width: "87%", flexDirection: "row" }}>
        <InterBoldText
          text={`${data.courseName} Lv.${data.levelName}`}
          style={{ color: "#808080", flex: 1 }}
        />
      </View>
      <InterBoldText
        text={start + " - " + end}
        style={{
          width: "87%",
          fontSize: 24,
          color: "#333333",
          marginTop: 8,
        }}
      />

      <InterBoldText
        text={Language.EN.courseDetail.note}
        style={{
          color: "#999999",
          width: "87%",
          marginTop: 24,
        }}
      />
      <Textarea
        containerStyle={{
          height: noteHeight,
          width: "87%",
          backgroundColor: "#ffffff",
          borderRadius: 4,
          marginTop: 4,
          textAlignVertical: "top",
        }}
        style={{
          fontSize: 16,
          marginLeft: 16,
          marginRight: 16,
          marginTop: 8,
          color: "#4d4d4d",
          fontFamily: "Inter-Bold",
        }}
        onChangeText={(value) => {
          setNote(value);
        }}
        defaultValue={note}
        underlineColorAndroid={"transparent"}
      />

      <InterBoldText
        text={Language.EN.courseDetail.link}
        style={{
          color: "#999999",
          width: "87%",
          marginTop: 24,
        }}
      />
      <View
        style={{
          marginBottom: 116 + keyboardHeight,
          width: "100%",
          alignItems: "center",
        }}
      >
        {renderTextLink()}
        <TouchableOpacity
          style={{ marginTop: 16 }}
          onPress={async () => {
            let link = [];
            textLink.map((row) => {
              link.push(row);
            });
            link.push({ link: "" });
            setTextLink(link);
          }}
        >
          <AddLink />
        </TouchableOpacity>
      </View>

      {keyboardHeight == 0 ? (
        <View
          style={{
            position: "absolute",
            bottom: 24,
            width: "87%",

            flexDirection: "row",
          }}
        >
          <Button
            style={{ flex: 1, backgroundColor: "#f8f7f7" }}
            styleText={{ color: "#ED1B24", fontSize: 14 }}
            text={Language.EN.courseDetail.cancel}
            onPress={() => {
              props.navigation.goBack();
            }}
          />
          <Button
            style={{
              flex: 2.5,
              backgroundColor: isLoad ? "#cccccc" : "#ED1B24",
            }}
            activeOpacity={isLoad ? 1 : 0.2}
            styleActivityIndicator={{ marginRight: 150 }}
            styleText={{ fontSize: 14 }}
            text={Language.EN.courseDetail.save}
            onPress={isLoad ? null : save}
            isLoad={isLoad}
          />
        </View>
      ) : null}
    </ScrollView>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.user.data,
    session: state.session.data,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    sessionAction: bindActionCreators(sessionAction, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Edit);
