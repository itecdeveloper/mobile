import React, { useState, useEffect } from "react";
import { View, TouchableOpacity, Platform, Dimensions } from "react-native";
import Modal from "react-native-modalbox";
import {
  InterBoldText,
  InterRegularText,
  InterRegularTextInput,
} from "../../components/StyledText";
import { ScrollView } from "react-native-gesture-handler";
import { Language } from "../../constants/language";
import Button from "../../components/Button";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { Appearance } from "react-native-appearance";
import moment from "moment";
import * as myCourseAction from "../../actions/myCourseAction";
import SegmentedPicker from "react-native-segmented-picker";
import numeral from "numeral";
import { checkTime } from "../../components/Func";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as sessionAction from "../../actions/sessionAction";

const modalHeight = 370;

function ModalBox(props) {
  const newDateRef = React.useRef();
  const newTimeRef = React.useRef();
  const segmentedPickerRef = React.useRef();

  const data = props.session;

  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [isTimePickerVisible, setTimePickerVisibility] = useState(false);
  const [newDate, setNewDate] = useState("");
  const [newTime, setNewTime] = useState("");
  const [isLoad, setIsLoad] = useState(false);
  const [hhAndroid, sethhAndroid] = useState("");
  const [mmAndroid, setmmAndroid] = useState("");
  // const [h, setH] = useState([]);
  let isDarkModeEnabled = false;
  let colorScheme = Appearance.getColorScheme();
  if (colorScheme === "dark") {
    isDarkModeEnabled = true;
  }

  const HH = () => {
    let h = [];
    for (let i = 0; i < 24; i++) {
      h.push({ label: numeral(i).format("00") });
    }
    return h;
  };

  const mm = () => {
    let m = [];
    for (let i = 0; i < 12; i++) {
      m.push({ label: numeral(5 * i).format("00") });
    }
    return m;
  };

  const handleDate = (date) => {
    const dateTime = moment(date).format("dddd D MMMM YYYY");
    newDateRef.current.setValue(dateTime);
    setDatePickerVisibility(false);
    setNewDate(date);
  };
  const handleTime = (time) => {
    const dateTime = moment(time).format("HH:mm");
    newTimeRef.current.setValue(dateTime);
    setTimePickerVisibility(false);
    setNewTime(time);
  };

  const handleTimeAndroid = (value) => {
    let dateTime = `${value.column1.label}:${value.column2.label}`;
    sethhAndroid(value.column1.label);
    setmmAndroid(value.column2.label);
    newTimeRef.current.setValue(dateTime);
    const dateNow = moment().format("YYYY-MM-DD ");
    dateTime = dateNow + dateTime;
    setNewTime(dateTime);
  };

  const checkSubmit = () => {
    let res = false;

    if (newDate && newTime) {
      const date = moment(newDate).format("YYYY-MM-DD ");
      const time = moment(newTime).format("HH:mm");
      let dateTime = date + time;

      dateTime = moment(dateTime).format("x");
      res = dateTime;
    }
    return res;
  };
  const onSubmit = async () => {
    const dateTime = checkSubmit();
    if (dateTime) {
      setIsLoad(true);
      const time = moment(parseInt(dateTime)).format("x");
      const check = checkTime({
        time: time,
        type: "bookClass",
        limitTime: props.data.route.data.item.time,
        exp: moment(props.data.route.data.exp).format("x"),
      });
      if (check) {
        const body = {
          NEW_START_TIME: dateTime,
          STATUS_NEW_START_TIME: "req",
        };
        const res = await myCourseAction.updateSession(
          props.data.user,
          props.data.route.data.ORDER_ID,
          props.data.route.data.SESSION_ID,
          body
        );
        setIsLoad(false);

        if (res) {
          const body = {
            ...res,
            teacher: data.teacher,
            student: data.student,
            levelName: data.levelName,
            courseName: data.courseName,
            detail: data.detail,
          };
          props.sessionAction.setSession(body);
          setNewDate("");
          setNewTime("");
          props.onClosed();
          props.onReSchedule(res);
        }
      } else {
        setIsLoad(false);
        props.onError();
      }
    }
  };
  return (
    <Modal
      onClosed={() => {
        setNewDate("");
        setNewTime("");
        props.onClosed();
      }}
      isOpen={props.isOpen}
      position="bottom"
      style={{
        backgroundColor: "white",
        height: modalHeight,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
      }}
    >
      <DateTimePickerModal
        isVisible={isDatePickerVisible}
        mode="date"
        onConfirm={handleDate}
        onCancel={() => {
          setDatePickerVisibility(false);
        }}
        onHide={() => {
          setDatePickerVisibility(false);
        }}
        isDarkModeEnabled={isDarkModeEnabled}
        date={newDate ? newDate : new Date(props.session.START_TIME)}
        display={"spinner"}
        minimumDate={new Date()}
      />

      <DateTimePickerModal
        isVisible={isTimePickerVisible}
        mode="time"
        onConfirm={handleTime}
        onCancel={() => {
          setTimePickerVisibility(false);
        }}
        locale="en_GB"
        onHide={() => {
          setTimePickerVisibility(false);
        }}
        date={newTime ? newTime : new Date(props.session.START_TIME)}
        isDarkModeEnabled={isDarkModeEnabled}
        display={"spinner"}
        minuteInterval={5}
      />

      <SegmentedPicker
        defaultSelections={{
          column1: hhAndroid
            ? hhAndroid
            : moment(props.session.START_TIME).format("HH"),
          column2: mmAndroid
            ? mmAndroid
            : moment(props.session.START_TIME).format("mm"),
        }}
        ref={segmentedPickerRef}
        onConfirm={() => {}}
        options={{
          column1: HH(),
          column2: mm(),
        }}
        size={30}
        onConfirm={handleTimeAndroid}
      />

      <View
        style={{
          flex: 1,
          backgroundColor: "#f8f7f7",
          alignItems: "center",
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
        }}
      >
        <InterBoldText
          text={Language.EN.courseDetail.selectDate}
          style={{ width: "87%", marginTop: 32 }}
        />

        <InterRegularTextInput
          ref={newDateRef}
          label={Language.EN.courseDetail.newDate}
          onPress={() => {
            setDatePickerVisibility(true);
          }}
          editable={false}
          showIconDate={true}
        />
        <InterRegularTextInput
          ref={newTimeRef}
          label={Language.EN.courseDetail.newTime}
          editable={false}
          onPress={() => {
            if (Platform.OS === "android") {
              segmentedPickerRef.current.show();
            } else {
              setTimePickerVisibility(true);
            }
          }}
          showIconTime={true}
        />
        <Button
          isLoad={isLoad}
          text={Language.EN.courseDetail.submit}
          styleText={{ fontSize: 14 }}
          style={{
            marginTop: 24,
            backgroundColor: checkSubmit() && !isLoad ? "#ED1B24" : "#cccccc",
          }}
          styleActivityIndicator={{ marginRight: 160 }}
          activeOpacity={checkSubmit() && !isLoad ? 0.2 : 1}
          onPress={onSubmit}
        />
        <TouchableOpacity
          onPress={() => {
            props.onClosed();
          }}
        >
          <InterBoldText
            text={Language.EN.courseDetail.cancel}
            style={{ marginTop: 20, color: "#ED1B24" }}
          />
        </TouchableOpacity>
      </View>
    </Modal>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user.data,
    session: state.session.data,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    sessionAction: bindActionCreators(sessionAction, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalBox);
