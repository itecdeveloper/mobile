import React, { useState, useEffect } from "react";
import { View, TouchableOpacity, Platform, Dimensions } from "react-native";
import Modal from "react-native-modalbox";
import CheckGrey from "../../components/CheckGrey";
import EditGrey from "../../components/EditGrey";
import {
  InterBoldText,
  InterRegularText,
  InterRegularTextInput,
} from "../../components/StyledText";
import { ScrollView } from "react-native-gesture-handler";
import { Language } from "../../constants/language";
import Button from "../../components/Button";
import Cancel from "../../components/Cancel";
import * as myCourseAction from "../../actions/myCourseAction";
import numeral from "numeral";
import moment from "moment";
import Constants from "expo-constants";

export default function ModalCancelReq(props) {
  let startTime = props.data.START_TIME;
  let endTime = startTime + props.data.SESSION_TIME * 1000;
  const start = moment(startTime).format("ddd, MMM D • HH:mm");
  const end = moment(endTime).format("HH:mm");

  let startNewTime = moment(props.data.NEW_START_TIME).format(
    "ddd, MMM D • HH:mm"
  );
  let endNewTime = moment(
    props.data.NEW_START_TIME + props.data.SESSION_TIME * 1000
  ).format("HH:mm");

  let addHeight = (props.data.courseName.length / 40) * 20;

  if (props.data.detail) {
    addHeight = (props.data.detail.length / 40) * 20;
  }

  const modalHeight = 336 + addHeight;
  return (
    <Modal
      onClosed={() => {
        props.onClosed();
      }}
      isOpen={props.isOpen}
      position="bottom"
      style={{
        backgroundColor: "white",
        height: modalHeight,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
      }}
    >
      <View
        style={{
          flex: 1,
          backgroundColor: "#f8f7f7",
          alignItems: "center",
          borderTopLeftRadius: 10,
          borderTopRightRadius: 10,
        }}
      >
        <View
          style={{
            width: "87%",
            marginTop: 24,
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Cancel />
          <InterBoldText
            text={Language.EN.reschedule.cancelReq}
            style={{ fontSize: 14, color: "#ED1B24", marginLeft: 12 }}
          />
        </View>
        <InterBoldText
          text={Language.EN.reschedule.textCancelBook}
          style={{ width: "87%", marginTop: 16 }}
        />
        <InterBoldText
          text={Language.EN.reschedule.course}
          style={{
            width: "87%",
            marginTop: 12,
            fontSize: 12,
            color: "#999999",
          }}
        />
        <InterBoldText
          text={
            props.data.detail
              ? props.data.courseName + " /"
              : props.data.courseName
          }
          style={{ width: "87%" }}
        />
        {props.data.detail ? (
          <InterBoldText text={props.data.detail} style={{ width: "87%" }} />
        ) : null}

        <InterBoldText
          text={Language.EN.reschedule.original}
          style={{
            width: "87%",
            marginTop: 12,
            fontSize: 12,
            color: "#999999",
          }}
        />
        <InterBoldText text={start + " - " + end} style={{ width: "87%" }} />
        <InterBoldText
          text={Language.EN.reschedule.new}
          style={{
            width: "87%",
            marginTop: 12,
            fontSize: 12,
            color: "#999999",
          }}
        />
        <InterBoldText
          text={startNewTime + " - " + endNewTime}
          style={{ width: "87%" }}
        />
        <View
          style={{
            position: "absolute",
            bottom:
              Platform.OS === "ios" && Constants.statusBarHeight > 24 ? 24 : 0,
            width: "100%",
            height: 47,
            flexDirection: "row",
          }}
        >
          <TouchableOpacity
            style={{
              flex: 1,
              borderTopWidth: 1,
              borderColor: "#e6e6e6",
              justifyContent: "center",
              alignItems: "center",
            }}
            onPress={() => {
              props.onClosed();
            }}
          >
            <InterBoldText
              text={Language.EN.reschedule.back}
              style={{ color: "#ED1B24" }}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              flex: 1,
              backgroundColor: "#ED1B24",
              justifyContent: "center",
              alignItems: "center",
            }}
            onPress={() => {
              props.onCancelReq();
              props.onClosed();
            }}
          >
            <InterBoldText
              text={Language.EN.reschedule.cancelReq}
              style={{ color: "#ffffff" }}
            />
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
}
