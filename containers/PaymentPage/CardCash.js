import React, { useState, useEffect } from "react";
import { View, TouchableOpacity, StyleSheet } from "react-native";
import {
  InterRegularTextInput,
  InterRegularText,
  InterBoldText,
} from "../../components/StyledText";
import { Language } from "../../constants/language";
import PaymentOptionIcon from "../../components/PaymentOptionIcon";
import Check from "../../components/Check";

export default function CardCash(props) {
  let opacity = props.active ? 1 : 0.2;
  return (
    <TouchableOpacity
      style={{
        width: "87%",
        height: 95,
        borderWidth: props.active ? 2 : 1,
        marginTop: 12,
        borderRadius: 5,
        borderColor: props.active ? "#ED1B24" : "#cccccc",
      }}
      onPress={() => {
        props.onPress();
      }}
    >
      <View
        style={{
          marginLeft: 18,
          marginTop: 16,
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <InterBoldText
          text={Language.EN.payment.credit}
          style={{ fontSize: 16, flex: 1 }}
        />
        <View style={{ flex: 1, alignItems: "flex-end", marginRight: 16 }}>
          {props.active ? (
            <Check />
          ) : (
            <View
              style={{
                width: 24,
                height: 24,
                borderRadius: 50,
                borderColor: "#cccccc",
                borderWidth: 1,
              }}
            />
          )}
        </View>
      </View>
      <View
        style={{
          marginLeft: 18,
          flexDirection: "row",
          alignItems: "center",
          marginTop: 6,
        }}
      >
        <PaymentOptionIcon
          name="mastercard"
          style={{ width: 30, height: 19, opacity: opacity }}
        />
        <PaymentOptionIcon
          name="visa"
          style={{ width: 55, height: 20, marginLeft: 6, opacity: opacity }}
        />
        <PaymentOptionIcon
          name="american"
          style={{ width: 37, height: 20, marginLeft: 6, opacity: opacity }}
        />
        <PaymentOptionIcon
          name="jcb"
          style={{ width: 24, height: 24, marginLeft: 6, opacity: opacity }}
        />
        <PaymentOptionIcon
          name="unionpay"
          style={{ width: 30, height: 19, marginLeft: 6, opacity: opacity }}
        />
        <PaymentOptionIcon
          name="discovercard"
          style={{ width: 18, height: 18, marginLeft: 6, opacity: opacity }}
        />
        <PaymentOptionIcon
          name="dinersclub"
          style={{ width: 26, height: 20, marginLeft: 6, opacity: opacity }}
        />
      </View>
    </TouchableOpacity>
  );
}
