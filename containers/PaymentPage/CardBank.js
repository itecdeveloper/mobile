import React, { useState, useEffect } from "react";
import { View, TouchableOpacity, StyleSheet } from "react-native";
import {
  InterRegularTextInput,
  InterRegularText,
  InterBoldText,
} from "../../components/StyledText";
import { Language } from "../../constants/language";
import Check from "../../components/Check";
import PaymentOptionIcon from "../../components/PaymentOptionIcon";

export default function PaymentPage(props) {
  let opacity = props.active ? 1 : 0.2;
  return (
    <TouchableOpacity
      style={{
        width: "87%",
        height: 95,
        borderWidth: props.active ? 2 : 1,
        marginTop: 12,
        borderRadius: 5,
        borderColor: props.active ? "#ED1B24" : "#cccccc",
      }}
      onPress={() => {
        props.onPress();
      }}
    >
      <View
        style={{
          marginLeft: 18,
          marginTop: 16,
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <InterBoldText
          text={Language.EN.payment.bank}
          style={{ fontSize: 16, flex: 1 }}
        />
        <View style={{ flex: 1, alignItems: "flex-end", marginRight: 16 }}>
          {props.active ? (
            <Check />
          ) : (
            <View
              style={{
                width: 24,
                height: 24,
                borderRadius: 50,
                borderColor: "#cccccc",
                borderWidth: 1,
              }}
            />
          )}
        </View>
      </View>
      <PaymentOptionIcon
        style={{
          width: 73,
          height: 25,
          marginLeft: 18,
          marginTop: 6,
          opacity: opacity,
        }}
      />
    </TouchableOpacity>
  );
}
