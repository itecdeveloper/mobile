import React, { useState, useEffect } from "react";
import { View, ActivityIndicator, StyleSheet } from "react-native";
import {
  InterRegularTextInput,
  InterRegularText,
  InterBoldText,
} from "../../components/StyledText";
import { Language } from "../../constants/language";
import CustomCheckBox from "../../components/CustomCheckBox";
import Button from "../../components/Button";
import Constants from "expo-constants";
import BackRed from "../../components/BackRed";
import { ScrollView } from "react-native-gesture-handler";
import PaymentOptionIcon from "../../components/PaymentOptionIcon";
import CardBank from "./CardBank";
import CardCash from "./CardCash";
import * as paymentAction from "../../actions/paymentAction";
import { connect } from "react-redux";
import moment from "moment";
import numeral from "numeral";
import * as myCourseAction from "../../actions/myCourseAction";
import HeaderDetail from "./HeaderDetail";

function PaymentPage(props) {
  const [bank, setBank] = useState(true);
  const [data, setData] = useState("");
  const [load, setLoad] = useState(false);
  const [loadData, setLoadData] = useState(true);
  const [name, setName] = useState("");
  const [detail, setDetail] = useState("");

  useEffect(() => {
    async function getCourse() {
      const res = await myCourseAction.getCourse(
        props.user,
        props.route.courseID
      );
      if (res) {
        const arr = res.DETAIL.COURSE_NAME.split(" / ");
        let name = arr[0];
        let detail = false;
        if (arr[1]) {
          detail = arr[1];
        }

        let dataAge = [];
        let session = res.DETAIL.SESSION_QUANTITY;
        let mins = res.DETAIL.SESSION_TIME / 60;
        const age = res.DETAIL.AGE.split("-");
        let label = "";
        if (age[0] == 0 && age[age.length - 1] == 99) {
          label = Language.EN.viewCourse.allAge;
        } else if (age[age.length - 1] == 99) {
          label = `${age[0]} ${Language.EN.viewCourse.above}`;
        } else {
          label = `${res.DETAIL.AGE} ${Language.EN.viewCourse.years}`;
        }
        let data = {
          label: label,
          text: `${session} sessions • ${mins} ${Language.EN.viewCourse.min}`,
        };
        dataAge.push(data);
        let item = {
          ...res.DETAIL,
          name,
          detail,
          dataAge,
        };
        setData(item);
        setLoadData(false);
      }
    }

    getCourse();
  }, []);

  async function bookCourse() {
    if (!props.route.backToPay) {
      setLoad(true);
      const body = {
        COURSE_ID: data.COURSE_ID,
        DATE: moment().format("x"),
        PAYMENT_TYPE: bank ? "bank_tran" : "2c2p",
        SESSION_QUANTITY: data.SESSION_QUANTITY,
        SESSION_TIME: data.SESSION_TIME,
      };
      console.log(body);
      const book = await paymentAction.payment(body, props.user);
      setLoad(false);
      const bookData = {
        dataOrder: book.data,
        dataUser: props.user,
        data,
      };
      if (bank) {
        props.navigation.navigate("BankTransfer", { bookData });
      } else {
        props.navigation.navigate("Request2c2p", { bookData });
      }
    } else {
      const bookData = {
        dataOrder: props.route.backToPay,
        dataUser: props.user,
        data,
      };
      if (bank) {
        props.navigation.navigate("BankTransfer", { bookData });
      } else {
        props.navigation.navigate("Request2c2p", { bookData });
      }
    }
  }

  return (
    <View style={{ flex: 1 }}>
      <View style={{ height: Constants.statusBarHeight }} />

      <ScrollView
        contentContainerStyle={{ flexGrow: 1 }}
        showsVerticalScrollIndicator={false}
      >
        <View
          style={{
            flex: 1,
            backgroundColor: "#f4f4f4",
            alignItems: "center",
            marginBottom: 400,
          }}
        >
          <View
            style={{
              marginTop: 17,
              width: "87%",
            }}
          >
            <BackRed
              onPress={() => {
                props.navigation.goBack();
              }}
            />
          </View>
          {loadData ? (
            <ActivityIndicator
              size="large"
              color="#ED1B24"
              style={{ marginTop: 100 }}
            />
          ) : (
            <View style={{ alignItems: "center", width: "100%" }}>
              <InterBoldText
                text={data.name}
                style={{ marginTop: 28, fontSize: 24, width: "87%" }}
              />
              {data.detail ? (
                <InterRegularText
                  text={data.detail}
                  style={{
                    width: "87%",
                    color: "#808080",
                    fontSize: 16,
                  }}
                />
              ) : null}
              <HeaderDetail
                label={Language.EN.viewCourse.by}
                text={data.INSTITUTE}
                textColor={"#ED1B24"}
              />
              <HeaderDetail
                multi={true}
                label={Language.EN.viewCourse.age}
                text={data.dataAge}
              />
              <HeaderDetail
                multi={true}
                length={true}
                label={Language.EN.viewCourse.length}
                text={data.dataAge}
              />

              <HeaderDetail
                label={Language.EN.viewCourse.price}
                text={numeral(data.PRICE).format(0, 0) + " THB"}
              />
              <InterRegularText
                text={Language.EN.payment.note}
                style={{
                  fontSize: 13,
                  marginTop: 16,
                  color: "#808080",
                  marginBottom: 16,
                }}
              />
            </View>
          )}
        </View>
      </ScrollView>

      <View
        style={{
          width: "100%",
          height: 384,
          backgroundColor: "#ffffff",
          position: "absolute",
          bottom: 0,
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
          alignItems: "center",
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 5,
          },
          shadowOpacity: 0.34,
          shadowRadius: 6.27,
          elevation: 10,
        }}
      >
        <InterBoldText
          text={Language.EN.payment.choose}
          style={{ marginTop: 23, width: "87%" }}
        />
        <CardCash
          active={!bank}
          onPress={() => {
            setBank(false);
          }}
        />
        <CardBank
          active={bank}
          onPress={() => {
            setBank(true);
          }}
        />
        <Button
          activeOpacity={load || loadData ? 1 : 0.2}
          text={Language.EN.payment.book}
          style={{
            marginTop: 24,
            backgroundColor: load || loadData ? "#cccccc" : "#ED1B24",
          }}
          onPress={load || loadData ? null : bookCourse}
          isLoad={load}
          styleActivityIndicator={{ marginRight: 150 }}
        />
      </View>
    </View>
  );
}
const mapStateToProps = (state) => {
  return { user: state.user.data };
};

export default connect(mapStateToProps)(PaymentPage);
