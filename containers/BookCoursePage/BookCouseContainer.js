import * as React from "react";
import {
  createStackNavigator,
  CardStyleInterpolators,
} from "@react-navigation/stack";
import Step1 from "./BookPage/Step1";
import Step2 from "./BookPage/Step2";

const Stack = createStackNavigator();

export default function BookCouseContainer(extraData) {
  return (
    <Stack.Navigator
      initialRouteName={"Step1"}
      screenOptions={{
        headerShown: false,
        animationEnabled: true,
      }}
    >
      <Stack.Screen
        name="Step1"
        options={{
          title: "Step1",
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      >
        {(props) => <Step1 {...props} extraData={extraData} />}
      </Stack.Screen>
      <Stack.Screen
        name="Step2"
        component={Step2}
        options={{
          title: "Step2",
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      />
    </Stack.Navigator>
  );
}
