export function Level(course) {
  let result = [
    {
      ...course,
      label: course.LEVEL,
      value: course.LEVEL_ID,
    },
  ];
  // coures.map((level, i) => {
  //   result.push({
  //     ...level,
  //     label: `Level ${i + 1}: ${level.LEVEL}`,
  //     value: level.LEVEL_ID,
  //   });
  // });
  return result;
}

export function DOC() {
  let result = [];
  var weekday = new Array(7);
  weekday[0] = "Sunday";
  weekday[1] = "Monday";
  weekday[2] = "Tuesday";
  weekday[3] = "Wednesday";
  weekday[4] = "Thursday";
  weekday[5] = "Friday";
  weekday[6] = "Saturday";

  weekday.map((day) => {
    result.push({
      label: day,
      value: day,
    });
  });

  return result;
}
