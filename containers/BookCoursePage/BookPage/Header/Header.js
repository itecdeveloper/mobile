import React, { useState, useEffect } from "react";
import { View, TouchableOpacity } from "react-native";
import { InterBoldText } from "../../../../components/StyledText";
import { Language } from "../../../../constants/language";
import Back from "../../../../components/Back";
export default function Header(props) {
  return (
    <View
      style={{
        height: 55,
        width: "100%",
        backgroundColor: "#ffffff",
        flexDirection: "row",
      }}
    >
      <TouchableOpacity
        style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        onPress={props.onPress}
      >
        <Back />
      </TouchableOpacity>
      <View style={{ flex: 4, justifyContent: "center", alignItems: "center" }}>
        <InterBoldText text={Language.EN.bookCourse.header} />
      </View>
      <View style={{ flex: 1 }} />
    </View>
  );
}
