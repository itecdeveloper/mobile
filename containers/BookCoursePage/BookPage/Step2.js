import React, { useState, useEffect } from "react";
import { View } from "react-native";
import Header from "./Header";
import Button from "../../../components/Button";
import { useFocusEffect } from "@react-navigation/native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as bookCourseAction from "../../../actions/bookCourseAction";

function Step2(props) {
  useEffect(() => {
    if (props.page.data.onBack) {
      props.bookCourseAction.setBookCoursePage({
        page: "Step1",
        onBack: false,
      });
      props.navigation.goBack();
    }
  }, [props]);

  return (
    <View style={{ flex: 1, backgroundColor: "red" }}>
      <Button
        text="Step2"
        onPress={() => {
          //   props.navigation.navigate("Step2");
        }}
      />
    </View>
  );
}
const mapStateToProps = (state) => {
  return {
    page: state.bookcousepage,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    bookCourseAction: bindActionCreators(bookCourseAction, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Step2);
