import React, { useState, useEffect } from "react";
import { View } from "react-native";
import Header from "./Header";
import Button from "../../../components/Button";
import { useFocusEffect } from "@react-navigation/native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as bookCourseAction from "../../../actions/bookCourseAction";
import { InterRegularText } from "../../../components/StyledText";
import { Language } from "../../../constants/language";
import Picker from "../../../components/Picker";
import { Level, DOC } from "./Func";

function Step1(props) {
  const [itemLevel, setItemLevel] = useState([]);
  const [level, setLevel] = useState(null);
  const [itemDOC, setItemDOC] = useState(null);
  useFocusEffect(
    React.useCallback(() => {
      props.bookCourseAction.setBookCoursePage({
        page: "Step1",
        onBack: false,
      });
    }, [])
  );

  useEffect(() => {
    setItemLevel(Level(props.extraData.route.course.LEVEL));
    setItemDOC(DOC());
  }, []);

  const placeholderLevel = {
    label: Language.EN.bookCourse.level,
    value: null,
    color: "#7a7a7a",
  };

  const placeholderDOC = {
    label: Language.EN.bookCourse.doc,
    value: null,
    color: "#7a7a7a",
  };

  return (
    <View style={{ flex: 1, backgroundColor: "#f4f4f4" }}>
      <InterRegularText
        text={props.extraData.route.course.DETAIL.COURSE_NAME}
        style={{ marginTop: 26, fontSize: 24, marginLeft: 25 }}
      />
      <InterRegularText
        text={Language.EN.bookCourse.fillIn}
        style={{ fontSize: 14, marginLeft: 25 }}
      />
      <View style={{ width: "100%", alignItems: "center" }}>
        <Picker
          label={Language.EN.bookCourse.level}
          placeholder={placeholderLevel}
          items={itemLevel}
          onValueChange={(value) => {
            setLevel(value);
          }}
        />
        {level == null ? (
          <InterRegularText
            text={Language.EN.bookCourse.textLevel}
            style={{
              width: "87%",
              fontSize: 14,
              color: "#999999",
              marginTop: 18,
            }}
          />
        ) : null}
        {level != null ? (
          <Picker
            label={Language.EN.bookCourse.doc}
            placeholder={placeholderDOC}
            items={itemDOC}
            onValueChange={(value) => {}}
          />
        ) : null}
      </View>
    </View>
  );
}

const mapStateToProps = (state) => {
  return {
    page: state.bookcousepage,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    bookCourseAction: bindActionCreators(bookCourseAction, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Step1);
