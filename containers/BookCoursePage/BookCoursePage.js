import React, { useState, useEffect } from "react";
import { View, TouchableOpacity } from "react-native";
import { InterBoldText } from "../../components/StyledText";
import { Language } from "../../constants/language";
import Back from "../../components/Back";
import BookCouseContainer from "./BookCouseContainer";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as bookCourseAction from "../../actions/bookCourseAction";
import { ScrollView } from "react-native-gesture-handler";

function BookCoursePage(props) {
  return (
    <View style={{ flex: 1 }}>
      <View
        style={{
          height: 55,
          width: "100%",
          backgroundColor: "#f4f4f4",
          flexDirection: "row",
        }}
      >
        <TouchableOpacity
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          onPress={() => {
            if (props.page.data.page === "Step1") {
              props.navigation.goBack();
              props.bookCourseAction.setBookCoursePage(false);
            } else {
              props.bookCourseAction.setBookCoursePage({
                page: props.page.data.page,
                onBack: true,
              });
            }
          }}
        >
          <Back />
        </TouchableOpacity>
        <View
          style={{ flex: 4, justifyContent: "center", alignItems: "center" }}
        >
          <InterBoldText text={Language.EN.bookCourse.header} />
        </View>
        <View style={{ flex: 1 }} />
      </View>
      <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
        <BookCouseContainer route={props.route} />
      </ScrollView>
    </View>
  );
}

const mapStateToProps = (state) => {
  return {
    page: state.bookcousepage,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    bookCourseAction: bindActionCreators(bookCourseAction, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BookCoursePage);
