import React, { useState, useEffect } from "react";
import { View, TouchableOpacity, StyleSheet, Keyboard } from "react-native";
import {
  InterRegularTextInput,
  InterBoldText,
} from "../../components/StyledText";
import { Language } from "../../constants/language";
import Button from "../../components/Button";
import Constants from "expo-constants";
import BackRed from "../../components/BackRed";
import { ScrollView } from "react-native-gesture-handler";
import UseKeyboard from "../../components/UseKeyboard";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from "moment";
import { Appearance } from "react-native-appearance";
import * as Crypto from "expo-crypto";
import registerForPushNotificationsAsync from "../../components/Notification/registerForPushNotificationsAsync";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as userLoginAction from "../../actions/userLoginAction";
import * as userEditAction from "../../actions/userEditAction";
import Toast, { DURATION } from "react-native-easy-toast";

function EditPropfilePage(props) {
  const [fName, setFName] = useState("");
  const [lName, setLName] = useState("");
  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");
  const [line, setLine] = useState("");
  const [school, setSchool] = useState("");
  const [contactFName, setContactFName] = useState("");
  const [contactLName, setContactLName] = useState("");
  const [relationship, setRelationship] = useState("");
  const [contactEmail, setContactEmail] = useState("");
  const [contactPhone, setContactPhone] = useState("");
  const [contactLine, setContactLine] = useState("");
  const [fixError, setFixError] = useState(false);
  const [load, setLoad] = useState(false);

  const [dob, setDOB] = useState("");
  const [contact, setContact] = useState(false);
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [keyboardHeight, setKeyboardHeight] = useState(0);
  const scrollView = React.useRef();
  const fNameRef = React.useRef();
  const lNameRef = React.useRef();
  const nameRef = React.useRef();
  const dobRef = React.useRef();
  const phoneRef = React.useRef();
  const lineRef = React.useRef();
  const schoolRef = React.useRef();
  const contacFnameRef = React.useRef();
  const contacLnameRef = React.useRef();
  const relationshipRef = React.useRef();
  const contacPhoneRef = React.useRef();
  const emailRef = React.useRef();
  const contacLineRef = React.useRef();
  const toastRef = React.useRef();

  const keyboard = UseKeyboard();
  useEffect(() => {
    setKeyboardHeight(keyboard);
  }, [keyboard]);

  function moveScroll(value) {
    scrollView.current.scrollTo({ y: value });
  }

  useEffect(() => {
    getUser();
  }, []);

  const getUser = async () => {
    const res = await props.userEditAction.getUser(props.user);
    if (res) {
      const {
        FISRT_NAME,
        LAST_NAME,
        NICK_NAME,
        CONTACT_NO,
        DOB,
        LINE,
        SC_NAME,
        CP_FISRT_NAME,
        CP_LAST_NAME,
        RELATIONSHIP,
        CP_CONTACT_NO,
        CP_EMAIL,
        CP_LINE,
      } = res.data;
      const dob = moment(DOB).format("DD MMM YYYY");
      fNameRef.current.setValue(FISRT_NAME);
      lNameRef.current.setValue(LAST_NAME);
      nameRef.current.setValue(NICK_NAME);
      phoneRef.current.setValue(CONTACT_NO);
      dobRef.current.setValue(dob);
      lineRef.current.setValue(LINE);
      schoolRef.current.setValue(SC_NAME);
      setFName(FISRT_NAME);
      setLName(LAST_NAME);
      setName(NICK_NAME);
      setPhone(CONTACT_NO);
      setDOB(DOB);
      setLine(LINE);
      setSchool(SC_NAME);

      if (calculate_age(dob)) {
        contacFnameRef.current.setValue(CP_FISRT_NAME);
        contacLnameRef.current.setValue(CP_LAST_NAME);
        relationshipRef.current.setValue(RELATIONSHIP);
        contacPhoneRef.current.setValue(CP_CONTACT_NO);
        emailRef.current.setValue(CP_EMAIL);
        contacLineRef.current.setValue(CP_LINE);
        setContactFName(CP_FISRT_NAME);
        setContactLName(CP_LAST_NAME);
        setRelationship(RELATIONSHIP);
        setContactPhone(CP_CONTACT_NO);
        setContactEmail(CP_EMAIL);
        setContactLine(CP_LINE);
      }
    }
  };

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };
  let isDarkModeEnabled = false;
  let colorScheme = Appearance.getColorScheme();
  if (colorScheme === "dark") {
    isDarkModeEnabled = true;
  }

  const calculate_age = (dob) => {
    const birthDate = new Date(dob);
    const difference = Date.now() - birthDate.getTime();
    let age = new Date(difference);
    age = Math.abs(age.getUTCFullYear() - 1970);
    if (age >= 15) {
      setContact(false);
      return false;
    } else {
      setContact(true);
      return true;
    }
  };

  const handleConfirm = (date) => {
    const dob = moment(date).format("DD MMM YYYY");
    dobRef.current.setValue(dob);
    hideDatePicker();
    calculate_age(dob);
    setDOB(date);
    setFixError(false);
  };

  const buttonRegis = () => {
    let result = false;
    if (contact) {
      if (
        fName &&
        lName &&
        name &&
        dob &&
        phone &&
        contactFName &&
        contactLName &&
        relationship &&
        contactPhone
      ) {
        result = true;
      }
    } else {
      if (fName && lName && name && dob && phone) {
        result = true;
      }
    }
    return result;
  };

  async function onRegis() {
    setLoad(true);
    const DOB = moment(dob).format("x");
    const body = {
      FISRT_NAME: fName,
      LAST_NAME: lName,
      NICK_NAME: name,
      DOB: DOB,
      CONTACT_NO: phone,
      LINE: line,
      SC_NAME: school,
      CP_FISRT_NAME: contactFName,
      CP_LAST_NAME: contactLName,
      RELATIONSHIP: relationship,
      CP_CONTACT_NO: contactPhone,
      CP_EMAIL: contactEmail,
      CP_LINE: contactLine,
    };
    // console.log(body);

    const edit = await props.userEditAction.editPropfile(body, props.user);

    if (edit) {
      // console.log(edit);
      // const { EMAIL, PASSWORD } = edit;
      // const token = await registerForPushNotificationsAsync();
      // const body = {
      //   EMAIL: EMAIL,
      //   PASSWORD: PASSWORD,
      //   NOTI_TOKEN: token,
      // };
      // await props.userLoginAction.userLogin(body);
      setLoad(false);
      toastRef.current.show(Language.EN.bookClass.success);
    } else {
      toastRef.current.show(Language.EN.bookClass.fail);
      setLoad(false);
    }
  }

  return (
    <View style={{ flex: 1 }}>
      <View style={{ height: Constants.statusBarHeight }} />
      <Toast
        style={{
          backgroundColor: "#32323290",
          opacity: 80,
          borderRadius: 50,
        }}
        positionValue={Constants.statusBarHeight + 70}
        fadeInDuration={750}
        fadeOutDuration={1000}
        position="bottom"
        ref={toastRef}
      />
      <DateTimePickerModal
        display="spinner"
        isVisible={isDatePickerVisible}
        mode="date"
        onConfirm={handleConfirm}
        onCancel={() => {
          hideDatePicker();
          if (!dob) {
            setFixError(true);
          }
        }}
        onHide={() => {
          phoneRef.current.focus();
        }}
        isDarkModeEnabled={isDarkModeEnabled}
        date={dob ? new Date(dob) : new Date()}
        maximumDate={new Date()}
      />

      <ScrollView ref={scrollView} contentContainerStyle={{ flexGrow: 1 }}>
        <View
          style={{
            flex: 1,
            backgroundColor: "#f4f4f4",
            alignItems: "center",
            marginBottom: 16,
          }}
        >
          <View
            style={{
              marginTop: 17,
              width: "87%",
            }}
          >
            <BackRed
              onPress={() => {
                props.navigation.goBack();
              }}
              text={Language.EN.editProfile.back}
            />
          </View>
          <InterBoldText
            text={Language.EN.editProfile.header}
            style={{
              fontSize: 24,
              width: "87%",
              marginBottom: 8,
              color: "#4d4d4d",
              marginTop: 48,
            }}
          />
          <InterRegularTextInput
            value={fName}
            label={Language.EN.regis.fname}
            ref={fNameRef}
            returnKeyType="next"
            onSubmitEditing={() => {
              lNameRef.current.focus();
            }}
            showError={true}
            onFocus={() => {
              moveScroll(64);
            }}
            onChangeText={(value) => {
              setFName(value);
            }}
          />
          <InterRegularTextInput
            label={Language.EN.regis.lname}
            ref={lNameRef}
            value={lName}
            showError={true}
            textError={Language.EN.regis.textError}
            returnKeyType="next"
            onSubmitEditing={() => {
              nameRef.current.focus();
            }}
            onFocus={() => {
              moveScroll(64 * 2);
            }}
            onChangeText={(value) => {
              setLName(value);
            }}
          />
          <InterRegularTextInput
            label={Language.EN.regis.name}
            ref={nameRef}
            value={name}
            returnKeyType="next"
            onSubmitEditing={() => {
              moveScroll(64 * 4);
              showDatePicker();
            }}
            showError={true}
            onFocus={() => {
              moveScroll(64 * 3);
            }}
            onChangeText={(value) => {
              setName(value);
            }}
          />
          <InterRegularTextInput
            label={Language.EN.regis.dob}
            ref={dobRef}
            returnKeyType="next"
            onSubmitEditing={() => {
              phoneRef.current.focus();
            }}
            editable={false}
            fixError={fixError}
            onPress={() => {
              moveScroll(64 * 4);
              showDatePicker();
            }}
          />
          <InterRegularTextInput
            label={Language.EN.regis.phone}
            ref={phoneRef}
            showError={true}
            textError={Language.EN.regis.textError}
            keyboardType="phone-pad"
            returnKeyType="next"
            onSubmitEditing={() => {
              lineRef.current.focus();
            }}
            onFocus={() => {
              moveScroll(64 * 5);
            }}
            onChangeText={(value) => {
              setPhone(value);
            }}
          />
          <InterRegularTextInput
            label={Language.EN.regis.line}
            ref={lineRef}
            returnKeyType="next"
            onSubmitEditing={() => {
              schoolRef.current.focus();
            }}
            onFocus={() => {
              moveScroll(64 * 6);
            }}
            onChangeText={(value) => {
              setLine(value);
            }}
          />
          <InterRegularTextInput
            label={Language.EN.regis.school}
            ref={schoolRef}
            onFocus={() => {
              moveScroll(64 * 7);
            }}
            returnKeyType={contact ? "next" : "done"}
            onChangeText={(value) => {
              setSchool(value);
            }}
            onSubmitEditing={() => {
              if (contact) {
                contacFnameRef.current.focus();
              } else {
              }
            }}
          />
          {contact ? (
            <View style={{ alignItems: "center", width: "100%" }}>
              <InterBoldText
                text={Language.EN.regis.contac}
                style={{
                  fontSize: 16,
                  marginTop: 32,
                  width: "87%",
                  color: "#4d4d4d",
                }}
              />

              <InterRegularTextInput
                label={Language.EN.regis.fname}
                ref={contacFnameRef}
                returnKeyType="next"
                showError={true}
                onSubmitEditing={() => {
                  contacLnameRef.current.focus();
                }}
                onFocus={() => {
                  moveScroll(64 * 8);
                }}
                onChangeText={(value) => {
                  setContactFName(value);
                }}
              />
              <InterRegularTextInput
                label={Language.EN.regis.lname}
                ref={contacLnameRef}
                returnKeyType="next"
                showError={true}
                onSubmitEditing={() => {
                  relationshipRef.current.focus();
                }}
                onFocus={() => {
                  moveScroll(64 * 9);
                }}
                onChangeText={(value) => {
                  setContactLName(value);
                }}
              />
              <InterRegularTextInput
                label={Language.EN.regis.relationship}
                ref={relationshipRef}
                returnKeyType="next"
                showError={true}
                onSubmitEditing={() => {
                  contacPhoneRef.current.focus();
                }}
                onFocus={() => {
                  moveScroll(64 * 10);
                }}
                onChangeText={(value) => {
                  setRelationship(value);
                }}
              />
              <InterRegularTextInput
                label={Language.EN.regis.phone}
                ref={contacPhoneRef}
                keyboardType="phone-pad"
                returnKeyType="next"
                onSubmitEditing={() => {
                  emailRef.current.focus();
                }}
                showError={true}
                onFocus={() => {
                  moveScroll(64 * 11);
                }}
                onChangeText={(value) => {
                  setContactPhone(value);
                }}
              />
              <InterRegularTextInput
                label={Language.EN.regis.emailContac}
                ref={emailRef}
                returnKeyType="next"
                keyboardType="email-address"
                showError={false}
                onSubmitEditing={() => {
                  contacLineRef.current.focus();
                }}
                onFocus={() => {
                  moveScroll(64 * 12);
                }}
                onChangeText={(value) => {
                  setContactEmail(value);
                }}
              />
              <InterRegularTextInput
                label={Language.EN.regis.line}
                ref={contacLineRef}
                returnKeyType="done"
                showError={false}
                onSubmitEditing={() => {}}
                onFocus={() => {
                  moveScroll(64 * 13);
                }}
                onChangeText={(value) => {
                  setContactLine(value);
                }}
              />
            </View>
          ) : null}
          <Button
            text={Language.EN.editProfile.save}
            activeOpacity={load ? 1 : buttonRegis() ? 0.2 : 1}
            style={{
              backgroundColor: load
                ? "#cccccc"
                : buttonRegis()
                ? "#ED1B24"
                : "#cccccc",
              marginBottom: keyboardHeight,
            }}
            onPress={() => {
              if (buttonRegis()) {
                onRegis();
              }
            }}
            isLoad={load}
          />
        </View>
      </ScrollView>
    </View>
  );
}
const mapStateToProps = (state) => {
  return {
    user: state.user.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    userLoginAction: bindActionCreators(userLoginAction, dispatch),
    userEditAction: bindActionCreators(userEditAction, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditPropfilePage);
