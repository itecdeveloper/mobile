import React from "react";
import { View, Text } from "react-native";
import BackRed from "../../components/BackRed";
import { InterBoldText, InterRegularText } from "../../components/StyledText";
import { Language } from "../../constants/language";
import QrCode from "../../components/QrCode";
import Important from "../../components/Important";
import PaymentOptionIcon from "../../components/PaymentOptionIcon";
import numeral from "numeral";
import moment from "moment";
import Button from "../../components/Button";
import { ScrollView } from "react-native-gesture-handler";

function BankTransferPage(props) {
  const time =
    props.route.bookData.dataOrder.CREATED_AT +
    props.route.bookData.dataOrder.ORDER_EXPIRATION_TIME * 1000;

  const expiration = moment(time).format("MMMM D, YYYY • HH:mm");
  const price = numeral(props.route.bookData.dataOrder.PRICE).format("0,0");
  return (
    <View>
      <ScrollView
        contentContainerStyle={{ flexGrow: 1 }}
        showsVerticalScrollIndicator={false}
      >
        <View style={{ flex: 1, backgroundColor: "#ffffff" }}>
          <View
            style={{
              alignItems: "center",
              borderBottomLeftRadius: 20,
              borderBottomRightRadius: 20,
              backgroundColor: "#ffffff",
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 1,
              },
              shadowOpacity: 0.2,
              shadowRadius: 1.41,
              elevation: 4,
            }}
          >
            <View
              style={{
                marginTop: 24,
                width: "87%",
              }}
            >
              <BackRed
                onPress={() => {
                  props.navigation.goBack();
                }}
              />
            </View>
            <View
              style={{
                width: "87%",
                backgroundColor: "#f8f7f7",
                marginTop: 16,
                alignItems: "center",
                borderRadius: 5,
                marginBottom: 23,
              }}
            >
              <InterRegularText
                text={Language.EN.bankTransfer.header}
                style={{
                  width: 266,
                  textAlign: "center",
                  marginTop: 8,
                  fontSize: 13,
                  color: "#ED1B24",
                }}
              />
              <InterBoldText
                text={expiration}
                style={{
                  width: 266,
                  textAlign: "center",
                  marginBottom: 8,
                  marginTop: 8,
                  fontSize: 13,
                  color: "#ED1B24",
                }}
              />
            </View>
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                marginBottom: 8,
              }}
            >
              <InterRegularText
                text={Language.EN.bankTransfer.amount}
                style={{
                  textAlign: "center",
                  color: "#4d4d4d",
                }}
              />
              <InterBoldText
                text={`${price} THB`}
                style={{
                  textAlign: "center",
                  color: "#4d4d4d",
                  fontSize: 18,
                }}
              />
            </View>
            <QrCode />
            <InterBoldText
              text={Language.EN.bankTransfer.name}
              style={{
                textAlign: "center",
                marginTop: 12,
                fontSize: 13,
                color: "#4d4d4d",
              }}
            />
            <InterBoldText
              text={Language.EN.bankTransfer.bankNumber}
              style={{
                textAlign: "center",
                marginBottom: 16,
                fontSize: 32,
                color: "#4d4d4d",
              }}
            />
            <PaymentOptionIcon
              style={{
                width: 73,
                height: 25,
                marginBottom: 24,
              }}
            />
          </View>

          <View
            style={{
              width: "100%",
              backgroundColor: "#ffffff",
              marginBottom: 136,

              alignItems: "center",
            }}
          >
            <View
              style={{
                flexDirection: "row",
                width: "87%",
                marginTop: 24,
                alignItems: "center",
              }}
            >
              <Important />
              <InterBoldText
                text={Language.EN.bankTransfer.important}
                style={{
                  color: "#ED1B24",
                  marginLeft: 12,
                }}
              />
            </View>
            <Text style={{ width: "87%", marginTop: 16 }}>
              <InterBoldText
                text={Language.EN.bankTransfer.confirm}
                style={{
                  color: "#4d4d4d",
                }}
              />
              <InterRegularText
                text={Language.EN.bankTransfer.information}
                style={{
                  color: "#4d4d4d",
                }}
              />
            </Text>
            <View style={{ width: "87%" }}>
              <InterRegularText
                text={Language.EN.bankTransfer.order}
                style={{
                  color: "#4d4d4d",
                  marginTop: 8,
                }}
              />
              <InterRegularText
                text={Language.EN.bankTransfer.courseName}
                style={{
                  color: "#4d4d4d",
                  marginTop: 8,
                }}
              />
              <InterRegularText
                text={Language.EN.bankTransfer.paymentSlip}
                style={{
                  color: "#4d4d4d",
                  marginTop: 8,
                }}
              />
              <InterRegularText
                text={Language.EN.bankTransfer.contact}
                style={{
                  color: "#4d4d4d",
                  marginTop: 24,
                }}
              />
              <View style={{ flexDirection: "row", marginTop: 8 }}>
                <InterRegularText
                  text={Language.EN.bankTransfer.lineLabel}
                  style={{
                    color: "#4d4d4d",
                    flex: 1,
                  }}
                />
                <InterBoldText
                  text={Language.EN.bankTransfer.line}
                  style={{
                    color: "#4d4d4d",
                    flex: 6,
                  }}
                />
              </View>
              <View style={{ flexDirection: "row", marginTop: 8 }}>
                <InterRegularText
                  text={Language.EN.bankTransfer.emailLabel}
                  style={{
                    color: "#4d4d4d",
                    flex: 1,
                  }}
                />
                <InterBoldText
                  text={Language.EN.bankTransfer.email}
                  style={{
                    color: "#4d4d4d",
                    flex: 6,
                  }}
                />
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
      <View
        style={{
          width: "100%",
          height: 120,
          position: "absolute",
          bottom: 0,
          alignItems: "center",
          backgroundColor: "#ffffff",
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 6,
          },
          shadowOpacity: 0.37,
          shadowRadius: 7.49,

          elevation: 12,
        }}
      >
        <Button
          text={Language.EN.bankTransfer.myCourse}
          style={{ backgroundColor: "#ED1B24" }}
          onPress={() => {
            //   props.onChange();
            props.navigation.navigate("MyCourse");
          }}
        />
      </View>
    </View>
  );
}

export default BankTransferPage;
