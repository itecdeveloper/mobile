import React, { Component, useRef, useState, useEffect } from "react";
import { View, TouchableOpacity, StyleSheet, ScrollView } from "react-native";
import {
  InterRegularTextInput,
  InterRegularText,
  InterBoldText,
} from "../../components/StyledText";
import { Language } from "../../constants/language";
import CustomCheckBox from "../../components/CustomCheckBox";
import Button from "../../components/Button";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as userLoginAction from "../../actions/userLoginAction";
import UseKeyboard from "../../components/UseKeyboard";
import Constants from "expo-constants";
import BackRed from "../../components/BackRed";
import * as Crypto from "expo-crypto";
import registerForPushNotificationsAsync from "../../components/Notification/registerForPushNotificationsAsync";
import AwesomeAlert from "react-native-awesome-alerts";
import _ from "lodash";

function LoginPage(props) {
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  // const [userName, setUserName] = useState("pakkaju96@hotmail.com");
  // const [password, setPassword] = useState("1111");
  const [keyboardHeight, setKeyboardHeight] = useState(0);
  const [load, setLoad] = useState(false);
  const [error, setError] = useState(false);
  const emailRef = React.createRef();
  const passwordRef = React.createRef();
  const scrollView = React.createRef();

  const onSubmitEmail = () => {
    passwordRef.current.focus();
  };

  const keyboard = UseKeyboard();
  useEffect(() => {
    setKeyboardHeight(keyboard);
  }, [keyboard]);

  async function login() {
    if (userName && password) {
      setLoad(true);
      const newPassword = await Crypto.digestStringAsync(
        Crypto.CryptoDigestAlgorithm.SHA512,
        password
      );
      let token = "";
      try {
        token = await registerForPushNotificationsAsync();
      } catch (e) {
        console.log("Error ", e);
      }

      //  console.log(token);
      const body = {
        EMAIL: _.toLower(userName),
        PASSWORD: newPassword,
        NOTI_TOKEN: token,
      };
      const login = await props.userLoginAction.userLogin(body);
      if (login) {
        //  props.navigation.goBack();
        setLoad(false);
        if (props.onLogin) {
          props.onLogin();
        }
      } else {
        setError(true);
      }
    }
  }
  function moveScroll(value) {
    scrollView.current.scrollTo({ y: value });
  }

  return (
    <View style={{ flex: 1 }}>
      <View style={{ height: Constants.statusBarHeight }} />

      <ScrollView ref={scrollView} contentContainerStyle={{ flexGrow: 1 }}>
        <View
          style={{
            flex: 1,
            alignItems: "center",
            width: "100%",
          }}
        >
          <View
            style={{
              marginTop: 17,
              width: "87%",
            }}
          >
            <BackRed
              onPress={() => {
                props.navigation.goBack();
              }}
            />
          </View>
          <InterBoldText
            text={Language.EN.login.textLogin}
            style={{
              fontSize: 24,
              marginBottom: 24,
              marginTop: 56,
              width: "87%",
              color: "rgba(0, 0, 0, 0.7)",
            }}
          />
          <InterRegularTextInput
            label={Language.EN.login.email}
            ref={emailRef}
            keyboardType="email-address"
            returnKeyType="next"
            onSubmitEditing={onSubmitEmail}
            showError={false}
            value={userName}
            onChangeText={(userName) => {
              const email = _.toLower(userName);
              setUserName(email);
            }}
            onFocus={() => {
              moveScroll(64);
            }}
          />
          <InterRegularTextInput
            label={Language.EN.login.password}
            ref={passwordRef}
            secureTextEntry={true}
            showError={false}
            value={password}
            onSubmitEditing={login}
            onChangeText={(password) => {
              setPassword(password);
            }}
            onFocus={() => {
              moveScroll(64 * 2);
            }}
          />

          <View style={{ width: "87%", flexDirection: "row", marginTop: 16 }}>
            <View
              style={{
                flex: 1,

                justifyContent: "center",
              }}
            ></View>

            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "flex-end",
              }}
            >
              <TouchableOpacity
                onPress={() => {
                  props.navigation.navigate("ForgotPassword");
                }}
              >
                <InterBoldText
                  text={Language.EN.login.forgot}
                  style={{ color: "#ED1B24" }}
                />
              </TouchableOpacity>
            </View>
          </View>
          <Button
            text={Language.EN.login.buttonLogin}
            activeOpacity={load ? 1 : userName && password ? 0.2 : 1}
            isLoad={load}
            onPress={login}
            style={{
              backgroundColor: load
                ? "#cccccc"
                : userName && password !== ""
                ? "#ED1B24"
                : "#cccccc",
            }}
          />
          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate("Regis", { dataPage: props.dataPage });
            }}
          >
            <InterBoldText
              text={Language.EN.login.createAccount}
              style={{
                fontSize: 18,
                marginTop: 16,
                color: "#ED1B24",
                fontWeight: "bold",
                marginBottom: 20 + keyboardHeight,
              }}
            />
          </TouchableOpacity>
        </View>
      </ScrollView>
      <AwesomeAlert
        show={error}
        showProgress={false}
        title={Language.EN.login.title}
        message={Language.EN.login.message}
        closeOnTouchOutside={true}
        closeOnHardwareBackPress={false}
        showCancelButton={false}
        showConfirmButton={true}
        confirmText={Language.EN.login.confirm}
        confirmButtonColor="#ED1B24"
        titleStyle={{
          fontSize: 18,
          color: "#333333",
          fontFamily: "Inter-Bold",
        }}
        confirmButtonTextStyle={{ fontFamily: "Inter-Bold" }}
        messageStyle={{
          textAlign: "center",
          fontSize: 14,
          color: "#333333",
          fontFamily: "Inter-Regular",
        }}
        onConfirmPressed={() => {
          setLoad(false);
          setError(false);
        }}
        onDismiss={() => {
          setLoad(false);
          setError(false);
        }}
      />
    </View>
  );
}
const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    userLoginAction: bindActionCreators(userLoginAction, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);

const styles = StyleSheet.create({
  checkbox: {
    backgroundColor: "#f4f4f4",
    borderWidth: 0,
    marginLeft: 0,
    marginTop: 0,
    alignItems: "flex-start",
  },
  labelCheckbox: {
    fontSize: 14,
    color: "#9b9b9b",
    fontWeight: "normal",
    marginLeft: 5,
  },
  textAccount: {
    fontSize: 18,
    marginTop: 16,
    color: "#ED1B24",
    fontWeight: "bold",
    marginBottom: 20,
  },
});
