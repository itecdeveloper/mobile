import React, { useState, useEffect } from "react";
import { View, TouchableOpacity, StyleSheet, BackHandler } from "react-native";
import Mail from "../../components/Mail";
import { InterBoldText, InterRegularText } from "../../components/StyledText";
import { Language } from "../../constants/language";
import Button from "../../components/Button";
import NextRed from "../../components/NextRed";
import * as paymentAction from "../../actions/paymentAction";
import Success from "./Success";

function ThankPage(props) {
  BackHandler.addEventListener("hardwareBackPress", function () {
    if (props.navigation.isFocused()) {
      return true;
    }
    return false;
  });
  const bookData = props.route.bookData;
  useEffect(() => {
    getOder();
  }, []);

  // console.log(bookData.data);

  async function getOder() {
    const order = await paymentAction.getOrder(
      bookData.dataOrder.ORDER_ID,
      bookData.dataUser
    );
    // console.log(order);
  }

  return (
    <Success
      courseName={bookData.data.COURSE_NAME}
      email={bookData.dataUser.detail.EMAIL}
      goHome={() => {
        props.navigation.navigate("Home");
      }}
      goMyCourse={() => {
        props.navigation.navigate("MyCourse");
      }}
    />
  );
}

export default ThankPage;
