import React, { useState, useEffect } from "react";
import { View, TouchableOpacity, StyleSheet, BackHandler } from "react-native";
import Mail from "../../components/Mail";
import { InterBoldText, InterRegularText } from "../../components/StyledText";
import { Language } from "../../constants/language";
import Button from "../../components/Button";
import NextRed from "../../components/NextRed";

function Success(props) {
  const arr = props.courseName.split(" / ");
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: "#ffffff",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Mail />
      <InterRegularText
        text={Language.EN.thank.success}
        style={{ color: "#7a7a7a", fontSize: 16, marginTop: 32 }}
      />
      <InterRegularText
        text={arr[0]}
        style={{
          width: "87%",
          textAlign: "center",
          fontSize: 24,
          marginTop: 4,
        }}
      />
      {arr[1] ? (
        <InterRegularText
          text={arr[1]}
          style={{
            width: "87%",
            textAlign: "center",
            fontSize: 24,
          }}
        />
      ) : null}
      <InterRegularText
        text={Language.EN.thank.confrimation}
        style={{
          color: "#4a4a4a",
          fontSize: 16,
          marginTop: 16,
          width: 300,
          textAlign: "center",
        }}
      />
      <InterBoldText
        text={props.email}
        style={{
          color: "#4a4a4a",
          fontSize: 16,
          width: "87%",
          textAlign: "center",
        }}
      />
      <InterRegularText
        text={Language.EN.thank.note}
        style={{
          color: "#4a4a4a",
          fontSize: 16,
          marginTop: 16,
          width: 300,
          textAlign: "center",
        }}
      />
      <Button
        style={{ backgroundColor: "#ED1B24" }}
        text={Language.EN.thank.mycourse}
        onPress={() => {
          props.goMyCourse();
        }}
      />
      <TouchableOpacity
        style={{ flexDirection: "row", marginTop: 16, alignItems: "center" }}
        onPress={() => {
          props.goHome();
        }}
      >
        <InterBoldText
          text={Language.EN.thank.browse}
          style={{ color: "#ED1B24", marginRight: 12 }}
        />
        <NextRed />
      </TouchableOpacity>
    </View>
  );
}

export default Success;
