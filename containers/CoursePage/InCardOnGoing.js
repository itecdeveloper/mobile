import React, { useState, useEffect } from "react";
import { View, Dimensions, TouchableOpacity } from "react-native";
import Calendar from "../../components/Calendar";
import Constants from "expo-constants";
import { Language } from "../../constants/language";
import { InterBoldText, InterRegularText } from "../../components/StyledText";
import _ from "lodash";
import Progress from "./Progress";
import * as myCourseAction from "../../actions/myCourseAction";

function InCardOnGoing(props) {
  const cardWidth = "87%";
  const max = props.item.SESSION_QUANTITY;
  const [name, setName] = useState("");
  const [detail, setDetail] = useState(false);
  const [level, setLevel] = useState("");
  const [student, setStudent] = useState("");
  const [success, setSuccess] = useState(0);
  useEffect(() => {
    let groupSession = _.groupBy(props.session, "ORDER_ID");
    Object.keys(groupSession).map((row) => {
      if (props.item.ORDER_ID === row) {
        let success = _.reduce(
          groupSession[row],
          function (sum, n) {
            let num = n.S_STATUS === "COMPLETE" ? 1 : 0;
            return sum + num;
          },
          0
        );
        setSuccess(success);
      }
    });
  }, []);

  useEffect(() => {
    async function getCourse() {
      const res = await myCourseAction.getCourse(
        props.user,
        props.item.COURSE_ID
      );
      if (res) {
        const arr = res.DETAIL.COURSE_NAME.split(" / ");
        setName(arr[0]);
        if (arr[1]) {
          setDetail(arr[1]);
        }
      }
    }
    async function getCourseLevel() {
      const res = await myCourseAction.getCourseLevel(
        props.user,
        props.item.LEVEL_ID
      );
      if (res) {
        setLevel(res.LEVEL);
      }
    }
    if (props.item.LEVEL_ID !== "-") {
      getCourseLevel();
    } else {
      setLevel("-");
    }
    async function getAccount() {
      const res = await myCourseAction.getAccount(
        props.user,
        props.item.STUDENT_ID
      );
      if (res) {
        setStudent(`${res.Items[0].FISRT_NAME} ${res.Items[0].LAST_NAME}`);
      }
    }
    getAccount();

    getCourse();
  }, []);

  return (
    <View key={props.index} style={{ width: "100%", alignItems: "center" }}>
      <TouchableOpacity
        style={{
          width: cardWidth,
          backgroundColor: "#ffffff",
          borderRadius: 10,
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.2,
          shadowRadius: 1.41,
          elevation: 2,
          marginBottom: 8,
        }}
        onPress={() => {
          props.onPress({
            user: props.user,
            item: {
              ...props.item,
              courseName: name,
              levelName: level,
              detail: detail,
            },
          });
        }}
      >
        <View
          style={{
            marginLeft: 12,
            marginRight: 12,
          }}
        >
          <InterBoldText
            text={name}
            style={{
              marginTop: 12,
              marginBottom: 7,
              color: "#4d4d4d",
              fontSize: 13,
            }}
            ellipsizeMode="tail"
            numberOfLines={1}
          />
          {props.user.detail.TYPE === "TEACHER" ? (
            <InterBoldText
              text={student}
              style={{
                marginBottom: 7,
                color: "#999999",
                fontSize: 13,
              }}
              ellipsizeMode="tail"
              numberOfLines={1}
            />
          ) : null}
          <Progress
            success={success}
            max={max}
            text={"Lv. " + level}
            style={{ marginBottom: 16 }}
          />
        </View>
      </TouchableOpacity>
    </View>
  );
}

export default InCardOnGoing;
