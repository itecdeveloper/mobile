import React, { useState, useEffect } from "react";
import { View, Dimensions, TouchableOpacity, Text } from "react-native";
import { Language } from "../../constants/language";
import { InterBoldText, InterRegularText } from "../../components/StyledText";
import _ from "lodash";
import * as myCourseAction from "../../actions/myCourseAction";
export default function InCardCompleted(props) {
  const { index, item, user } = props;
  const cardWidth = "87%";
  const max = props.item.SESSION_QUANTITY;
  const [name, setName] = useState("");
  const [level, setLevel] = useState("");
  const [student, setStudent] = useState("");
  const [success, setSuccess] = useState(0);
  useEffect(() => {
    let groupSession = _.groupBy(props.session, "ORDER_ID");
    Object.keys(groupSession).map((row) => {
      if (props.item.ORDER_ID === row) {
        let success = _.reduce(
          groupSession[row],
          function (sum, n) {
            let num = n.S_STATUS === "COMPLETE" ? 1 : 0;
            return sum + num;
          },
          0
        );
        setSuccess(success);
      }
    });
  }, []);

  useEffect(() => {
    async function getCourse() {
      const res = await myCourseAction.getCourse(
        props.user,
        props.item.COURSE_ID
      );
      if (res) {
        const arr = res.DETAIL.COURSE_NAME.split(" / ");
        setName(arr[0]);
      }
    }
    async function getCourseLevel() {
      const res = await myCourseAction.getCourseLevel(
        props.user,
        props.item.LEVEL_ID
      );
      if (res) {
        setLevel(res.LEVEL);
      }
    }
    if (props.item.LEVEL_ID !== "-") {
      getCourseLevel();
    } else {
      setLevel("-");
    }
    async function getAccount() {
      const res = await myCourseAction.getAccount(
        props.user,
        props.item.STUDENT_ID
      );
      if (res) {
        setStudent(`${res.Items[0].FISRT_NAME} ${res.Items[0].LAST_NAME}`);
      }
    }
    getAccount();

    getCourse();
  }, []);

  return (
    <TouchableOpacity
      style={{
        width: "100%",
        alignItems: "center",
        marginBottom: 12,
      }}
      key={index}
      onPress={() => {
        props.onPress({
          user: props.user,
          item: {
            ...props.item,
            courseName: name,
            levelName: level,
          },
        });
      }}
    >
      <View
        style={{
          width: "100%",
          borderColor: "#e6e6e6",
          borderTopWidth: index == 0 ? 0 : 1,
          flexDirection: "row",
        }}
      >
        <Text
          ellipsizeMode="tail"
          numberOfLines={1}
          style={{ marginTop: 15, marginLeft: 16, marginRight: 16 }}
        >
          <InterBoldText
            text={name}
            style={{
              marginTop: 15,
              marginLeft: 16,

              color: "#333333",
            }}
          />
          <InterBoldText
            text={" Lv. " + level}
            style={{
              marginTop: 15,
              fontSize: 13,
              color: "#999999",
            }}
          />
        </Text>
      </View>
    </TouchableOpacity>
  );
}
