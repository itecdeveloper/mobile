import React, { useState, useEffect } from "react";
import { View, Dimensions, TouchableOpacity, FlatList } from "react-native";
import { Language } from "../../constants/language";
import { InterBoldText, InterRegularText } from "../../components/StyledText";
import InCardOnGoing from "./InCardOnGoing";
import _ from "lodash";

function CardOnGoing(props) {
  let data = [];
  const onGoing = _.reduce(
    props.data,
    function (sum, n) {
      let paid = 0;
      if (n.STATUS === "PAID") {
        paid = 1;
        data.push(n);
      }
      return sum + paid;
    },
    0
  );

  return (
    <View style={{ width: "100%", alignItems: "center", marginBottom: 28 }}>
      <View style={{ flexDirection: "row", width: "87%" }}>
        <InterBoldText
          text={Language.EN.course.onGoing}
          style={{
            color: "#4d4d4d",
            fontSize: 16,
          }}
        />
        <InterBoldText
          text={`(${onGoing})`}
          style={{
            color: "#808080",
            fontSize: 15,
          }}
        />
      </View>

      <FlatList
        style={{ width: "100%", marginTop: 8 }}
        data={data}
        renderItem={({ item, index }) => (
          <InCardOnGoing
            item={item}
            session={props.session}
            index={index}
            user={props.user}
            onPress={(data) => {
              props.onPress(data);
            }}
          />
        )}
        keyExtractor={(item, index) => index.toString()}
        disableVirtualization
      />
    </View>
  );
}

export default CardOnGoing;
