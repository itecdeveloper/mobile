import React, { useState, useEffect } from "react";
import { View, Dimensions, TouchableOpacity } from "react-native";
import BackRed from "../../components/BackRed";
import { Language } from "../../constants/language";
import { InterBoldText, InterRegularText } from "../../components/StyledText";
import CardOnGoing from "./CardOnGoing";
import CardCompleated from "./CardCompleted";
import _ from "lodash";

const CoursePage = (props) => {
  const [onGoing, setOnGoing] = useState([]);
  const [session, setSession] = useState(props.route.course);
  useEffect(() => {
    let groupSession = _.groupBy(props.route.course, "ORDER_ID");
    let sessionPending = [];
    let sessionGoin = [];
    // Object.keys(groupSession).map((row) => {
    //   const data = groupSession[row];
    //   const dataPending = [];
    //   data.map((statusPending) => {
    //     if (statusPending.S_STATUS === "COMPLETE") {
    //       dataPending.push(statusPending);
    //     }
    //   });
    //   if (dataPending.length > 0) {
    //     sessionPending.push(dataPending);
    //   }
    // });
    props.route.groupCourse.map((item) => {
      const pen = _.reduce(
        groupSession[item.ORDER_ID],
        function (sum, n) {
          let paid = 0;
          if (n.S_STATUS === "COMPLETE") {
            paid = 1;
          }
          return sum + paid;
        },
        0
      );

      if (pen >= item.SESSION_QUANTITY) {
        sessionPending.push(item);
      } else {
        sessionGoin.push(item);
      }
    });
    setOnGoing(sessionGoin);
    setPending(sessionPending);
  }, []);

  const [pending, setPending] = useState([]);

  // console.log(props);

  return (
    <View style={{ flex: 1, backgroundColor: "#F8F7F7" }}>
      <View
        style={{
          alignItems: "center",
          borderBottomLeftRadius: 20,
          borderBottomRightRadius: 20,
          backgroundColor: "#f8f7f7",
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.2,
          shadowRadius: 1.41,
          elevation: 4,
          marginBottom: 28,
        }}
      >
        <View
          style={{
            marginTop: 24,
            width: "87%",
          }}
        >
          <BackRed
            text={Language.EN.reschedule.myClass}
            onPress={() => {
              props.navigation.goBack();
            }}
          />
        </View>
        <InterBoldText
          text={Language.EN.course.header}
          style={{
            fontSize: 24,
            color: "#333333",
            marginTop: 17,
            width: "87%",
            marginBottom: 16,
          }}
        />
      </View>

      <CardOnGoing
        user={props.route.user}
        data={onGoing}
        session={session}
        onPress={(item) => {
          const mySession = _.groupBy(session, "ORDER_ID");

          const data = {
            ...item,
            session: mySession[item.item.ORDER_ID]
              ? mySession[item.item.ORDER_ID]
              : [],
          };
          props.navigation.navigate("CourseDetail", { data: data });
        }}
      />

      {pending.length > 0 ? (
        <CardCompleated
          data={pending}
          session={session}
          user={props.route.user}
          onPress={(item) => {
            const mySession = _.groupBy(session, "ORDER_ID");
            const data = {
              ...item,
              session: mySession[item.item.ORDER_ID]
                ? mySession[item.item.ORDER_ID]
                : [],
            };
            props.navigation.navigate("CourseDetail", { data: data });
          }}
        />
      ) : null}
    </View>
  );
};

export default CoursePage;
