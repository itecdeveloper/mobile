import React, { useState, useEffect } from "react";
import { View, Dimensions, FlatList } from "react-native";
import { Language } from "../../constants/language";
import { InterBoldText, InterRegularText } from "../../components/StyledText";
import InCardCompleated from "./InCardCompleated";
export default function CardCompleted(props) {
  return (
    <View style={{ width: "100%", alignItems: "center", marginBottom: 20 }}>
      <View
        style={{
          width: "87%",
          borderRadius: 10,
          backgroundColor: "#f8f7f7",
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.23,
          shadowRadius: 2.62,
          elevation: 3,
        }}
      >
        <View style={{ flexDirection: "row" }}>
          <InterBoldText
            text={Language.EN.course.complete}
            style={{
              marginTop: 15,
              marginLeft: 16,

              color: "#808080",
            }}
          />
          <InterBoldText
            text={` (${props.data.length})`}
            style={{
              marginTop: 15,
              marginRight: 16,
              color: "#bbbbbb",
            }}
          />
        </View>
        <View
          style={{
            borderRadius: 10,
            backgroundColor: "#ffffff",
            marginTop: 9,
          }}
        >
          <FlatList
            style={{ width: "100%" }}
            data={props.data}
            renderItem={({ item, index }) => (
              <InCardCompleated
                item={item}
                index={index}
                user={props.user}
                onPress={(data) => {
                  props.onPress(data);
                }}
              />
            )}
            keyExtractor={(item, index) => index.toString()}
            disableVirtualization
          />
        </View>
      </View>
    </View>
  );
}
