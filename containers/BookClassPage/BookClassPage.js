import React, { useState, useEffect } from "react";
import { View, Dimensions, Platform } from "react-native";
import { Language } from "../../constants/language";
import {
  InterBoldText,
  InterRegularTextInput,
  InterRegularText,
} from "../../components/StyledText";
import CloseGrey from "../../components/CloseGrey";
import Button from "../../components/Button";
import { ScrollView } from "react-native-gesture-handler";
import moment from "moment";
import Important from "../../components/Important";
import HeaderDetail from "./HeaderDetail";
import { Dropdown } from "react-native-material-dropdown";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { Appearance } from "react-native-appearance";
import * as myCourseAction from "../../actions/myCourseAction";
import Toast, { DURATION } from "react-native-easy-toast";
import Constants from "expo-constants";
import SegmentedPicker from "react-native-segmented-picker";
import numeral from "numeral";
import { setCourse } from "../../actions/courseAction";
import { checkTime } from "../../components/Func";
import AwesomeAlert from "react-native-awesome-alerts";

const BookClassPage = (props) => {
  const item = props.route.data.item;
  const sessionTime = item.SESSION_TIME / 60;
  const [countSession, setCountSession] = useState(
    `${props.route.data.book}/${item.SESSION_QUANTITY}`
  );

  const dateRef = React.useRef();
  const timeRef = React.useRef();
  const toastRef = React.useRef();
  const segmentedPickerRef = React.useRef();
  const [session, setSession] = useState([]);
  const [num, setNum] = useState(1);
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [isTimePickerVisible, setTimePickerVisibility] = useState(false);
  const [date, setDate] = useState("");
  const [time, setTime] = useState("");
  const [isLoad, setIsLoad] = useState(false);
  const [hhAndroid, sethhAndroid] = useState("");
  const [mmAndroid, setmmAndroid] = useState("");
  const [bookingType, setBookingType] = useState(
    Language.EN.bookClass.type[0].value
  );

  const [error, setError] = useState(false);

  let isDarkModeEnabled = false;
  let colorScheme = Appearance.getColorScheme();
  if (colorScheme === "dark") {
    isDarkModeEnabled = true;
  }

  const HH = () => {
    let h = [];
    for (let i = 0; i < 24; i++) {
      h.push({ label: numeral(i).format("00") });
    }
    return h;
  };

  const mm = () => {
    let m = [];
    for (let i = 0; i < 12; i++) {
      m.push({ label: numeral(5 * i).format("00") });
    }
    return m;
  };

  useEffect(() => {
    numSession();
  }, []);

  async function createSession(dateTime) {
    const body = {
      SESSION_START_TIME: dateTime,
    };
    const res = await myCourseAction.createSessions(
      props.route.data.user,
      item.ORDER_ID,
      body
    );
    if (res) {
      setCountSession(
        `${props.route.data.book + num}/${item.SESSION_QUANTITY}`
      );
      setIsLoad(false);
      setNum(1);
      setDate("");
      setTime("");
      dateRef.current.setValue("");
      timeRef.current.setValue("");
      // toastRef.current.show(Language.EN.bookClass.success);

      props.navigation.goBack();
    } else {
      setIsLoad(false);
      toastRef.current.show(Language.EN.bookClass.fail);
    }
  }

  function numSession() {
    const count = item.SESSION_QUANTITY - props.route.data.book;
    for (let i = 1; i <= count; i++) {
      const num = {
        value: i,
      };
      session.push(num);
    }
    return session;
  }

  function dateTimeSession(dateTime) {
    const bookingTime = [];
    for (let i = 0; i < num; i++) {
      const type =
        bookingType === Language.EN.bookClass.type[1].value ? "day" : "week";
      const time = moment(parseInt(dateTime)).add(i, type).format("x");
      bookingTime.push(time);

      const res = checkTime({
        time: time,
        type: "bookClass",
        limitTime: item.time,
        exp: moment(item.exp).format("x"),
      });

      if (!res) {
        return false;
      }
    }
    return bookingTime;
  }

  const handleDate = (date) => {
    const dateTime = moment(date).format("dddd D MMMM YYYY");
    dateRef.current.setValue(dateTime);
    setDatePickerVisibility(false);
    setDate(date);
  };
  const handleTime = (time) => {
    const dateTime = moment(time).format("HH:mm");
    timeRef.current.setValue(dateTime);
    setTimePickerVisibility(false);
    setTime(time);
  };

  const handleTimeAndroid = (value) => {
    let dateTime = `${value.column1.label}:${value.column2.label}`;
    sethhAndroid(value.column1.label);
    setmmAndroid(value.column2.label);
    timeRef.current.setValue(dateTime);
    const dateNow = moment().format("YYYY-MM-DD ");
    dateTime = dateNow + dateTime;
    setTime(dateTime);
  };
  const checkSubmit = () => {
    let res = false;
    if (date && time) {
      const d = moment(date).format("YYYY-MM-DD ");
      const t = moment(time).format("HH:mm");
      let dateTime = d + t;
      dateTime = moment(dateTime).format("x");
      res = dateTime;
    }
    return res;
  };
  const onSubmit = async () => {
    const dateTime = checkSubmit();
    if (dateTime) {
      const finalTime = dateTimeSession(dateTime);

      if (finalTime) {
        setIsLoad(true);
        await createSession(finalTime);
      } else {
        setError(true);
      }
    }
    //  toastRef.current.show(Language.EN.bookClass.fail);
  };
  return (
    <ScrollView
      style={{ flex: 1, backgroundColor: "#F8F7F7" }}
      contentContainerStyle={{ flexGrow: 1, alignItems: "center" }}
    >
      <DateTimePickerModal
        isVisible={isDatePickerVisible}
        mode="date"
        onConfirm={handleDate}
        onCancel={() => {
          setDatePickerVisibility(false);
        }}
        onHide={() => {
          setDatePickerVisibility(false);
        }}
        isDarkModeEnabled={isDarkModeEnabled}
        date={date ? date : new Date()}
        display={"spinner"}
        minimumDate={new Date()}
      />

      <DateTimePickerModal
        isVisible={isTimePickerVisible}
        mode="time"
        onConfirm={handleTime}
        onCancel={() => {
          setTimePickerVisibility(false);
        }}
        locale="en_GB"
        onHide={() => {
          setTimePickerVisibility(false);
        }}
        date={time ? time : new Date()}
        isDarkModeEnabled={isDarkModeEnabled}
        display={"spinner"}
        minuteInterval={5}
      />

      <SegmentedPicker
        defaultSelections={{
          column1: hhAndroid ? hhAndroid : "00",
          column2: mmAndroid ? mmAndroid : "00",
        }}
        ref={segmentedPickerRef}
        onConfirm={() => {}}
        options={{
          column1: HH(),
          column2: mm(),
        }}
        size={30}
        onConfirm={handleTimeAndroid}
      />

      <Toast
        style={{
          backgroundColor: "#32323290",
          opacity: 80,
          borderRadius: 50,
        }}
        positionValue={Constants.statusBarHeight + 70}
        fadeInDuration={750}
        fadeOutDuration={1000}
        position="bottom"
        ref={toastRef}
      />

      <View style={{ marginTop: 27, width: "87%", flexDirection: "row" }}>
        <View style={{ flex: 1, flexDirection: "row", alignItems: "center" }}>
          <Important />
          <InterBoldText
            text={Language.EN.bookClass.booking}
            style={{ color: "#ED1B24", marginLeft: 8, fontSize: 16 }}
          />
        </View>

        <CloseGrey
          onPress={() => {
            props.navigation.goBack();
          }}
        />
      </View>
      <HeaderDetail
        label={Language.EN.bookClass.course}
        text={item.courseName}
        containStyle={{ marginTop: 16 }}
      />
      <HeaderDetail
        label={Language.EN.bookClass.class}
        text={sessionTime + Language.EN.bookClass.minutes}
      />
      <HeaderDetail label={Language.EN.bookClass.session} text={countSession} />

      <View
        style={{
          width: "87%",
          backgroundColor: "#ffffff",
          borderRadius: 5,
          marginTop: 16,
        }}
      >
        <Dropdown
          lineWidth={0}
          containerStyle={{ marginLeft: 8, marginRight: 8 }}
          labelTextStyle={{ fontFamily: "Inter-Regular" }}
          textStyle={{ fontFamily: "Inter-Regular" }}
          label={Language.EN.bookClass.bookingType}
          data={Language.EN.bookClass.type}
          value={bookingType}
          onChangeText={async (value, index) => {
            setNum(1);
            setBookingType(value);
          }}
          animationDuration={0}
        />
      </View>
      {bookingType !== Language.EN.bookClass.type[0].value ? (
        <View
          style={{
            width: "87%",
            backgroundColor: "#ffffff",
            borderRadius: 5,
            marginTop: 16,
          }}
        >
          <Dropdown
            lineWidth={0}
            containerStyle={{ marginLeft: 8, marginRight: 8 }}
            labelTextStyle={{ fontFamily: "Inter-Regular" }}
            textStyle={{ fontFamily: "Inter-Regular" }}
            label={Language.EN.bookClass.number}
            data={session}
            value={num}
            onChangeText={async (value, index) => {
              setNum(value);
            }}
            animationDuration={0}
          />
        </View>
      ) : null}
      <InterBoldText
        text={Language.EN.bookClass.select}
        style={{ width: "87%", marginTop: 16 }}
      />
      <InterRegularTextInput
        ref={dateRef}
        label={Language.EN.bookClass.date}
        onPress={() => {
          setDatePickerVisibility(true);
        }}
        editable={false}
        showIconDate={true}
      />
      <InterRegularTextInput
        ref={timeRef}
        label={Language.EN.bookClass.time}
        editable={false}
        onPress={() => {
          if (Platform.OS === "android") {
            segmentedPickerRef.current.show();
          } else {
            setTimePickerVisibility(true);
          }
        }}
        showIconTime={true}
      />
      <InterRegularText
        text={Language.EN.bookClass.note}
        style={{ width: "87%", marginTop: 10, color: "#999999" }}
      />
      <Button
        text={Language.EN.bookClass.submit}
        styleText={{ fontSize: 14 }}
        style={{
          marginTop: 24,
          backgroundColor: isLoad
            ? "#cccccc"
            : checkSubmit()
            ? "#ED1B24"
            : "#cccccc",
        }}
        styleActivityIndicator={{ marginRight: 160 }}
        activeOpacity={isLoad ? 1 : checkSubmit() ? 0.2 : 1}
        onPress={onSubmit}
        isLoad={isLoad}
      />
      <AwesomeAlert
        show={error}
        showProgress={false}
        title={Language.EN.bookClass.title}
        message={Language.EN.bookClass.message}
        closeOnTouchOutside={true}
        closeOnHardwareBackPress={false}
        showCancelButton={false}
        showConfirmButton={true}
        confirmText={Language.EN.login.confirm}
        confirmButtonColor="#ED1B24"
        titleStyle={{
          fontSize: 18,
          color: "#333333",
          fontFamily: "Inter-Bold",
        }}
        confirmButtonTextStyle={{ fontFamily: "Inter-Bold" }}
        messageStyle={{
          textAlign: "center",
          fontSize: 14,
          color: "#333333",
          fontFamily: "Inter-Regular",
        }}
        onConfirmPressed={() => {
          setError(false);
        }}
        onDismiss={() => {
          setError(false);
        }}
      />
    </ScrollView>
  );
};

export default BookClassPage;
