import React, { useState, useEffect } from "react";
import { View, TouchableOpacity, Platform, Dimensions } from "react-native";
import Modal from "react-native-modalbox";
import { InterBoldText, InterRegularText } from "../../components/StyledText";
import { ScrollView } from "react-native-gesture-handler";
import { Language } from "../../constants/language";

const modalHeight = Dimensions.get("window").height * 0.742;

export default function ModalBox(props) {
  return (
    <Modal
      onClosed={() => {
        props.onClosed();
      }}
      isOpen={props.isOpen}
      position="bottom"
      style={{
        backgroundColor: "white",
        height: modalHeight,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
      }}
      swipeArea={100}
    >
      <View
        style={{
          width: "100%",
          backgroundColor: "#ffffff",
          alignItems: "center",
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
          height: modalHeight,
        }}
      >
        <InterBoldText
          text={Language.EN.regis.termModal}
          style={{
            width: "83%",
            fontSize: 24,
            marginTop: 32,
            color: "#4d4d4d",
          }}
        />
        <ScrollView
          style={{
            width: "100%",
            marginTop: 16,
          }}
          contentContainerStyle={{ alignItems: "center" }}
        >
          <InterBoldText
            text={Language.EN.regis.labelTerm}
            style={{
              width: "83%",
              fontSize: 16,
              marginBottom: 8,
              color: "#4d4d4d",
            }}
          />
          <InterRegularText
            text={Language.EN.regis.termText}
            style={{
              width: "83%",
              fontSize: 14,
              marginBottom: 16,
              color: "#4d4d4d",
            }}
          />
          <InterBoldText
            text={Language.EN.regis.labelCon}
            style={{
              width: "83%",
              fontSize: 16,
              marginBottom: 8,
              color: "#4d4d4d",
            }}
          />
          <InterRegularText
            text={Language.EN.regis.conText}
            style={{
              width: "83%",
              fontSize: 14,
              marginBottom: 16,
              color: "#4d4d4d",
            }}
          />
          <InterBoldText
            text={Language.EN.regis.labelDis}
            style={{
              width: "83%",
              fontSize: 16,
              marginBottom: 8,
              color: "#4d4d4d",
            }}
          />
          <InterRegularText
            text={Language.EN.regis.disText}
            style={{
              width: "83%",
              fontSize: 14,
              marginBottom: 16,
              color: "#4d4d4d",
            }}
          />
          <InterBoldText
            text={Language.EN.regis.labelLim}
            style={{
              width: "83%",
              fontSize: 16,
              marginBottom: 8,
              color: "#4d4d4d",
            }}
          />
          <InterRegularText
            text={Language.EN.regis.limText}
            style={{
              width: "83%",
              fontSize: 14,
              marginBottom: 16,
              color: "#4d4d4d",
            }}
          />
          <InterBoldText
            text={Language.EN.regis.labelContact}
            style={{
              width: "83%",
              fontSize: 16,
              marginBottom: 8,
              color: "#4d4d4d",
            }}
          />
          <InterRegularText
            text={Language.EN.regis.contactText}
            style={{
              width: "83%",
              fontSize: 14,
              marginBottom: 16,
              color: "#4d4d4d",
            }}
          />
          <InterBoldText
            text={Language.EN.regis.labelAmend}
            style={{
              width: "83%",
              fontSize: 16,
              marginBottom: 8,
              color: "#4d4d4d",
            }}
          />
          <InterRegularText
            text={Language.EN.regis.amendText}
            style={{
              width: "83%",
              fontSize: 14,
              marginBottom: 24,
              color: "#4d4d4d",
            }}
          />
        </ScrollView>
      </View>
    </Modal>
  );
}
