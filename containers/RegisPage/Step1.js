import React, { useState, useEffect } from "react";
import { View, TouchableOpacity, StyleSheet } from "react-native";
import {
  InterRegularTextInput,
  InterRegularText,
  InterBoldText,
} from "../../components/StyledText";
import { Language } from "../../constants/language";
import CustomCheckBox from "../../components/CustomCheckBox";
import Button from "../../components/Button";
import Constants from "expo-constants";
import BackRed from "../../components/BackRed";
import { ScrollView } from "react-native-gesture-handler";
import UseKeyboard from "../../components/UseKeyboard";
import ModalBox from "./ModalBox";
import * as registerAction from "../../actions/registerAction";
import _ from "lodash";

export default function Step1(props) {
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [cPassword, setCPassword] = useState("");
  const [emailError, setEmailError] = useState(false);
  const [messageEmailError, setMessageEmailError] = useState("");
  const [passwordError, setPasswordError] = useState(false);
  const [cPasswordError, setCPasswordError] = useState(false);
  const [visible, setVisible] = useState(false);
  const [agree, setAgree] = useState(false);
  const [keyboardHeight, setKeyboardHeight] = useState(0);
  const emailRef = React.createRef();
  const passwordRef = React.createRef();
  const cPasswordRef = React.createRef();
  const scrollView = React.createRef();

  const keyboard = UseKeyboard();
  useEffect(() => {
    setKeyboardHeight(keyboard);
  }, [keyboard]);

  async function onSubmitEmail() {
    passwordRef.current.focus();
    checkEmail(userName);
  }
  async function checkEmail(value) {
    if (value) {
      let format = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if (format.test(value)) {
        let body = { EMAIL: value };
        const res = await registerAction.checkEmail(body);
        if (res.statusCode === "0") {
          setEmailError(false);
        } else {
          setEmailError(true);
          setMessageEmailError(res.data);
        }
      } else {
        setEmailError(true);
        setMessageEmailError(Language.EN.regis.invalid);
      }
    }
  }
  function onSubmitPassword() {
    cPasswordRef.current.focus();
    checkPassword(password);
  }
  function checkPassword(value) {
    if (value) {
      if (value.length > 7) {
        setPasswordError(false);
      } else {
        setPasswordError(true);
      }
      checkCPassword(cPassword, value);
    }
  }
  function checkCPassword(value, password) {
    if (value && password) {
      if (value === password) {
        setCPasswordError(false);
      } else {
        setCPasswordError(true);
      }
    }
  }
  function buttonContinue() {
    let result = false;
    if (
      userName &&
      password.length > 7 &&
      cPassword.length > 7 &&
      cPassword === password &&
      agree &&
      !emailError &&
      !passwordError &&
      !cPasswordError
    ) {
      result = true;
    }

    return result;
  }
  function moveScroll(value) {
    scrollView.current.scrollTo({ y: value });
  }

  return (
    <View style={{ flex: 1 }}>
      <ModalBox
        onClosed={() => {
          setVisible(false);
        }}
        isOpen={visible}
        navigation={props.navigation}
      />
      <View style={{ height: Constants.statusBarHeight }} />
      <ScrollView ref={scrollView} contentContainerStyle={{ flexGrow: 1 }}>
        <View
          style={{
            flex: 1,
            backgroundColor: "#f4f4f4",
            alignItems: "center",
          }}
        >
          <View
            style={{
              marginTop: 17,
              width: "87%",
            }}
          >
            <BackRed
              onPress={() => {
                props.navigation.goBack();
              }}
            />
          </View>
          <InterBoldText
            text={Language.EN.regis.createAccountText}
            style={{
              fontSize: 24,
              width: "87%",
              marginBottom: 8,
              color: "#4d4d4d",
              marginTop: 48,
            }}
          />
          <InterRegularTextInput
            label={Language.EN.regis.email}
            ref={emailRef}
            keyboardType="email-address"
            returnKeyType="next"
            onSubmitEditing={onSubmitEmail}
            showError={true}
            fixError={emailError}
            textError={emailError ? messageEmailError : null}
            onFocus={() => {
              moveScroll(64);
            }}
            onChangeText={(value) => {
              const email = _.toLower(value);
              setUserName(email);
              setEmailError(false);
            }}
            onBlur={() => {
              checkEmail(userName);
            }}
          />
          <InterRegularTextInput
            label={Language.EN.regis.password}
            ref={passwordRef}
            secureTextEntry={true}
            showError={true}
            fixError={passwordError}
            textError={passwordError ? Language.EN.regis.passwordError : null}
            returnKeyType="next"
            onSubmitEditing={onSubmitPassword}
            onFocus={() => {
              moveScroll(64 * 2);
            }}
            onChangeText={(password) => {
              setPassword(password);
              setPasswordError(false);
            }}
            onBlur={() => {
              checkPassword(password);
            }}
          />
          <InterRegularTextInput
            label={Language.EN.regis.cPassword}
            ref={cPasswordRef}
            secureTextEntry={true}
            showError={true}
            fixError={cPasswordError}
            textError={cPasswordError ? Language.EN.regis.cPasswordError : null}
            onFocus={() => {
              moveScroll(64 * 3);
            }}
            onChangeText={(cPassword) => {
              setCPassword(cPassword);
              setCPasswordError(false);
            }}
            onBlur={() => {
              checkCPassword(cPassword, password);
            }}
            //  onSubmitEditing={checkCPassword(cPassword, password)}
          />
          <View style={{ width: "87%", marginTop: 16 }}>
            <CustomCheckBox
              isChecked={false}
              text1={Language.EN.regis.agree}
              text2={Language.EN.regis.term}
              onCheck={(check) => {
                if (check) {
                  setAgree(true);
                } else {
                  setAgree(false);
                }
              }}
              onPressText2={() => {
                setVisible(true);
              }}
            />
          </View>

          <Button
            text={Language.EN.regis.continue}
            activeOpacity={buttonContinue() ? 0 : 1}
            style={{
              backgroundColor: buttonContinue() ? "#ED1B24" : "#cccccc",
            }}
            onPress={() => {
              if (userName && password && password === cPassword && agree) {
                props.navigation.navigate("Step2", { userName, password });
              }
            }}
          />
          <TouchableOpacity
            onPress={() => {
              props.navigation.goBack();
            }}
            style={{ marginBottom: keyboardHeight }}
          >
            <InterBoldText
              text={Language.EN.regis.haveAccount}
              style={{
                fontSize: 18,
                marginTop: 24,
                color: "#ED1B24",
                fontWeight: "bold",
                marginBottom: 20,
              }}
            />
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}
